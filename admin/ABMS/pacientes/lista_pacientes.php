
<?php

include('../../server/db_connect.php');
session_start();
include('../../server/forbidden.php');

$sqlPacientes="SELECT * FROM pacientes inner join financiadores on pac_finan=finan_id inner join cordones on cordon_id=pac_cordon";
$pacientes=mysqli_query($enlace, $sqlPacientes);
?>

<div class="container">
<div class="titleForm">Pacientes</div>
<button type="button" id="btnNewPaciente" class="btn btn-success"  data-toggle="modal" data-target="#modalPaciente"><i class="fa fa-plus-circle"></i></span>  Nuevo paciente</button>
    <div class="formulario" style="overflow-x: auto; white-space: nowrap;">    
        

    <table class="table table-hover">
    <thead>
    <tr>
      <th scope="col">N° Afil.</th>
      <th scope="col">Nombre</th>
      <th scope="col">Financiador</th>
      <th scope="col">Cordón</th>
      <th hidden scope="col">IVA</th>
      <th scope="col">Diagnóstico</th>
      <th scope="col">Fecha alta</th>
      <th hidden scope="col">Domicilio</th>
      <th hidden scope="col">Teléfono 1</th>
      <th hidden scope="col">Teléfono 2</th>
      <th scope="col">Responsable</th>
      <th hidden scope="col">Tel. Responsable</th>
      <!-- <th scope="col">Acción</th> -->
      
    </tr>
  </thead>            
        <tbody id="camposPacientes">

        <?php 
                $i = 0;
                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($pacientes))
                {
        ?>
            <tr>
            <td>
                <label id="<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_num_afil"])); ?>"><?php echo(utf8_encode($fila["pac_num_afil"])); ?></label>                
            </td>
            <td>
                <label id="pac_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_nombre"])); ?>"><?php echo(utf8_encode($fila["pac_nombre"])); ?></label>                
            </td>
            <td>
                <label id="finan_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["finan_id"])); ?>"><?php echo(utf8_encode($fila["finan_nombre"])); ?></label>                
            </td>
            <td>
                <label id="cordon_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_cordon"])); ?>"><?php echo(utf8_encode($fila["cordon_nombre"])); ?></label>                
            </td>      
            <td hidden>
                <label id="iva_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_iva"])); ?>"><?php echo(utf8_encode($fila["pac_iva"])); ?></label>                
            </td>  
            <td>
                <label id="diag_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_diag"])); ?>"><?php echo(utf8_encode($fila["pac_diag"])); ?></label>                
            </td>  
            <td>
                <label id="fecha_alta_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_fecha_alta"])); ?>"><?php echo(utf8_encode($fila["pac_fecha_alta"])); ?></label>                
            </td>  
            <td hidden>
                <label id="dom_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_dom"])); ?>"><?php echo(utf8_encode($fila["pac_dom"])); ?></label>                
            </td>  
            <td hidden>
                <label id="tel_1_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_tel1"])); ?>"><?php echo(utf8_encode($fila["pac_tel1"])); ?></label>                
            </td>  
            <td hidden>
                <label id="tel_2_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_tel2"])); ?>"><?php echo(utf8_encode($fila["pac_tel2"])); ?></label>                
            </td>  
            <td>
                <label id="resp_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_resp"])); ?>"><?php echo(utf8_encode($fila["pac_resp"])); ?></label>                
            </td>  
            <td hidden>
                <label id="tel_resp_<?php echo(utf8_encode($fila["pac_num_afil"])); ?>" value="<?php echo(utf8_encode($fila["pac_tel_resp"])); ?>"><?php echo(utf8_encode($fila["pac_tel_resp"])); ?></label>                
            </td>   
            <td style="text-align:right">
                <button type="button" data-toggle="modal" data-target="#modalPaciente" class="btn btn-sm btn-primary form_edit" id="<?php echo(utf8_encode($fila["pac_num_afil"])); ?>"><i class="fa fa-edit"></i>  Editar</button>                                            
                <button type="button" data-toggle="modal" data-target="#modalPacienteDelete" class="btn btn-sm btn-danger form_edit" id="<?php echo(utf8_encode($fila["pac_num_afil"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button>       
            </td>     
        </tr>
        <?php			
                $i ++;
                }			
            ?>
        </tbody>
    </table>
    </div>

  </div>

  <!-- MODAL PARA NUEVO/EDITAR -->
  <div class="modal fade" id="modalPaciente" role="dialog" >  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo Paciente</h5>
                        <button type="button" class="close"  onclick="borrarCampos()" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >                
                        <form method="post" id="form" name="form">                                                   
                            <p><b>n° afiliado</b></p>
                            <input type="text" id="pac_num_afil_edit" name="pac_num_afil_edit" class="form-control required"></input> 
                            <div id="errorMsg" class="alert alert-danger" role="alert" style="display:none;"></div>
                            <p><b>Nombre</b></p>
                            <input type="text" id="pac_nombre_edit" class="form-control required" name="pac_nombre_edit"></input>  
                            <p><b>Financiador</b></p>
                            <select class="custom-select selectTabla" id="pac_finan_edit" name="pac_finan_edit">
                                <option selected hidden>Seleccionar Financiador</option>  
                                    <?php
                                        $sqlFinanciadores = "SELECT * FROM financiadores order by finan_nombre";
                                        $financiadores=mysqli_query($enlace, $sqlFinanciadores);
                                        while($financiador=mysqli_fetch_assoc($financiadores))
                                        {
                                            echo "<option value='".$financiador['finan_id']."'>".$financiador['finan_nombre']."</option>";
                                        }
                                    ?>       
                            </select>                   
                            <p><b>Cordón</b></p>
                            <select class="form-control required" id="pac_cordon_edit" name="pac_cordon_edit" value="Seleccionar">
                                <option disabled selected hidden>Seleccionar</option>
                                <option value="1">AMBA</option>
                                <option value="2">PRIMER CORDON</option>
                                <option value="3">SEGUNDO CORDON</option>
                            </select>
                            <div id="errorCordon" class="alert alert-danger" role="alert" style="display:none;"></div>
                            <!--
                            <input type="text" id="pac_cordon_edit" class="form-control" name="pac_cordon_edit"></input>
                            -->
                            <p><b>IVA</b></p>
                            <input type="text" id="pac_iva_edit" class="form-control" name="pac_iva_edit"></input>
                            <p><b>Diagnóstico</b></p>
                            <input type="text" id="pac_diag_edit" class="form-control required" name="pac_diag_edit"></input>
                            <p><b>Fecha de alta</b></p>
                            <input type="date" id="pac_fecha_alta_edit" class="form-control required" name="pac_fecha_alta_edit"></input>
                            <p><b>Domicilio</b></p>
                            <input type="text" id="pac_dom_edit" class="form-control" name="pac_dom_edit"></input>
                            <p><b>Teléfono 1</b></p>
                            <input type="number" id="pac_tel_1_edit" class="form-control" name="pac_tel_1_edit"></input>
                            <p><b>Teléfono 2</b></p>
                            <input type="number" id="pac_tel_2_edit" class="form-control" name="pac_tel_2_edit"></input>
                            <p><b>Responsable</b></p>
                            <input type="text" id="pac_resp_edit" class="form-control" name="pac_resp_edit"></input>
                            <p><b>Tel. Responsable</b></p>
                            <input type="text" id="pac_tel_resp_edit" class="form-control" name="pac_tel_resp_edit"></input>

                                                   
                    </div>
                            <div class="modal-footer">
                                    <button type="button" id="btnGuardarPaciente" onclick="modificarPaciente('editar')" class="btn btn-primary">Guardar</button>
                                    <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>                     
            </div>
          </div>   
    </div>

    <!-- MODAL PARA DELETE -->
    <div class="modal fade" id="modalPacienteDelete" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Borrar paciente?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                    <form method="post" action="" id="form" name="form">                                            
                            Nombre: <input disabled type="text" id="pac_nombre_edit" class="form-control" name="pac_nombre_edit" ></input>
                            <input type="text" id="pac_num_afil_edit" name="pac_num_afil_edit" hidden disabled></label>                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="modificarPaciente('borrar')" class="btn btn-primary">Aceptar</button>
                            <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>    
    
    <script src="js/fc_pacientes.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">   