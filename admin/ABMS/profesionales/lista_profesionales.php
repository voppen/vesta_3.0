
<?php

include('../../server/db_connect.php');
session_start();
include('../../server/forbidden.php');

//SE CREA EL HTML DE LA LISTA DE COORDINADORES
$sqlCoordinadores = "SELECT * FROM coordinadores";
$coordinadores=mysqli_query($enlace, $sqlCoordinadores);

$i = 0;
$listaCoordinadores = "<select id='dropdownCoordinadores'>";

while($fila=mysqli_fetch_assoc($coordinadores))
{
    $listaCoordinadores.="<option class='listaCoordinadores' id='coord_edit_"; 
    $listaCoordinadores.=$fila["coord_id"]; 
    $listaCoordinadores.="' value='";
    $listaCoordinadores.=$fila["coord_id"]."'>";
    $listaCoordinadores.=$fila["coord_nombre"]."</option>";
}
$listaCoordinadores.="</select>";


$sqlProfesionales="SELECT * FROM profesionales INNER JOIN coordinadores ON profesionales.prof_id_coord = coordinadores.coord_id ";
$profesionales=mysqli_query($enlace, $sqlProfesionales);
?>

<div class="container">
<div class="titleForm">Profesionales</div>
<button type="button" class="btn btn-success asda"  data-toggle="modal" data-target="#modalProfesional"><i class="fa fa-plus-circle"></i></span>  Nuevo profesional</button>
    <div class="formulario">    
        

    <table class="table table-hover">
    <thead>
    <tr>
      <th scope="col">Nombre</th> 
      <th scope="col">Profesión</th> 
      <th scope="col">Coordinador</th> 
      <th scope="col">Acciones</th> 
    </tr>
  </thead>            
        <tbody id="camposProfesionales">

        <?php 
                $i = 0;
                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($profesionales))
                {
        ?>
            <tr>
          <!--
            <label for="" id="<?php /*echo(utf8_encode($fila["prof_id"]));*/ ?>"></label>                
            -->
            <td>
                <label for="" id="prof_<?php echo(utf8_encode($fila["prof_id"])); ?>" value="<?php echo(utf8_encode($fila["prof_nombre"])); ?>"><?php echo(utf8_encode($fila["prof_nombre"])); ?></label>                
            </td>
            <td>
                <label for="" id="prof_profesion_<?php echo(utf8_encode($fila["prof_id"])); ?>" value="<?php echo(utf8_encode($fila["prof_profesion"])); ?>"><?php echo(utf8_encode($fila["prof_profesion"])); ?></label>                
            </td>
            <td>
                <label for="" id="coord_<?php echo(utf8_encode($fila["prof_id_coord"])); ?>" value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"><?php echo(utf8_encode($fila["coord_nombre"])); ?></label>                
            </td>
            <td>
                <button type="button" data-toggle="modal" data-target="#modalProfesional" class="btn btn-sm btn-primary form_edit" id="<?php echo(utf8_encode($fila["prof_id"])); ?>" coordinador="<?php echo(utf8_encode($fila["prof_id_coord"])); ?>"><i class="fa fa-edit"></i>  Editar</button>                                            
                <button type="button" data-toggle="modal" data-target="#modalProfesionalDelete" class="btn btn-sm btn-danger form_edit" id="<?php echo(utf8_encode($fila["prof_id"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button>
                
            </td>            
        </tr>
        <?php			
                $i ++;
                }			
            ?>
        </tbody>
    </table>
    </div>

  </div>

  <!-- MODAL PARA NUEVO/EDITAR -->
  <div class="modal fade" id="modalProfesional" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo Profesional</h5>
                        <button type="button" class="close"  onclick="borrarCampos()" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                        <form method="post" id="form" name="form">                                                   
                            <p>Nombre</p>
                            <input type="text" id="prof_nombre_edit" class="form-control" name="prof_nombre_edit"  value="<?php echo(utf8_encode($fila["prof_nombre"])); ?>"></input>
                            <div id="errorMsg" class="alert alert-danger" role="alert" style="display:none;"></div>
                            <input type="text" id="prof_id_edit" name="prof_id_edit" value="<?php echo(utf8_encode($fila["prof_id"])); ?>" hidden disabled></label>                        
                            <p>Profesión</p>
                            <input type="text" id="prof_profesion_edit" class="form-control" name="prof_profesion_edit"  value="<?php echo(utf8_encode($fila["prof_profesion"])); ?>"></input>
                            <p>Coordinador</p> 
                            
                            <?php
                                $sqlCoordinadores = "SELECT * FROM coordinadores order by coord_nombre";
                                $coordinadores=mysqli_query($enlace, $sqlCoordinadores);
                            ?>                                               
                            <select class="custom-select" id="idCoordinadorModal">                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($coordinador=mysqli_fetch_assoc($coordinadores))
                                    {
                                        echo "<option value='".$coordinador['coord_id']."'>".$coordinador['coord_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                                                    
                            
                            <!--
                            <input type="text" id="coord_nombre_edit" class="form-control" name="coord_nombre_edit"  value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"></input>
                    -->
                            
                    </div>
                            <div class="modal-footer">
                                    <button type="button" onclick="modificarProfesional('guardar')" class="btn btn-primary">Guardar</button>
                                    <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>                     
            </div>
          </div>   
    </div>

    <!-- MODAL PARA DELETE -->
    <div class="modal fade" id="modalProfesionalDelete" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Borrar Profesional?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                    <form method="post" action="" id="form" name="form">                                            
                            <p>Nombre</p>
                            <input disabled type="text" id="prof_nombre_edit" class="form-control" name="prof_nombre_edit"  value="<?php echo(utf8_encode($fila["prof_nombre"])); ?>"></input>
                            <input type="text" id="prof_id_edit" name="prof_id_edit" hidden disabled></label>                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="modificarProfesional('borrar')" class="btn btn-primary">Aceptar</button>
                            <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>    
    
    <script src="js/fc_profesionales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>