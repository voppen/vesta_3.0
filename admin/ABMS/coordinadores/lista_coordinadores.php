<?php
include('../../server/db_connect.php');
session_start();
include('../../server/forbidden.php');

$sqlCoordinadores="SELECT * FROM coordinadores order by coord_nombre desc";
$resultado=mysqli_query($enlace, $sqlCoordinadores);
?>

<div class="container">
    <div class="titleForm">Coordinadores</div>
    <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#modalCoord"><i class="fa fa-plus-circle"></i></span>  Nuevo coordinador</button>
<div class="formulario">    
    

    <table class="table table-hover tableCoordinadores">
    <thead>
        <tr>
            <th colspan=3 scope="col">Nombre</th>
        </tr>
    </thead>            
        <tbody id="camposCoordinadores">

            <?php 
                    $i = 0;
                    /* INICIO DEL MUESTREO */
                    while($fila=mysqli_fetch_assoc($resultado))
                    {
            ?>
            <tr class='datos' pagina="<?php echo(ceil(($i+1)/10)); ?>">
                <td hidden>
                    <label for="" value="<?php echo(utf8_encode($fila["coord_id"])); ?>" value="<?php echo(utf8_encode($fila["coord_id"])); ?>"><?php echo(utf8_encode($fila["coord_id"])); ?></label>                
                </td>
                <td>
                    <label for="" id="coord_<?php echo(utf8_encode($fila["coord_id"])); ?>" value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"><?php echo(utf8_encode($fila["coord_nombre"])); ?></label>                
                </td>
                <td style="text-align:right" id="edit_<?php echo($i); ?>">
                    <button type="button" data-toggle="modal" data-target="#modalCoord" class="btn btn-sm btn-primary form_edit" id="<?php echo(utf8_encode($fila["coord_id"])); ?>"><i class="fa fa-edit"></i>  Editar</button>
                    <button type="button" data-toggle="modal" data-target="#modalCoordDelete" class="btn btn-sm btn-danger form_edit" id="<?php echo(utf8_encode($fila["coord_id"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button>
                </td>
            </tr>

            <?php			
                    $i ++;
                    }			
                ?>


        </tbody>
    </table>
<!--
    <ul class="pagination">
        <li><a class="anterior" href="#"><< Anterior&nbsp;&nbsp;</a></li>
        <li class="active"><a style="background-color:#6699ff;" class="pagina" pag="1" href="#">&nbsp;1&nbsp;</a></li>
    <?php
    /*
    $pages = ceil($i/10);
    for ($i=1; $i < $pages ; $i++) { 
        ?>
        <li><a class="pagina" pag=<?php echo($i+1); ?> href="#">&nbsp;<?php echo($i+1); ?>&nbsp;</a></li>
        <?php
    }
    */
    ?>
        <li><a class="siguiente" href="#">&nbsp;&nbsp;Siguiente >></a></li>
    </ul>
-->

</div>
</div>

    <!-- MODAL PARA NUEVO/EDITAR -->
  <div class="modal fade" id="modalCoord" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo Coordinador</h5>
                        <button type="button" class="close"  onclick="borrarCampos()" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                        <form method="post" id="form" name="form">                                                   
                            <p>Nombre</p>
                            <input type="text" id="coord_nombre_edit" class="form-control" name="coord_nombre_edit"  value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"></input>
                            <div id="errorMsg" class="alert alert-danger" role="alert" style="display:none;"></div>
                            <input type="text" id="coord_id_edit" name="coord_id_edit" value="<?php echo(utf8_encode($fila["coord_id"])); ?>" hidden disabled></label>                        
                    </div>
                            <div class="modal-footer">
                                    <button type="button" onclick="modificarCoordinador('guardar')" class="btn btn-primary">Guardar</button>
                                    <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>                     
            </div>
          </div>   
    </div>

    <!-- MODAL PARA DELETE -->
    <div class="modal fade" id="modalCoordDelete" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Borrar Coordinador?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                    <form method="post" action="" id="form" name="form">                                            
                            <p>Nombre</p>
                            <input disabled type="text" id="coord_nombre_edit" class="form-control" name="coord_nombre_edit"  value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"></input>
                            <input type="text" id="coord_id_edit" name="coord_id_edit" hidden disabled></label>                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="modificarCoordinador('borrar')" class="btn btn-primary">Aceptar</button>
                            <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>    

<script src="js/fc_coordinadores.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>