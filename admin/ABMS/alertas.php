<?php

include('../server/db_connect.php');
session_start();
include('../server/forbidden.php');

$sqlAlertas="SELECT *,(DATEDIFF(hiscli_det_fecha_baja, CURDATE())) AS days FROM hiscli_cab INNER JOIN pacientes on hiscli_id_paciente=pac_num_afil inner join hiscli_det on hiscli_id=hiscli_det_cab_id inner join lista_prestaciones on hiscli_det_id_prest=prest_id WHERE (DATEDIFF(hiscli_det_fecha_baja, CURDATE()))>=0 and (DATEDIFF(hiscli_det_fecha_baja, CURDATE()))<=5";
$alertas=mysqli_query($enlace, $sqlAlertas);
?>

<div class="container">
<div class="titleForm">Alertas</div>
    <div class="formulario" style="overflow-x: auto; white-space: nowrap;">    
        

    <table class="table table-hover">
    <thead>
    <tr>
      <th scope="col">Hist. Clin.</th>
      <th scope="col">Paciente</th>
      <th scope="col">Prestación</th>
      <th scope="col">Días restantes</th>
    </tr>
  </thead>            
        <tbody id="camposAlertas">

        <?php 
                $i = 0;
                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($alertas))
                {
        ?>
            <tr>
                <td>
                    <label id="<?php echo(utf8_encode($fila["hiscli_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_id"])); ?>"><?php echo(utf8_encode($fila["hiscli_id"])); ?></label>                
                </td>
                <td>
                    <label id="<?php echo(utf8_encode($fila["hiscli_id_paciente"])); ?>" value="<?php echo(utf8_encode($fila["pac_nombre"])); ?>"><?php echo(utf8_encode($fila["pac_nombre"])); ?></label>                
                </td>
                <td>
                    <label id="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["prest_nombre"])); ?>"><?php echo(utf8_encode($fila["prest_nombre"])); ?></label>                
                </td>
                <td>
                    <label id="<?php echo(utf8_encode($fila["days"])); ?>" value="<?php echo(utf8_encode($fila["days"])); ?>"><?php echo(utf8_encode($fila["days"])); ?></label>                
                </td>
            </tr>
            <?php			
                $i ++;
                }			
            ?>
        </tbody>
    </table>

</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">