
<?php
session_start();
include('../../server/db_connect.php');
include('../../server/forbidden.php');

$sqlHistoriasClinicas = "SELECT *,(DATEDIFF(hiscli_fecha_baja, CURDATE())) AS days FROM hiscli_cab INNER JOIN pacientes on hiscli_id_paciente=pac_num_afil";
$historiasClinicas=mysqli_query($enlace, $sqlHistoriasClinicas);
?>

<div class="container">
<div class="titleForm">Historias Clinicas</div>
<!-- <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#modalHistoriaClinica"><i class="fa fa-plus-circle"></i></span>  Nueva Historia Clinica</button> -->
<button type="button" onclick="abrirFormNuevaHistoriaClinica()" class="btn btn-success" ><i class="fa fa-plus-circle"></i></span>  Nueva Historia Clinica</button>
    <div class="formulario">    
        

    <table class="table table-hover">
    <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Paciente</th>
      <th scope="col">Fecha Alta</th>
      <th scope="col">Fecha Baja</th>
      <th scope="col">Estado</th>      
      <th scope="col"></th>      
    </tr>
  </thead>            
        <tbody id="camposHistoriasClinicas">

        <?php 
                $i = 0;
                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($historiasClinicas))
                {
                    $dat = $fila["hiscli_id"];
                    $sqlAlertas = "SELECT *,(DATEDIFF(hiscli_det_fecha_baja, CURDATE())) AS days FROM hiscli_det where hiscli_det_cab_id='$dat' and (DATEDIFF(hiscli_det_fecha_baja, CURDATE()))>=0 and (DATEDIFF(hiscli_det_fecha_baja, CURDATE()))<=5";
                    $alertas=mysqli_query($enlace, $sqlAlertas);                    
        ?>
            <tr>
            <td>
                <label for="" value="<?php echo(utf8_encode($fila["hiscli_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_id"])); ?>"><?php echo(utf8_encode($fila["hiscli_id"])); ?></label>                
                <?php 
                    $j = 0;
                    /* INICIO DEL MUESTREO */
                    while($alertaDias=mysqli_fetch_assoc($alertas))
                    {
                ?>
                    <i class="fa fa-exclamation-triangle alertaDias" style="color:red" value="<?php echo(utf8_encode($alertaDias["days"])); ?>" hidden></i>
                <?php			
                    $j ++;
                    }			
                ?>
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_id"])); ?>" value="<?php echo(utf8_encode($fila["pac_nombre"])); ?>"><?php echo(utf8_encode($fila["pac_nombre"])); ?></label>                
            </td>
            <td>                
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_fecha_alta"])); ?>"><?php echo $fila["hiscli_fecha_alta"]; ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_fecha_baja"])); ?>"><?php echo $fila["hiscli_fecha_baja"]; ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_estado"])); ?>"><?php echo(utf8_encode($fila["hiscli_estado"])); ?></label>                
            </td>
            <td style="text-align:right">
                <button type="button" class="btn btn-sm btn-primary form_edit" onclick="cargarDetalles2(this.id)" id="<?php echo(utf8_encode($fila["hiscli_id"])); ?>"><i class="fa fa-align-justify"></i>  Detalles</button>
                <button type="button" class="btn btn-sm btn-success form_edit"onclick="cargarEdicionHistoriaClinica(this)" id="<?php echo(utf8_encode($fila["hiscli_id"])); ?>"><i class="fa fa-edit"></i>  Editar</button>                                            
                <button type="button" data-toggle="modal" data-target="#modalHistoriaClinicaDelete" class="btn btn-sm btn-danger form_edit" id="<?php echo(utf8_encode($fila["hiscli_id"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button>
                
            </td>            
        </tr>
        <?php			
                $i ++;
                }			
            ?>
        </tbody>
    </table>
    </div>

  </div>

  <!-- MODAL PARA NUEVO/EDITAR -->
  <div class="modal fade" id="modalHistoriaClinica" role="dialog" >  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nueva Historia Clínica</h5>
                        <button type="button" class="close"  onclick="borrarCampos()" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >                
                        <form method="post" id="form" name="form">

                        <?php
                        $sqlPacientes = "SELECT * FROM pacientes order by pac_nombre";
                        $pacientes=mysqli_query($enlace, $sqlPacientes);
                        ?>
                        <tr>
                            <td><label>Paciente</label></td>
                            <td colspan="3">
                                <select class="custom-select required" id="hiscli_pacientes_edit">                                    
                                <option value="">Seleccionar</option>
                                    <?php
                                        while($lista=mysqli_fetch_assoc($pacientes))
                                        {
                                            echo "<option value='".$lista['pac_num_afil']."'>".$lista['pac_nombre']."</option>";
                                        }
                                    ?>       
                                </select>
                            </td>   
                        </tr>  
                        </br>   </br> 
                        <tr>
                          <td><label>Motivo de ingreso</label></td>
                          <td><input id="hiscli_ingreso_edit" type="text" class="form-control required" placeholder="Ingresar motivo de ingreso"></input></td>
                        </tr> 
                        </br>           
                        <tr>
                          <td><label>Fecha de alta</label></td>
                          <td><input type="date" id="hiscli_fecha_alta_edit" class="form-control required" name="hiscli_fecha_alta_edit"></input></td>
                        </tr>
                        </br>
                        <tr>
                          <td><label>Fecha de baja</label></td>
                          <td><input type="date" id="hiscli_fecha_baja_edit" class="form-control" name="hiscli_fecha_baja_edit"></input></td>
                        </tr>
                        </br>
                        <tr>
                          <td><label>Estado</label></td>
                          <td><select class="custom-select required" id="hiscli_estado_edit">
                          <option value="Vigente" selected>Vigente</option>
                          <option value="Vigente" selected>Internado</option>
                          </select></td>
                        </tr>
                        </br> </br> 
                        <tr>
                            <td><label>Descripción</label></td>
                            <td>
                                <textarea rows="4" cols="50" class="form-control" id="hiscli_descripcion_edit" placeholder="Ingresar descripción"></textarea>
                            </td>
                        </tr>
                    </div>
                            <div class="modal-footer">
                                    <button type="button" id="btnGuardarHiscli" onclick="guardarHisCli('guardar')" class="btn btn-primary">Guardar</button>
                                    <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>                     
            </div>
          </div>   
    </div>

      <!-- MODAL PARA DELETE -->
      <div class="modal fade" id="modalHistoriaClinicaDelete" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Borrar historia clínica?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                    <form method="post" action="" id="form" name="form">                                            
                            Paciente: <input disabled type="text" id="pac_nombre_edit" class="form-control" name="pac_nombre_edit" ></input>
                            <input type="text" id="pac_num_afil_edit" name="pac_num_afil_edit" hidden disabled></label>                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="deleteHiscli($('#pac_num_afil_edit').val())" class="btn btn-primary">Aceptar</button>
                            <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>  


    
    
    <script src="js/fc_historias_clinicas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">            