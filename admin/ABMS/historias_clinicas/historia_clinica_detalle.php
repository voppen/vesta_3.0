
<?php
session_start();
include('../../server/db_connect.php');
include('../../server/forbidden.php');

$idCab = $_POST['idCab'];
$sqlHistoriasClinicas = "SELECT *,(DATEDIFF(hiscli_fecha_baja, CURDATE())) AS days FROM hiscli_cab inner join hiscli_det on hiscli_id=hiscli_det_cab_id INNER JOIN pacientes on hiscli_id_paciente=pac_num_afil inner join lista_prestaciones on hiscli_det_id_prest=prest_id inner join coordinadores on hiscli_det_id_coord=coord_id where hiscli_id='$idCab'";
$historiasClinicas=mysqli_query($enlace, $sqlHistoriasClinicas);
?>

<div class="container">
<div class="titleForm">Historias Clinicas</div>

    <table id="tableCabecera" class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Paciente</th>
            <th scope="col">Motivo de ingreso</th>
            <th scope="col">Fecha alta</th>
            <th scope="col">Fecha baja</th>
            <th scope="col">Estado</th>
            <th scope="col">Descripción</th>
        </tr>
        </thead>
        <tbody>
            <?php
                $sqlCabecera="SELECT * FROM hiscli_cab inner join pacientes on pac_num_afil=hiscli_id_paciente where hiscli_id='$idCab'";
                $cabecera=mysqli_query($enlace, $sqlCabecera);
                while($fila=mysqli_fetch_assoc($cabecera))
                {
            ?>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["pac_nombre"])); ?>" value="<?php echo(utf8_encode($fila["pac_nombre"])); ?>"><?php echo(utf8_encode($fila["pac_nombre"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_ingreso"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_ingreso"])); ?>"><?php echo(utf8_encode($fila["hiscli_ingreso"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_fecha_alta"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_fecha_alta"])); ?>"><?php echo(utf8_encode($fila["hiscli_fecha_alta"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_fecha_baja"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_fecha_baja"])); ?>"><?php echo(utf8_encode($fila["hiscli_fecha_baja"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_estado"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_estado"])); ?>"><?php echo(utf8_encode($fila["hiscli_estado"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_descripcion"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_descripcion"])); ?>"><?php echo(utf8_encode($fila["hiscli_descripcion"])); ?></label>                
            </td>

            <?php
                }
            ?>
        </tbody>
    </table>

<!--
    <div class="divTableInternaciones formulario" id="divTableInternaciones" style="display:none; width:70.1% !important; padding-bottom:1em !important">
        <table id="tableInternaciones" class="table table-hover">
            <tr>
                <th scope="col">Motivo Internacion</th>
                <th scope="col">Fecha Internacion</th>
                <th scope="col">Fecha Alta</th>
            </tr>
        </table>
    </div> -->


    <div style="overflow-x: scroll">    
        

    <table class="table table-hover tableFit">
    <thead>
    <tr>
      <th scope="col">Prestación</th>
      <th scope="col">Fecha Alta</th>
      <th scope="col">Fecha Baja</th>
      <th scope="col">Profesional</th> 
      <th scope="col">Coordinador</th>
      <th scope="col">Frecuencia</th>
      <th scope="col"># sesiones</th>    
      <th scope="col">Total sesiones</th>   
      <th scope="col"># visitas</th>
      <th scope="col">Total hs/visita</th>
      <th scope="col">Total hs/mes</th>
      <th scope="col"># módulos</th>
      <th scope="col">Total módulos</th>                             
    </tr>
  </thead>            
        <tbody id="camposHistoriasClinicas">

        <?php 
                $i = 0;
                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($historiasClinicas))
                {
                    $dat = $fila["hiscli_id"];
                    $dat2 = $fila["hiscli_det_id"];
                    $sqlAlertas = "SELECT *,(DATEDIFF(hiscli_det_fecha_baja, CURDATE())) AS days FROM hiscli_det where hiscli_det_cab_id='$dat' and hiscli_det_id='$dat2' and (DATEDIFF(hiscli_det_fecha_baja, CURDATE()))>=0 and (DATEDIFF(hiscli_det_fecha_baja, CURDATE()))<=5";
                    $alertas=mysqli_query($enlace, $sqlAlertas);
        ?>
            <tr>   
            <td>
                <?php 
                    /* INICIO DEL MUESTREO */
                    while($alertaDias=mysqli_fetch_assoc($alertas))
                    {
                ?>
                    <i class="fa fa-exclamation-triangle alertaDias" style="color:red" value="<?php echo(utf8_encode($alertaDias["days"])); ?>"></i>
                <?php			
                    }			
                ?>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["prest_nombre"])); ?>" value="<?php echo(utf8_encode($fila["prest_nombre"])); ?>"><?php echo(utf8_encode($fila["prest_nombre"])); ?></label>                
            </td>
            <td>                
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_fecha_alta"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_fecha_alta"])); ?>"><?php echo $fila["hiscli_det_fecha_alta"]; ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_fecha_baja"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_fecha_baja"])); ?>"><?php echo $fila["hiscli_det_fecha_baja"]; ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_profesional"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_profesional"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_profesional"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["coord_nombre"])); ?>" value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"><?php echo(utf8_encode($fila["coord_nombre"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_frecuencia"])); ?>">
                <?php 
                    switch ($fila["hiscli_det_frecuencia"]) {
                        case '1':
                            echo("Diaria");
                            break;
                        case '2':
                            echo("Semanal");
                            break;
                        case '3':
                            echo("Mensual");
                            break;
                        
                        default:
                           echo("-");
                            break;
                    } 
                ?></label>                
            </td>  
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_cant_ses"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_cant_ses"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_cant_ses"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_total_ses"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_total_ses"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_total_ses"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_cant_vist_hs"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_cant_vist_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_cant_vist_hs"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_cant_hs"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_cant_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_cant_hs"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_total_hs"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_total_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_total_hs"])); ?></label>                
            </td>  
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_cant_mod"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_cant_mod"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_cant_mod"])); ?></label>                
            </td>  
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_total_mod"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_total_mod"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_total_mod"])); ?></label>                
            </td> 
        </tr>
        <?php			
                $i ++;
                }			
            ?>
        
        <!-- TABLA OLD -->


        <?php 
            $i = 0;
            $sqlHisCliOld = "SELECT *,(DATEDIFF(hiscli_fecha_baja, CURDATE())) AS days FROM hiscli_cab inner join hiscli_det_old on hiscli_id=hiscli_det_old_cab_id INNER JOIN pacientes on hiscli_id_paciente=pac_num_afil inner join lista_prestaciones on hiscli_det_old_id_prest=prest_id inner join coordinadores on hiscli_det_old_id_coord=coord_id where hiscli_id='$idCab' order by prest_id";            
            $hisCliOld=mysqli_query($enlace, $sqlHisCliOld);

                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($hisCliOld))
                {
        ?>
            <tr>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["prest_nombre"])); ?>" value="<?php echo(utf8_encode($fila["prest_nombre"])); ?>"><?php echo(utf8_encode($fila["prest_nombre"])); ?></label>                
            </td>
            <td>                
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_fecha_alta"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_fecha_alta"])); ?>"><?php echo $fila["hiscli_det_old_fecha_alta"]; ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_fecha_baja"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_fecha_baja"])); ?>"><?php echo $fila["hiscli_det_old_fecha_baja"]; ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_profesional"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_profesional"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_profesional"])); ?></label>                
            </td>
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["coord_nombre"])); ?>" value="<?php echo(utf8_encode($fila["coord_nombre"])); ?>"><?php echo(utf8_encode($fila["coord_nombre"])); ?></label>                
            </td>   
            <td>
            <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_frecuencia"])); ?>">
                <?php 
                    switch ($fila["hiscli_det_old_frecuencia"]) {
                        case '1':
                            echo("Diaria");
                            break;
                        case '2':
                            echo("Semanal");
                            break;
                        case '3':
                            echo("Mensual");
                            break;
                        
                        default:
                           echo("-");
                            break;
                    } 
                ?></label>                  
            </td>  
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_cant_ses"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_cant_ses"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_cant_ses"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_total_ses"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_total_ses"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_total_ses"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_cant_vist_hs"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_cant_vist_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_cant_vist_hs"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_cant_hs"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_cant_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_cant_hs"])); ?></label>                
            </td>   
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_total_hs"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_total_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_total_hs"])); ?></label>                
            </td>  
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_cant_mod"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_cant_mod"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_cant_mod"])); ?></label>                
            </td>  
            <td>
                <label for="" id="hiscli_<?php echo(utf8_encode($fila["hiscli_det_old_total_mod"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_old_total_mod"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_old_total_mod"])); ?></label>                
            </td> 
        </tr>
        <?php			
            $i ++;
            }			
        ?>

        </tbody>
    </table>
    </div>

    <br><button type="button" onclick="cargarHistoriasClinicas()" id="btnCancelar" class="btn btn-secondary">Volver</button>

  </div>


    
    
    <script src="js/fc_historias_clinicas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">            