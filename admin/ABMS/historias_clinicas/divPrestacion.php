
<?php
    session_start();
    include('../../server/db_connect.php');
    include('../../server/forbidden.php');
    include('../listaPrestaciones.php');

?>

<div id="containerPrestaciones">

    <label style="margin-right:2em !important; width:100% !important" for="">Prestacion</label>
    <select style="margin-bottom:1em !important; width:38.7% !important" class="custom-select" id="prestacionCreateHiscli" onchange="seleccionarPrestacion(this)">  
    <option value="Seleccionar">Seleccionar</option>
        <?php                        
            while($lista=mysqli_fetch_assoc($listaPrestaciones))
            {
                echo "<option value='".$lista['prest_id']."'>".$lista['prest_nombre']."</option>";
            }
        ?>       
    </select>

    <form action="" method="post">
        <table id="tableRows" style="display:none">        
            <tr>
                <td style="width:15% !important;">Fecha de alta</td>
                <td>Fecha de baja</td>
            </tr>
            <tr class="spaceUnder">
                <td><input type="date" id="fechaAltaPrestacionCreateHiscli" class="form-control required"></input></td>                            
                <td style="width:15% !important;"><input type="date" id="fechaBajaPrestacionCreateHiscli" class="form-control"></input></td>
            </tr>
            <tr>
                <td colpan="2" style="width:16% !important;">Profesional</td>
                <td style="width:16% !important;">Coordinador</td>
            </tr>
            <tr class="spaceUnder">
                <td colpan="2"  style="width:25% !important;"><input id="profesionalCreateHiscli" type="text" class="form-control required"></input></td>                
        <!--        <td style="width:25% !important;"><input id="coordinadorCreateHiscli" type="text" class="form-control required"></input></td> -->
                <td>
                    <select class="custom-select selectTabla" id="coordinadorCreateHiscli">
                    <option selected hidden>Seleccionar Coordinador</option>  
                        <?php
                            $sqlCoordinadores = "SELECT * FROM coordinadores order by coord_nombre";
                            $coordinadores=mysqli_query($enlace, $sqlCoordinadores);
                            while($coordinador=mysqli_fetch_assoc($coordinadores))
                            {
                                echo "<option value='".$coordinador['coord_id']."'>".$coordinador['coord_nombre']."</option>";
                            }
                        ?>       
                    </select>
                </td>
            </tr>        
            <tr>
                <td style="width:16% !important;">Frecuencia</td>
            </tr>
            <tr class="spaceUnder">
                <td>
                    <select class="custom-select required" id="frecuenciaCreateHiscli">                                    
                        <option value="1">Por Dia</option>
                        <option value="2">Por Semana</option>
                        <option value="3">Por Mes</option>                   
                    </select>
                </td>
            </tr>                                              
            <tr class="modulos" id="modulos">            
                <td style="width:16% !important;">Cantidad de Modulos</td>
                <td style="width:16% !important;">Total Modulos Por Mes</td>
                
            </tr>       
            <tr class="modulos" id="modulos">                                    
                <td style="width:25% !important;"><input id="modulosCreateHiscli" type="text" class="form-control required"></input></td>
                <td style="width:25% !important;"><input id="totalModulosCreateHiscli" type="text" class="form-control required"></input></td>
            </tr>    
            <tr class="horas" id="horas">            
                <td style="width:16% !important;">Cantidad de Visitas</td>                           
            </tr>       
            <tr class="horas" id="horas">            
            <td style="width:25% !important;"><input id="visitasHorasCreateHiscli" type="text" class="form-control required"></input></td>
            </tr>       
            <tr class="horas" id="horas">            
                <td style="width:16% !important;">Horas Por Visita</td>
                <td style="width:16% !important;">Total Horas Por Mes</td>                
            </tr>       
            <tr class="horas" id="horas">            
                <td style="width:25% !important;"><input id="horasCreateHiscli" type="text" class="form-control required"></input></td>
                <td  style="width:25% !important;"><input id="totalHorasCreateHiscli" type="text" class="form-control required"></input></td>
            </tr> 
            <tr class="sesiones" id="sesiones">            
                <td style="width:16% !important;">Cantidad de Sesiones</td>
                <td style="width:16% !important;">Total Sesiones Por Mes</td>
            </tr>
            <tr>            
            </tr>       
            <tr class="sesiones" id="sesiones">            
                <td style="width:25% !important;"><input id="sesionesCreateHiscli" type="text" class="form-control required"></input></td>
                <td style="width:25% !important;"><input id="totalSesionesCreateHiscli" type="text" class="form-control required"></input></td>
            </tr> 
            <tr>
                <td style="padding-top:1em !important;">Observaciones</td>
            </tr>
            <tr>
                <td colspan="5">
                    <textarea rows="2" cols="50" class="form-control" id="obserCreateHiscli"></textarea>
                </td>
            </tr>
            <tr>
                <button type="button" onclick="borrarPrestacion(this)" id="btnDeletePrest" class="btn btn-danger btnDeletePrest" >Eliminar prestación</button>
            </tr>     
            <tr>
                <input type="text" id="idDetalleCreateHiscli" hidden disabled></input>
            </tr>                         
        </table>        
    </form>
    <hr style="border: solid 0.5px black;">
</div>



