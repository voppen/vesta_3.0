
<?php
session_start();
include('../../server/db_connect.php');
include('../../server/forbidden.php');
include('../listaPrestaciones.php');

?>

<div class="container">
<div class="titleForm">Editar Historia Clinica</div>
<!--
<div class="btnFormHistClinicas">
    <button type="button" onclick="abrirFormNuevaHistoriaClinica()" class="btn btn-success" >Alertas</button>
    <button type="button" onclick="abrirFormNuevaHistoriaClinica()" class="btn btn-primary" >Internaciones</button>
    
</div>
-->

<!-- <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#modalHistoriaClinica"><i class="fa fa-plus-circle"></i></span>  Nueva Historia Clinica</button> -->
<!-- <button type="button" onclick="abrirFormNuevaHistoriaClinica()" class="btn btn-success" ><i class="fa fa-plus-circle"></i></span>  Nueva Historia Clinica</button> -->
<hr>
    <div class="formularioHistoriaClinica">    
        
<?php

    $sqlPacientes = "SELECT * FROM pacientes order by pac_nombre";
    $pacientes=mysqli_query($enlace, $sqlPacientes);
?>
<!-- <div class="divTableInternaciones formulario" id="divTableInternaciones" style="display:none; width:70.1% !important; padding-bottom:1em !important">
    <table id="tableInternaciones" class="table table-hover">
        <tr>
            <td>Motivo Internacion</td>
            <td>Fecha Internacion</td>
            <td>Fecha Alta</td>
        </tr>
    </table>
</div>  -->
<div class="tableFormHistoriaClinicaNueva">
    <form>
    <table style="width:70% !important">
        <input hidden id="numeroId" value="0">
        <tr>
            <td colspan="2" style="width:25% !important;">Paciente</td>
            <td colspan="2" style="width:25% !important;">Motivo de ingreso</td>
        </tr>
        <tr class="spaceUnder">
            <td class="paddingRight1" colspan="2" style="width:15% !important;">
                <select class="custom-select required" id="pacientesEditHiscli">                                    
                <option value="">Seleccionar</option>
                    <?php
                        while($lista=mysqli_fetch_assoc($pacientes))
                        {
                            echo "<option value='".$lista['pac_num_afil']."'>".$lista['pac_nombre']."</option>";
                        }
                    ?>       
                </select>
            </td>  
        <!-- </tr>     
        <tr class="spaceUnder"> -->            
            <td style="width:50% !important;"><input id="motivoIngresoEditHiscli" type="text" class="form-control required" placeholder="Ingresar motivo de ingreso"></input></td>
        </tr>              
        <tr>
            <td style="width:15% !important;">Fecha de alta</td>
            <td>Fecha de baja</td>
            <td style="width:15% !important;">Estado</td>
        </tr>
        <tr class="spaceUnder">
            <td class="paddingRight1" style="width:15% !important;"><input type="date" id="fechaAltaEditHiscli" class="form-control required"></input></td>                    
            <td class="paddingRight1" style="width:15% !important;"><input type="date" id="fechaBajaEditHiscli" class="form-control"></input></td>
            <td><select class="custom-select required" id="estadoEditHiscli" onchange="buscarInternaciones()">
            <option value="Vigente" selected>Vigente</option>
            <option value="Internado">Internado</option>
            <option value="Alta">Alta</option>
            </select></td>
        </tr>
        <tr>
            <td style="vertical-align:top !important;">Descripción</td>
        </tr>
        <tr>
            <td colspan="5">
                <textarea rows="4" cols="50" class="form-control" id="descripcionEditHiscli" placeholder="Ingresar descripción"></textarea>
            </td>
        </tr>  
        <input type="text" id="cabecera" hidden disabled></input>          
    </table>
    </form>
<hr>    
<div class="divInternaciones" id="divInternaciones">
    <h3 id="titleInternaciones" hidden>Internaciones</h3>
</div>       
<hr>

</div>
<div class="divPrestaciones" id="divPrestaciones"></div>
<div id="divBtnAgregarPrestacion"><button style="margin:0em 0em 0.5em 0em !important;" type="button" onclick="addRowPrestacionHistoriasClinicas()" class="btn btn-success"><i class="fa fa-plus-circle"></i></span>  Agregar Prestacion</button></div>
<hr>
<div class="botonesNuevaHistoriaClinica">
    <button type="button" onclick="guardarHisCliCabEdit()" id="btnGuardarEdit" class="btn btn-primary">Guardar</button>
    <button type="button" onclick="cargarHistoriasClinicas()" id="btnCancelar" class="btn btn-secondary">Cancelar</button>
    <button type="button" onclick="calcularFacturacion()" class="btn btn-danger" >Facturar</button>
</div>


  

<script src="js/fc_historias_clinicas.js"></script>
<script src="js/fc_facturacion_pac.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">            