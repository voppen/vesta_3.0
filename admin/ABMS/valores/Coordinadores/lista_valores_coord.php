
<?php

session_start();
include('../../../server/db_connect.php');
include('../../listaPeriodos.php');


$sqlCoordinadores = "SELECT * FROM coordinadores order by coord_nombre";
$coordinadores=mysqli_query($enlace, $sqlCoordinadores);

$sqlFinanciadores = "SELECT * FROM financiadores order by finan_nombre";
$financiadores=mysqli_query($enlace, $sqlFinanciadores);

?>

<div class="container" style="width:120%">    
    <div class="titleForm">Valores Coordinador</div>
    <hr>
    <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#modalValor"><i class="fa fa-plus-circle"></i></span>  Nuevo Valor</button></td></tr>
    <hr>
    <table>
        <tr>    
            <td>
                <select class="custom-select selectTabla" id="idCoordinador">
                <option selected hidden value="Todos">Seleccionar Coordinador</option>  
                <option value="Todos">Todos</option>
                    <?php
                        while($coordinador=mysqli_fetch_assoc($coordinadores))
                        {
                            echo "<option value='".$coordinador['coord_id']."'>".$coordinador['coord_nombre']."</option>";
                        }
                    ?>       
                </select>
            </td>   
            <td>
                <select class="custom-select selectTabla" id="idFinanciador">
                <option selected hidden value="Todos">Seleccionar Financiador</option>  
                <option value="Todos">Todos</option>
                    <?php
                        while($financiador=mysqli_fetch_assoc($financiadores))
                        {
                            echo "<option value='".$financiador['finan_id']."'>".$financiador['finan_nombre']."</option>";
                        }
                    ?>       
                </select>
            </td>    

            <td>
                <select class="custom-select selectTabla" id="anio"> 
                <option selected hidden value="Todos">Año</option>  
                    <option value="Todos">Todos</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>    
                </select>
            </td>   
            <td>
                <select class="custom-select selectTabla" id="periodo">  
                <option selected hidden value="Todos">Período</option>  
                <option value="Todos">Todos</option>
                    <?php
                        foreach ($listaPeriodos as $v)  
                        {   
                            echo "<option value='".$v->id."'>".$v->mes."</option>";
                        }
                    ?>       
                </select>
            </td>        
            <td><button type="button" onclick="cargarValoresPorCoordinador()" class="btn btn-primary"><i class="fa fa-search"></i></span>  Buscar</button></td>
        </tr>
    </table>    
    <div id="tablaValores"></div>
</div>

<!-- MODAL PARA NUEVO -->
<div class="modal fade" id="modalValor" role="dialog">  
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:50em !important; margin:6em 0em 0em -8em !important;">
            <div class="modal-header">
                <h4 class="modal-title" style="padding-left:2em !important;">Cargar Nuevo Valor</h5>
                <button type="button" class="close"  onclick="borrarCamposValoresCoord()" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">  
            <input type="" id="numeroId" value="0" hidden>              
                <form method="post" action="" id="form" name="form" style="padding:0em 3em 0em 3em !important;">    
                <table>
                    <tr>
                        <td><label for="">Año</label></td>    
                        <td>
                            <select class="custom-select" id="anioModal">                                 
                                <option value="">Seleccionar</option>
                                <option value="2018">2018</option>
                                <option value="2017">2017</option>                                            
                            </select>
                        </td>   
                        <td style="width:25% !important; padding-left:2em !important;"><label for="">Periodo</label></td>    
                        <td style="width:25% !important;">
                            <select class="custom-select" id="periodoModal">  
                            <option value="">Seleccionar</option>
                                <?php
                                    foreach ($listaPeriodos as $v)  
                                    {   
                                        echo "<option value='".$v->id."'>".$v->mes."</option>";
                                    }
                                ?>       
                            </select>
                        </td>       
                    </tr>
                    <?php
                        $sqlCoordinadores = "SELECT * FROM coordinadores order by coord_nombre";
                        $coordinadores=mysqli_query($enlace, $sqlCoordinadores);
                    ?>
                    <tr>
                        <td><label>Coordinador</label></td>
                        <td colspan="3">
                            <select class="custom-select" id="idCoordinadorModal">                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($coordinador=mysqli_fetch_assoc($coordinadores))
                                    {
                                        echo "<option value='".$coordinador['coord_id']."'>".$coordinador['coord_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                        </td>   
                    </tr>
                    <?php
                        $sqlFinanciadores = "SELECT * FROM financiadores order by finan_nombre";
                        $financiadores=mysqli_query($enlace, $sqlFinanciadores);
                    ?>
                    <tr>
                        <td><label for="">Financiador</label></td>
                        <td colspan="3">
                            <select class="custom-select" id="idFinanciadorModal">                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($financiador=mysqli_fetch_assoc($financiadores))
                                    {
                                        echo "<option value='".$financiador['finan_id']."'>".$financiador['finan_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                        </td>    
                    </tr>
                </table>
                <hr>
                <div><button style="margin:0em 0em 0.5em 0em !important;" type="button" onclick="addRow()" class="btn btn-success"><i class="fa fa-plus-circle"></i></span>  Agregar Prestacion</button></div>
                <div id="rows">                                          
                </div>
                <div class="modal-footer">
                        <button type="button" onclick="guardarValores()" id="btnGuardar" class="btn btn-primary" disabled="disabled">Guardar</button>
                        <button type="button" onclick="borrarCamposValoresCoord()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
                </form>                     
            </div>
        </div>   
    </div>
</div>






<!-- MODAL PARA EDITAR -->
<div class="modal fade" id="modalFinanciadorEdit" role="dialog">  
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:50em !important; margin:6em 0em 0em -8em !important;">
            <div class="modal-header">
                <h4 class="modal-title" style="padding-left:2em !important;">Editar Valor</h5>
                <button type="button" class="close"  onclick="borrarCamposValoresCoord()" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">  
            <input type="" id="numeroIdEdit" value="0" hidden>              
            <input type="" id="idCabModalEdit" hidden>              
                <form method="post" action="" id="form" name="form" style="padding:0em 3em 0em 3em !important;">    
                <table>
                    <tr>
                        <td><label for="">Año</label></td>    
                        <input type="text" id="anioHidden" hidden>   
                        <td>
                            <select class="custom-select" id="anioModalEdit">                                 
                                <option value="">Seleccionar</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>                                            
                            </select>
                        </td>   
                        <td style="width:25% !important; padding-left:2em !important;"><label for="">Periodo</label></td> 
                        <input type="text" id="periodoHidden" hidden>   
                        <td style="width:25% !important;">
                            <select class="custom-select" id="periodoModalEdit" onchange="habilitarCamposEdit()">  
                            <option value="">Seleccionar</option>
                                <?php
                                    foreach ($listaPeriodos as $v)  
                                    {   
                                        echo "<option value='".$v->id."'>".$v->mes."</option>";
                                    }
                                ?>       
                            </select>
                        </td>       
                    </tr>
                    <?php
                        $sqlCoordinadores = "SELECT * FROM coordinadores order by coord_nombre";
                        $coordinadores=mysqli_query($enlace, $sqlCoordinadores);
                    ?>
                    <tr>
                        <td><label>Coordinador</label></td>
                        <td colspan="3">
                            <select class="custom-select" id="idCoordinadorModalEdit" disabled>                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($coordinador=mysqli_fetch_assoc($coordinadores))
                                    {
                                        echo "<option value='".$coordinador['coord_id']."'>".$coordinador['coord_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                        </td>   
                    </tr>
                    <?php
                        $sqlFinanciadores = "SELECT * FROM financiadores order by finan_nombre";
                        $financiadores=mysqli_query($enlace, $sqlFinanciadores);
                    ?>
                    <tr>
                        <td><label for="">Financiador</label></td>
                        <td colspan="3">
                            <select class="custom-select" id="idFinanciadorModalEdit" disabled>                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($financiador=mysqli_fetch_assoc($financiadores))
                                    {
                                        echo "<option value='".$financiador['finan_id']."'>".$financiador['finan_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                        </td>    
                    </tr>
                </table>
                <hr>
                <div><button style="margin:0em 0em 0.5em 0em !important;" type="button" onclick="addRowEdit()" class="btn btn-success"><i class="fa fa-plus-circle"></i></span>  Agregar Prestacion</button></div>
                <div id="rowsEdit">                                          
                </div>
                <div class="modal-footer">
                        <button type="button" onclick="validarCondicionGuardadoEdit()" id="btnGuardarEdit" class="btn btn-primary" disabled="disabled">Guardar</button>
                        <button type="button" onclick="borrarCamposValoresCoord()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
                </form>                     
            </div>
        </div>   
    </div>
</div>








<!-- MODAL PARA DELETE -->
<div class="modal fade" id="modalDetalleValorCoordinadorDelete" role="dialog">  
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:40em !important; margin:6em 0em 0em -4em !important;">
            <div class="modal-header">
                <h4 class="modal-title" style="padding-left:2em !important;">¿Borrar Todos los Valores?</h5>
                <button type="button" class="close"  onclick="borrarCamposValoresCoord()" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">  
            <input type="" id="numeroIdDelete" value="0" hidden>              
            <input type="" id="idCabModalDelete" hidden>              
                <form method="post" action="" id="form" name="form" style="padding:0em 3em 0em 3em !important;">    
                <table>
                    <tr>
                        <td><label for="">Año</label></td>    
                        <input type="text" id="anioHidden" hidden>   
                        <td>
                            <select class="custom-select" id="anioModalDelete">                                 
                                <option value="">Seleccionar</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>                                            
                            </select>
                        </td>   
                        <td style="width:25% !important; padding-left:2em !important;"><label for="">Periodo</label></td> 
                        <input type="text" id="periodoHidden" hidden>   
                        <td style="width:25% !important;">
                            <select class="custom-select" id="periodoModalDelete">  
                            <option value="">Seleccionar</option>
                                <?php
                                    foreach ($listaPeriodos as $v)  
                                    {   
                                        echo "<option value='".$v->id."'>".$v->mes."</option>";
                                    }
                                ?>       
                            </select>
                        </td>       
                    </tr>
                    <?php
                        $sqlCoordinadores = "SELECT * FROM coordinadores order by coord_nombre";
                        $coordinadores=mysqli_query($enlace, $sqlCoordinadores);
                    ?>
                    <tr>
                        <td><label>Coordinador</label></td>
                        <td colspan="3">
                            <select class="custom-select" id="idCoordinadorModalDelete" disabled>                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($coordinador=mysqli_fetch_assoc($coordinadores))
                                    {
                                        echo "<option value='".$coordinador['coord_id']."'>".$coordinador['coord_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                        </td>   
                    </tr>
                    <?php
                        $sqlFinanciadores = "SELECT * FROM financiadores order by finan_nombre";
                        $financiadores=mysqli_query($enlace, $sqlFinanciadores);
                    ?>
                    <tr>
                        <td><label for="">Financiador</label></td>
                        <td colspan="3">
                            <select class="custom-select" id="idFinanciadorModalDelete" disabled>                                    
                            <option value="">Seleccionar</option>
                                <?php
                                    while($financiador=mysqli_fetch_assoc($financiadores))
                                    {
                                        echo "<option value='".$financiador['finan_id']."'>".$financiador['finan_nombre']."</option>";
                                    }
                                ?>       
                            </select>
                        </td>    
                    </tr>
                </table>
                <!-- <hr> -->
                <!-- <div><button style="margin:0em 0em 0.5em 0em !important;" type="button" onclick="addRowEdit()" class="btn btn-success"><i class="fa fa-plus-circle"></i></span>  Agregar Prestacion</button></div> -->
                <div id="rowsDelete">                                          
                </div>
                <div class="modal-footer">
                        <button type="button" onclick="borrarValoresCoordinador()" id="btnDeleteValor" class="btn btn-danger">Borrar Valores</button>
                        <button type="button" onclick="borrarCamposValoresCoord()" id="btnDeleteValorAceptar" class="btn btn-success" data-dismiss="modal">Aceptar</button>
                        <button type="button" onclick="borrarCamposValoresCoord()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
                </form>                     
            </div>
        </div>   
    </div>
</div>   
    
    <script src="js/fc_valores_coord.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">            
