<?php

    include('../../../server/db_connect.php'); 
    session_start();
    include('../../../server/forbidden.php');

    $anio = $_POST['anio'];
    $periodo = $_POST['periodo'];
    $financiador = $_POST['financiador'];    
    $filtro = "WHERE val_id_coord='0'";


    if($financiador != "Todos")
    {
        $filtro = $filtro."AND val_id_finan='$financiador'";       
    }
    
    if($anio !== "Todos" && $periodo !== "Todos")
    {
        $filtro = $filtro."AND val_anio='$anio' and val_periodo='$periodo'";               
    }
    else
    {
        if($anio !== "Todos")
        {
            $filtro = $filtro."AND val_anio='$anio'";              
        }
        elseif($periodo !== "Todos")
        {
            $filtro = $filtro."AND val_periodo='$periodo'";
        }
    }        

    $sqlSelect = "SELECT * FROM valores_cab inner join valores_det on val_id=val_det_id_cab inner join financiadores on finan_id=val_id_finan inner join lista_prestaciones on prest_id=val_det_id_prest inner join lista_periodos on id_periodo=val_periodo $filtro order by val_anio, val_periodo, prest_nombre";            
    $detalles=mysqli_query($enlace, $sqlSelect);            
?>

<div class="container" style="padding-left:0px">
    <div class="formulario">            
    <table class="table table-hover">
    <thead>
    <tr>
        <!-- <th scope="col">#</th>         -->
        <th scope="col">Año</th>    
        <th scope="col">Periodo</th>    
        <th scope="col">Financiador</th>                                       
        <th scope="col">Prestacion</th>
        <th scope="col">AMBA</th>
        <th scope="col">2° Cordón</th>
        <th scope="col">3° Cordón</th>
        <th scope="col"></th>
    </tr>
  </thead>            
        <tbody id="camposValoresFinanciador">

        <?php 
            $i = 0;                        
            $ultimoIdCab = "";
            while($fila=mysqli_fetch_assoc($detalles))
            {
                if($ultimoIdCab == "" || $ultimoIdCab != $fila["val_id"])
                {
                    $ultimoIdCab = $fila["val_id"];
        ?>
                    <input hidden for="" id="idCab" value="<?php echo(utf8_encode($fila["val_id"])); ?>"/>
                    <!-- <tr style="border-top: 0.13em solid black"> -->
                        <!-- <td>    
                            <label for="" value="<?php echo(utf8_encode($fila["val_id"])); ?>"><?php echo(utf8_encode($fila["val_id"])); ?></label>                
                        </td> -->
                        <td>    
                            <label for="" value="<?php echo(utf8_encode($fila["val_anio"])); ?>"><?php echo(utf8_encode($fila["val_anio"])); ?></label>                
                        </td>
                        <td>    
                            <label for="" value="<?php echo(utf8_encode($fila["nombre_periodo"])); ?>"><?php echo(utf8_encode($fila["nombre_periodo"])); ?></label>                
                        </td>
                        <td>    
                        <label for="" value="<?php echo(utf8_encode($fila["finan_nombre"])); ?>"><?php echo(utf8_encode($fila["finan_nombre"])); ?></label>                
                        </td>
                        <td>
                            <label for="" value="<?php echo(utf8_encode($fila["prest_nombre"])); ?>"><?php echo(utf8_encode($fila["prest_nombre"])); ?></label>                
                        </td>
                        <td>
                            <label for="" id="finan_<?php echo(utf8_encode($fila["val_det_valor_amba"])); ?>"><?php echo(utf8_encode($fila["val_det_valor_amba"])); ?></label>                                
                        </td>
                        <td>
                            <label for="" id="finan_<?php echo(utf8_encode($fila["val_det_valor_cordUno"])); ?>"><?php echo(utf8_encode($fila["val_det_valor_cordUno"])); ?></label>                                
                        </td>
                        <td>
                            <label for="" id="finan_<?php echo(utf8_encode($fila["val_det_valor_cordDos"])); ?>"><?php echo(utf8_encode($fila["val_det_valor_cordDos"])); ?></label>                                
                        </td>
                        <td style="text-align:right">
                            <button type="button" data-toggle="modal" data-target="#modalFinanciadorEdit" data-id="<?php echo($fila["val_id"]); ?>" onclick="cargarValoresEditar(<?php echo(utf8_encode($fila["val_id"])); ?>)" class="btn btn-sm btn-primary form_edit" id="<?php echo(utf8_encode($fila["val_det_id"])); ?>"><i class="fa fa-edit"></i>  Editar</button>                                                            
                            <button type="button" data-toggle="modal" data-target="#modalDetalleValorFinanciadorDelete" class="btn btn-sm btn-danger form_edit" onclick="cargarValoresDeleteModal(<?php echo(utf8_encode($fila["val_id"])); ?>)" id="<?php echo(utf8_encode($fila["val_det_id"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button>
                            
                        </td>            
                    </tr>

        <?php
                }
                else
                {
        ?>
                    <input hidden for="" id="idCab" value="<?php echo(utf8_encode($fila["val_id"])); ?>"/>
                    <!-- <tr style="border-bottom: 0.13em solid black"> -->
                    
                        <!-- <td>    
                            <label for="" value="<?php echo(utf8_encode($fila["val_id"])); ?>"><?php echo(utf8_encode($fila["val_id"])); ?></label>                
                        </td> -->
                        <td>    
                            <label for=""></label>                
                        </td>
                        <td>    
                            <label for=""></label>                
                        </td>
                        <td>
                            <label for="" ></label>                                    
                        </td>
                        <!-- <td>    
                        <label for=""></label>                
                        </td> -->
                        <td>
                            <label for="" value="<?php echo(utf8_encode($fila["prest_nombre"])); ?>"><?php echo(utf8_encode($fila["prest_nombre"])); ?></label>                
                        </td>
                        <td>
                            <label for="" id="finan_<?php echo(utf8_encode($fila["val_det_valor_amba"])); ?>"><?php echo(utf8_encode($fila["val_det_valor_amba"])); ?></label>                                
                        </td>
                        <td>
                            <label for="" id="finan_<?php echo(utf8_encode($fila["val_det_valor_cordUno"])); ?>"><?php echo(utf8_encode($fila["val_det_valor_cordUno"])); ?></label>                                
                        </td>
                        <td>
                            <label for="" id="finan_<?php echo(utf8_encode($fila["val_det_valor_cordDos"])); ?>"><?php echo(utf8_encode($fila["val_det_valor_cordDos"])); ?></label>                                
                        </td>
                        <td style="text-align:right">
                            <!-- <button type="button" data-toggle="modal" data-target="#modalFinanciadorEdit" data-id="<?php echo($fila["val_id"]); ?>" onclick="cargarValoresEditar(<?php echo(utf8_encode($fila["val_id"])); ?>)" class="btn btn-sm btn-primary form_edit" id="<?php echo(utf8_encode($fila["val_det_id"])); ?>"><i class="fa fa-edit"></i>  Editar</button>                                                            
                            <button type="button" data-toggle="modal" data-target="#modalFinanciadorDelete" class="btn btn-sm btn-danger form_edit" id="<?php echo(utf8_encode($fila["val_det_id"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button> -->
                            
                        </td>            
                        </tr>
        <?php
                }		
                $i ++;
                }			
            ?>
        </tbody>
    </table>
    </div>

  </div>



