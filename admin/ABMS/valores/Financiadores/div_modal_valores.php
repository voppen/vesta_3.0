<?php 
    
    include('../../../server/db_connect.php');

    $sqlSelect = "SELECT * FROM lista_prestaciones";    
    $listaPrestaciones = mysqli_query($enlace, $sqlSelect);  
?>

<div>
<hr id="hrModal">
<form action="" method="post">
    <table id="tableRows">        
        <tr>
            <td><label style="margin-right:2em !important;" for="">Prestacion</label></td>
            <td><select class="custom-select" id="selectPrestacion_" onchange="setId()">  
            <option value="Seleccionar">Seleccionar</option>            
                <?php                               
                     while($prestacion=mysqli_fetch_assoc($listaPrestaciones))
                     {
                         echo "<option value='".$prestacion['prest_id']."'>".$prestacion['prest_nombre']."</option>";
                     }
                ?>       
            </select></td>
        </tr>    
        <tr>
            <td style="width:40% !important;"><label for="">AMBA</label></td>                                
            <td><input type="text" class="form-control" id="val_amba"></input></td>
        <tr>
            <td><label for="">2° Cordon</label></td>                                
            <td><input type="text" class="form-control" id="val_cord1"></input></td>
        </tr>
        <tr>
            <td><label for="">3° Cordon</label></td>                                
            <td><input type="text" class="form-control" id="val_cord2"></input></td>
        </tr>        
    </table>
    </form>
</div>
</div>