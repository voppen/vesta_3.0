<?php 
include('../../listaPrestaciones.php');
?>
<div>
<hr id="hrModal">
<form action="" method="post">
    <table id="tableRows">        
    <input type="" id="idDetModalDelete" hidden>  
        <tr>
            <td><label style="margin-right:2em !important;" for="">Prestacion</label></td>
            <td><select class="custom-select" id="selectPrestacionDelete">  
            <option value="Seleccionar">Seleccionar</option>
                            <?php
                                foreach ($listaPrestaciones as $v)  
                                {   
                                    echo "<option value='".$v->id."'>".$v->nombre."</option>";
                                }
                            ?>       
            </select></td>
            <td><button style="margin:0em 0em 0em 2em !important;" id="btnBorrarPrestacion" type="button" onclick="borrarPrestacion(this)" class="btn btn-danger"><i class="fa fa-times-octagon"></i></span> Eliminar Prestacion</button></td>
        </tr>    
        <tr>
            <td style="width:40% !important;"><label for="">AMBA</label></td>                                
            <td><input type="text" class="form-control" id="val_ambaDelete"></input></td>
        <tr>
            <td><label for="">2° Cordon</label></td>                                
            <td><input type="text" class="form-control" id="val_cord1Delete"></input></td>
        </tr>
        <tr>
            <td><label for="">3° Cordon</label></td>                                
            <td><input type="text" class="form-control" id="val_cord2Delete"></input></td>
        </tr>        
    </table>
    </form>
</div>
</div>