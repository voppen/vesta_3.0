<?php 
    include('../../../server/db_connect.php');

    $sqlSelect = "SELECT * FROM lista_prestaciones";    
    $listaPrestaciones = mysqli_query($enlace, $sqlSelect); 
?>
<div>
<hr id="hrModal">
<form action="" method="post">
    <table id="tableRows">        
    <input type="" id="idDetModalEdit" hidden>  
        <tr>
            <td style="width:15% !important;"><label style="margin-right:2em !important;" for="">Prestacion</label></td>
            <td style="width:25% !important; padding-right:2em">
            <select class="custom-select" id="selectPrestacionEdit_" disabled>  
                <option value="Seleccionar">Seleccionar</option>
                                <?php
                                    while($prestacion=mysqli_fetch_assoc($listaPrestaciones))
                                    {
                                        echo "<option value='".$prestacion['prest_id']."'>".$prestacion['prest_nombre']."</option>";
                                    }
                                ?>       
            </select></td>
            <td style="width:15%">Total </td>
        </tr>    
        <tr>
            <td><label for="">AMBA</label></td>              
            <input type="" id="val_ambaEditHidden" hidden>                    
            <td style="padding-right:2em"><input type="text" class="form-control" id="val_ambaEditFinanciador"></input></td>
            <td style="padding-right:2em"><input type="text"class="form-control" id="val_ambaEditTotal"></input></td>
            <input type="" id="val_ambaNuevo" hidden> 
        <tr>
            <td><label for="">2° Cordon</label></td>           
            <input type="" id="val_cord1EditHidden" hidden>                                         
            <td style="padding-right:2em"><input type="text" class="form-control" id="val_cord1Edit"></input></td>
            <td style="padding-right:2em"><input type="text"class="form-control" id="val_cord1EditTotal"></input></td>
            <input type="" id="val_cord1Nuevo" hidden> 
        </tr>
        <tr>
            <td><label for="">3° Cordon</label></td>       
            <input type="" id="val_cord2EditHidden" hidden>                                             
            <td style="padding-right:2em"><input type="text" class="form-control" id="val_cord2Edit"></input></td>
            <td style="padding-right:2em"><input type="text"class="form-control" id="val_cord2EditTotal"></input></td>
            <input type="" id="val_cord2Nuevo" hidden> 
        </tr>                   
    </table>
    </form>
</div>
</div>