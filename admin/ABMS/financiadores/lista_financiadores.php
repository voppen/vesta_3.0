
<?php

include('../../server/db_connect.php');
session_start();
include('../../server/forbidden.php');

$sqlFinanciadores="SELECT * FROM financiadores";
$financiadores=mysqli_query($enlace, $sqlFinanciadores);
?>

<div class="container">
<div class="titleForm">Financiadores</div>
<button type="button" class="btn btn-success"  data-toggle="modal" data-target="#modalFinanciador"><i class="fa fa-plus-circle"></i></span>  Nuevo financiador</button>
    <div class="formulario">    
        

    <table class="table table-hover">
    <thead>
    <tr>
      <th scope="col">#</th>
      <th colspan=3 scope="col">Nombre</th>
      
    </tr>
  </thead>            
        <tbody id="camposFinanciadores">

        <?php 
                $i = 0;
                /* INICIO DEL MUESTREO */
                while($fila=mysqli_fetch_assoc($financiadores))
                {
        ?>
            <tr>
            <td>
                <label for="" value="<?php echo(utf8_encode($fila["finan_id"])); ?>" value="<?php echo(utf8_encode($fila["finan_id"])); ?>"><?php echo(utf8_encode($fila["finan_id"])); ?></label>                
            </td>
            <td>
                <label for="" id="finan_<?php echo(utf8_encode($fila["finan_id"])); ?>" value="<?php echo(utf8_encode($fila["finan_nombre"])); ?>"><?php echo(utf8_encode($fila["finan_nombre"])); ?></label>                
            </td>
            <td style="text-align:right">
                <button type="button" data-toggle="modal" data-target="#modalFinanciador" class="btn btn-sm btn-primary form_edit" id="<?php echo(utf8_encode($fila["finan_id"])); ?>"><i class="fa fa-edit"></i>  Editar</button>                                            
                <button type="button" data-toggle="modal" data-target="#modalFinanciadorDelete" class="btn btn-sm btn-danger form_edit" id="<?php echo(utf8_encode($fila["finan_id"])); ?>"><i class="fa fa-trash"></i>  Eliminar</button>
                
            </td>            
        </tr>
        <?php			
                $i ++;
                }			
            ?>
        </tbody>
    </table>
    </div>

  </div>

  <!-- MODAL PARA NUEVO/EDITAR -->
  <div class="modal fade" id="modalFinanciador" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo Financiador</h5>
                        <button type="button" class="close"  onclick="borrarCampos()" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                        <form method="post" id="form" name="form">                                                   
                            <p>Nombre</p>
                            <input type="text" id="finan_nombre_edit" class="form-control" name="finan_nombre_edit"  value="<?php echo(utf8_encode($fila["finan_nombre"])); ?>"></input>
                            <div id="errorMsg" class="alert alert-danger" role="alert" style="display:none;"></div>
                            <input type="text" id="finan_id_edit" name="finan_id_edit" value="<?php echo(utf8_encode($fila["finan_id"])); ?>" hidden disabled></label>                        
                    </div>
                            <div class="modal-footer">
                                    <button type="button" onclick="modificarFinanciador('guardar')" class="btn btn-primary">Guardar</button>
                                    <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>                     
            </div>
          </div>   
    </div>

    <!-- MODAL PARA DELETE -->
    <div class="modal fade" id="modalFinanciadorDelete" role="dialog">  
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Borrar Financiador?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                
                    <form method="post" action="" id="form" name="form">                                            
                            <p>Nombre</p>
                            <input disabled type="text" id="finan_nombre_edit" class="form-control" name="finan_nombre_edit" ></input>
                            <input type="text" id="finan_id_edit" name="finan_id_edit" hidden disabled></label>                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="modificarFinanciador('borrar')" class="btn btn-primary">Aceptar</button>
                            <button type="button" onclick="borrarCampos()" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>    
    
    <script src="js/fc_financiadores.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>