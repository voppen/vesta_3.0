<?php

include('../../server/db_connect.php'); 
session_start();
include('../../server/forbidden.php');

// $idPaciente = '1';
// $anio = '2018';
// $periodo = '7';


$idPaciente = $_POST['paciente'];
$anio = $_POST['anio'];
$periodo = $_POST['periodo'];    

$totalPaciente = 0;

?>

<table id="tableFacturacion" class="table table-hover">
        <thead>
        <tr>
            <th>Prestación</th>
            <th>Sesión</th>              
            <th>Hora</th>
            <th>Módulo</th>
            <th>Valor</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
            <?php
               // $sqlModalidad = "SELECT prest_mod FROM `facturacion_pac` inner join lista_prestaciones on fac_id_prest=prest_id WHERE fac_id_pac"
                $sqlFacturacion="SELECT * FROM facturacion_pac 
                inner join pacientes on pac_num_afil=fac_id_pac 
                inner join lista_prestaciones on fac_id_prest=prest_id
                inner join hiscli_cab on hiscli_id_paciente=fac_id_pac            
                inner join hiscli_det on hiscli_id=hiscli_det_cab_id                
                and fac_id_prest=hiscli_det_id_prest
                inner join valores_cab on val_id_finan=pac_finan
                inner join valores_det on val_id=val_det_id_cab and val_det_id_prest=hiscli_det_id_prest
                WHERE fac_periodo='$periodo' AND fac_anio='$anio' AND fac_id_pac='$idPaciente'                
                and val_anio='$anio' and val_periodo='$periodo'
                AND year(hiscli_det_fecha_alta)>='$anio' AND month(hiscli_det_fecha_alta)<='$periodo' AND year(hiscli_det_fecha_baja)<='$anio' 
                AND month(hiscli_det_fecha_baja)>='$periodo'";

                $facturacion=mysqli_query($enlace, $sqlFacturacion);
                while($fila=mysqli_fetch_assoc($facturacion))
                {
                    switch ($fila['pac_cordon']) {
                        case '1':
                            $elCordon = 'val_det_valor_amba';
                            break;
                        case '2':
                            $elCordon = 'val_det_valor_cordUno';
                            break;
                        case '3':
                            $elCordon = 'val_det_valor_cordDos';
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                    $valorCab = $fila['val_id'];
                    $prestDet = $fila['fac_id_prest'];


                    $sqlCordon = "SELECT $elCordon FROM valores_det where val_det_id_cab='$valorCab' and val_det_id_prest='$prestDet'";                   
                    $cordonQuery = mysqli_query($enlace, $sqlCordon)->fetch_object()->$elCordon;

                    $totalPaciente = $totalPaciente + $fila['fac_total'];
            ?>
            <tr>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode($fila["fac_id_prest"])); ?>"><?php echo(utf8_encode($fila["prest_nombre"])); ?></label>                
            </td>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_total_ses"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_total_ses"])); ?></label>                
            </td>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_total_hs"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_total_hs"])); ?></label>                
            </td>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_total_mod"])); ?>"><?php echo(utf8_encode($fila["hiscli_det_total_mod"])); ?></label>                
            </td>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode("sdofi")); ?>"><?php echo('$'.number_format($cordonQuery,0, ',', '.')); ?></label>
            </td>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode($fila["fac_total"])); ?>"><?php echo('$'.number_format($fila["fac_total"],0, ',', '.')); ?></label>                
            </td>
            <?php
                }                                
            ?>
            </tr>
            <tr><td></td><td></td><td></td><td></td><td></td>
            <td>
                <label for="" id="fac_<?php echo(utf8_encode($fila["fac_id"])); ?>" value="<?php echo(utf8_encode($totalPaciente)); ?>"><?php echo('$'.number_format($totalPaciente,0, ',', '.')); ?></label>                
            </td>
            </tr>
        </tbody>
    </table>
    