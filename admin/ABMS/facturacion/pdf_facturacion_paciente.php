
<?php
session_start();
include('../../server/db_connect.php');
include('../../server/forbidden.php');
include('../../fpdf/fpdf.php');

?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=iso-8859-1" http-equiv=Content-Type>
  </head>
  <body>  
  
<?php

//DATOS

$paciente = $_POST['paciente'];
$anio = $_POST['anio'];
$periodo = $_POST['periodo'];

$sqlDatos = "SELECT * FROM hiscli_cab JOIN pacientes ON pac_num_afil= hiscli_id_paciente WHERE hiscli_id_paciente='$paciente'";
$datos = mysqli_query($enlace, $sqlDatos);

$totalAkm = 0;
$totalAkr = 0;
$totalCuidador = 0;
$totalDlmt = 0;
$totalModEnf1 = 0;
$totalModEnf2 = 0;
$totalVisitaEnf = 0;
$totalFono = 0;
$totalKines = 0;
$totalMedClin = 0;
$totalMedEsp = 0;
$totalMedPal = 0;
$totalNut = 0;
$totalTOcup = 0;
$totalGuard4 = 0;
$totalGuard6 = 0;
$totalGuard8 = 0;
$totalGuard12 = 0;  

$Akm = 0;
$Akr = 0;
$Cuidador = 0;
$Dlmt = 0;
$ModEnf1 = 0;
$ModEnf2 = 0;
$VisitaEnf = 0;
$Fono = 0;
$Kines = 0;
$MedClin = 0;
$MedEsp = 0;
$MedPal = 0;
$Nut = 0;
$TOcup = 0;
$Guard4 = 0;
$Guard6 = 0;
$Guard8 = 0;
$Guard12 = 0; 

$ivaAkm = 0;
$ivaAkr = 0;
$ivaCuidador = 0;
$ivaDlmt = 0;
$ivaModEnf1 = 0;
$ivaModEnf2 = 0;
$ivaVisitaEnf = 0;
$ivaFono = 0;
$ivaKines = 0;
$ivaMedClin = 0;
$ivaMedEsp = 0;
$ivaMedPal = 0;
$ivaNut = 0;
$ivaTOcup = 0;
$ivaGuard4 = 0;
$ivaGuard6 = 0;
$ivaGuard8 = 0;
$ivaGuard12 = 0;   

$total = 0;
$totalIva = 0;

while($dato=mysqli_fetch_assoc($datos))
{
    $idHiscli = $dato['hiscli_id'];
    $motivoIngreso = $dato['hiscli_ingreso'];
    $fechaAlta = $dato['hiscli_fecha_alta'];
    $nombrePaciente = $dato['pac_nombre'];    
    $pac_iva = $dato['pac_iva'];
}

$sqlSelectHiscliDet = "SELECT * FROM hiscli_det JOIN lista_prestaciones on hiscli_det_id_prest = prest_id WHERE hiscli_det_cab_id='$idHiscli' AND year(hiscli_det_fecha_alta)>='$anio' AND month(hiscli_det_fecha_alta)<='$periodo' AND year(hiscli_det_fecha_baja)<='$anio' AND month(hiscli_det_fecha_baja)>='$periodo' ORDER BY hiscli_det_id_prest";    
$detalles = mysqli_query($enlace, $sqlSelectHiscliDet); 

while($detalle=mysqli_fetch_assoc($detalles))
{
   $idPrestacion = $detalle['hiscli_det_id_prest'];

   switch ($idPrestacion)
    {
      case '0':    
      $Akm = $detalle['hiscli_det_total_ses'];
      break;    
      case '1':    
      $Akr = $detalle['hiscli_det_total_ses'];
      break;    
      case '2':
      $Cuidador = $detalle['hiscli_det_total_hs'];
      break;    
      case '3':
      $Dlmt = $detalle['hiscli_det_total_ses'];
      break;    
      case '4':
      $ModEnf1 = $detalle['hiscli_det_total_mod'];
      break;    
      case '5':
      $ModEnf2 = $detalle['hiscli_det_total_mod'];
      break;    
      case '6':
      $VisitaEnf = $detalle['hiscli_det_total_hs'];
      break;    
      case '7':
      $Fono = $detalle['hiscli_det_total_ses'];
      break;    
      case '8':
      $Kines = $detalle['hiscli_det_total_ses'];
      break;    
      case '9':
      $MedClin = $detalle['hiscli_det_total_ses'];
      break;    
      case '10':
      $MedEsp = $detalle['hiscli_det_total_ses'];
      break;    
      case '11':
      $MedPal = $detalle['hiscli_det_total_ses'];
      break;    
      case '12':
      $Nut = $detalle['hiscli_det_total_ses'];
      break;    
      case '13':
      $TOcup = $detalle['hiscli_det_total_ses'];
      break;    
      case '14':
      $Guard4 = $detalle['hiscli_det_total_hs'];
      break;    
      case '15':
      $Guard6 = $detalle['hiscli_det_total_hs'];
      break;    
      case '16':
      $Guard8 = $detalle['hiscli_det_total_hs'];
      break;    
      case '17':
      $Guard12 = $detalle['hiscli_det_total_hs'];
      break;                      
   }
}


$sqlFacturacion="SELECT * FROM facturacion_pac 
  inner join pacientes on pac_num_afil=fac_id_pac 
  inner join lista_prestaciones on fac_id_prest=prest_id
  inner join hiscli_cab on hiscli_id_paciente=fac_id_pac
  inner join hiscli_det on hiscli_id=hiscli_det_cab_id
  and fac_id_prest=hiscli_det_id_prest";                

$facturacion=mysqli_query($enlace, $sqlFacturacion);

while($detalleFacturacion=mysqli_fetch_assoc($facturacion))
{
  $idPrestacion = $detalleFacturacion['fac_id_prest'];  

  switch ($idPrestacion) 
  {
    case '0':    
    $totalAkm = $detalleFacturacion['fac_total'];  
    if($pac_iva != 'EXC')  
    {      
      $totalIvaAkm = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaAkm = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaAkm;
      $totalIva = $totalIva + $ivaAkm;
    }
    else
    {
      $total = $total + $totalAkm;
    }      
    
    break;    
    case '1':    
    $totalAkr = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaAkr = $detalleFacturacion['fac_total'] * 0.21;
      $totalIvaAkr = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaAkr;
      $totalIva = $totalIva + $ivaAkr;  
    }
    else
    {
      $total = $total + $totalAkr;
    }      
    break;    
    case '2':
    $totalCuidador = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaCuidador = $detalleFacturacion['fac_total'] * 0.21;
      $totalIvaCuidador = $detalleFacturacion['fac_total'] * 1.21;
      $total = $total + $totalIvaCuidador;
      $totalIva = $totalIva + $ivaCuidador;  
    }
    else
    {
      $total = $total + $totalCuidador;
    }      
    break;    
    case '3':
    $totalDlmt = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaDlmt = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaDlmt = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaDlmt;
      $totalIva = $totalIva + $ivaDlmt;  
    }
    else
    {
      $total = $total + $totalDlmt;
    }      
    break;    
    case '4':
    $totalModEnf1 = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaModEnf1 = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaModEnf1 = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalModEnf1;
      $totalIva = $totalIva + $ivaModEnf1;  
    }
    else
    {
      $total = $total + $totalModEnf1;
    }      
    break;    
    case '5':
    $totalModEnf2 = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaModEnf2 = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaModEnf2 = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaModEnf2;
      $totalIva = $totalIva + $ivaModEnf2;  
    }
    else
    {
      $total = $total + $totalModEnf2;
    }      
    break;    
    case '6':
    $totalVisitaEnf = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaVisitaEnf = $detalleFacturacion['fac_total'];
      $totalIvaVisitaEnf = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaVisitaEnf;
      $totalIva = $totalIva + $ivaVisitaEnf;  
    }
    else
    {
      $total = $total + $totalVisitaEnf;
    }      
    break;    
    case '7':
    $totalFono = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaFono = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaFono = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaFono;
      $totalIva = $totalIva + $ivaFono;  
    }
    else
    {
      $total = $total + $totalFono;
    }      
    break;    
    case '8':
    $totalKines = $detalleFacturacion['fac_total'];
    if($totalIvaKines != 'EXC')  
    {
      $ivaKines = $detalleFacturacion['fac_total'] * 0.21;
      $totalIvaKines = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaKines;
      $totalIva = $totalIva + $ivaKines;  
    }
    else
    {
      $total = $total + $totalKines;
    }      
    break;    
    case '9':
    $totalMedClin = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaMedClin = $detalleFacturacion['fac_total'] * 0.21;
      $totalIvaMedClin = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaMedClin;
      $totalIva = $totalIva + $ivaMedClin;  
    }
    else
    {
      $total = $total + $totalMedClin;
    }      
    break;    
    case '10':
    $totalMedEsp = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaMedEsp = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaMedEsp = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaMedEsp;
      $totalIva = $totalIva + $ivaMedEsp;  
    }
    else
    {
      $total = $total + $totalMedEsp;
    }      
    break;    
    case '11':
    $totalMedPal = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaMedPal = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaMedPal = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaMedPal;
      $totalIva = $totalIva + $ivaMedPal;  
    }
    else
    {
      $total = $total + $totalMedPal;
    }      
    break;    
    case '12':    
    $totalNut = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaNut = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaNut = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaNut;
      $totalIva = $totalIva + $ivaNut;  
    }
    else
    {
      $total = $total + $totalNut;
    }      
    break;    
    case '13':
    $totalTOcup = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaTOcup = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaTOcup = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaTOcup;
      $totalIva = $totalIva + $ivaTOcup;  
    }
    else
    {
      $total = $total + $totalTOcup;
    }      
    break;    
    case '14':
    $totalGuard4 = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaGuard4 = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaGuard4 = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaGuard4;
      $totalIva = $totalIva + $ivaGuard4;  
    }
    else
    {
      $total = $total + $totalGuard4;
    }      
    break;    
    case '15':
    $totalGuard6 = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaGuard6 = $detalleFacturacion['fac_total'] * 0.21; 
      $totalIvaGuard6 = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaGuard6;
      $totalIva = $totalIva + $ivaGuard6;  
    }
    else
    {
      $total = $total + $totalGuard6;
    }      
    break;    
    case '16':
    $totalGuard8 = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaGuard8 = $detalleFacturacion['fac_total'] * 0.21;
      $totalIvaGuard8 = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaGuard8;
      $totalIva = $totalIva + $ivaGuard8;  
    }
    else
    {
      $total = $total + $totalGuard8;
    }      
    break;    
    case '17':
    $totalGuard12 = $detalleFacturacion['fac_total'];
    if($pac_iva != 'EXC')  
    {
      $ivaGuard12 = $detalleFacturacion['fac_total'] * 0.21;
      $totalIvaGuard12 = $detalleFacturacion['fac_total'] * 1.21; 
      $total = $total + $totalIvaGuard12;
      $totalIva = $totalIva + $ivaGuard12;  
    }
    else
    {
      $total = $total + $totalGuard12;
    }      
    break;                              
  }
}


$pdf = new FPDF();
$pdf->AddPage();

$pdf->SetFont('Helvetica', '', 18);		
$pdf->Image('../../imgs/Vesta-Logo.png',50,0,0,50);
$pdf->SetXY(78, 10);

$pdf->line(10,45,200,45);
$pdf->Ln(5);
$pdf->SetTextColor('34', '48', '99');
$pdf->Cell(0,75, $nombrePaciente,0,0,'C');	

$pdf->SetTextColor('0','0','0');
$pdf->SetFont('Helvetica', '', 10);	
$pdf->Ln(45); 
$pdf->Cell(195,7,'HISTORIA CLINICA',1,0,'L');
$pdf->Ln(7); 
$pdf->Cell(20,7,"ID",1,0,'C');
$pdf->Cell(23,7,"Fecha Alta",1,0,'C');
$pdf->Cell(152,7,"Motivo Ingreso",1,0,'C');
$pdf->Ln(7);
$pdf->Cell(20,7,$idHiscli,1,0,'C');
$pdf->Cell(23,7,$fechaAlta,1,0,'C');	
$pdf->Cell(152,7,$motivoIngreso,1,0,'C');	

$pdf->Ln(20); 
$pdf->Cell(195,7,'PRESTACIONES:',1,0,'C');
$pdf->Ln(7);
$pdf->SetTextColor('0','0','0');
$pdf->SetFont('Helvetica', '', 10);	

//CABECEROS
$pdf->Cell(20,16,"",1,0,'C');
$pdf->Cell(155,10,"ENFERMERIA",1,0,'C');				
$pdf->Cell(20,10,"CUIDADOR",1,0,'C');				

$pdf->Ln(1);
$pdf->SetXY(30, 111);
$pdf->Cell(20,6,"Hora",1,0,'C');		
$pdf->Cell(23,6,"Guardia 4hs",1,0,'C');
$pdf->Cell(24,6,"Guardia 6hs",1,0,'C');
$pdf->Cell(24,6,"Guardia 8hs",1,0,'C');
$pdf->Cell(24,6,"Guardia 12hs",1,0,'C');
$pdf->Cell(20,6,"Modulo 1",1,0,'C');
$pdf->Cell(20,6,"Modulo 2",1,0,'C');
$pdf->Cell(20,6,"Cuidador",1,0,'C');

//DETALLE
$pdf->Ln(6);
$pdf->Cell(20,8,"Cantidad",1,0,'C');
$pdf->Cell(20,8,$VisitaEnf,1,0,'C');		
$pdf->Cell(23,8,$Guard4,1,0,'C');
$pdf->Cell(24,8,$Guard6,1,0,'C');
$pdf->Cell(24,8,$Guard8,1,0,'C');
$pdf->Cell(24,8,$Guard12,1,0,'C');
$pdf->Cell(20,8,$ModEnf1,1,0,'C');
$pdf->Cell(20,8,$ModEnf2,1,0,'C');	
$pdf->Cell(20,8,$Cuidador,1,0,'C');

$pdf->Ln(8);
$pdf->Cell(20,8,"Subtotal",1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalVisitaEnf,0, ',', '.'),1,0,'C');		
$pdf->Cell(23,8,'$'.number_format($totalGuard4,0, ',', '.'),1,0,'C');
$pdf->Cell(24,8,'$'.number_format($totalGuard6,0, ',', '.'),1,0,'C');
$pdf->Cell(24,8,'$'.number_format($totalGuard8,0, ',', '.'),1,0,'C');
$pdf->Cell(24,8,'$'.number_format($totalGuard12,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalModEnf1,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalModEnf2,0, ',', '.'),1,0,'C');		
$pdf->Cell(20,8,'$'.number_format($totalCuidador,0, ',', '.'),1,0,'C');
						  
$pdf->Ln(8);
$pdf->Cell(20,8,"IVA 10,5%",1,0,'C');
$pdf->Cell(20,8,'$'.number_format($ivaVisitaEnf,0, ',', '.'),1,0,'C');		
$pdf->Cell(23,8,'$'.number_format($ivaGuard4,0, ',', '.'),1,0,'C');
$pdf->Cell(24,8,'$'.number_format($ivaGuard6,0, ',', '.'),1,0,'C');
$pdf->Cell(24,8,'$'.number_format($ivaGuard8,0, ',', '.'),1,0,'C');
$pdf->Cell(24,8,'$'.number_format($ivaGuard12,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($ivaModEnf1,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($ivaModEnf2,0, ',', '.'),1,0,'C');	
$pdf->Cell(20,8,'$'.number_format($ivaCuidador,0, ',', '.'),1,0,'C');		

$pdf->Ln(8);
$pdf->Cell(20,10,"Subtotal",1,0,'C');
$pdf->Cell(20,10,'$'.number_format($totalVisitaEnf + $ivaVisitaEnf,0, ',', '.'),1,0,'C');		
$pdf->Cell(23,10,'$'.number_format($totalGuard4 + $ivaGuard4,0, ',', '.'),1,0,'C');
$pdf->Cell(24,10,'$'.number_format($totalGuard6 + $ivaGuard6,0, ',', '.'),1,0,'C');
$pdf->Cell(24,10,'$'.number_format($totalGuard8 + $ivaGuard8,0, ',', '.'),1,0,'C');
$pdf->Cell(24,10,'$'.number_format($totalGuard12 + $ivaGuard12,0, ',', '.'),1,0,'C');
$pdf->Cell(20,10,'$'.number_format($totalModEnf1 + $ivaModEnf1,0, ',', '.'),1,0,'C');
$pdf->Cell(20,10,'$'.number_format($totalModEnf2 + $ivaModEnf2,0, ',', '.'),1,0,'C');
$pdf->Cell(20,10,'$'.number_format($totalCuidador + $ivaCuidador,0, ',', '.'),1,0,'C');	
				

//CABECEROS
$pdf->Ln(16);
$pdf->Cell(20,15,"",1,0,'C');		
$pdf->Cell(60,9,"MEDICO",1,0,'C');
$pdf->Cell(40,9,"KINESIOLOGIA",1,0,'C');
$pdf->Cell(25,15,"FONOAUD.",1,0,'C');
$pdf->Cell(25,15,"T. OCUP.",1,0,'C');
$pdf->Cell(25,15,"NUTRIC.",1,0,'C');

$pdf->Ln(16);
$pdf->SetXY(30, 166);	
$pdf->Cell(20,6,"Clinico",1,0,'C');
$pdf->Cell(20,6,"Especialista",1,0,'C');
$pdf->Cell(20,6,"Paliativo",1,0,'C');		
$pdf->Cell(20,6,"Ses.",1,0,'C');
$pdf->Cell(20,6,"DL/MT",1,0,'C');	

//DETALLE

$pdf->Ln(6);
$pdf->Cell(20,8,"Cantidad",1,0,'C');
$pdf->Cell(20,8, $MedClin,1,0,'C');
$pdf->Cell(20,8, $MedEsp,1,0,'C');
$pdf->Cell(20,8, $MedPal,1,0,'C');	
$pdf->Cell(20,8, $Kines,1,0,'C');		
$pdf->Cell(20,8, $Dlmt,1,0,'C');
$pdf->Cell(25,8, $Fono,1,0,'C');							
$pdf->Cell(25,8, $TOcup,1,0,'C');
$pdf->Cell(25,8, $Nut,1,0,'C');

$pdf->Ln(8);  
$pdf->Cell(20,8,"Subtotal",1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalMedClin,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalMedEsp,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalMedPal,0, ',', '.'),1,0,'C');
$pdf->Cell(20,8,'$'.number_format($totalKines,0, ',', '.'),1,0,'C');		
$pdf->Cell(20,8,'$'.number_format($totalDlmt,0, ',', '.'),1,0,'C');
$pdf->Cell(25,8,'$'.number_format($totalFono,0, ',', '.'),1,0,'C');
$pdf->Cell(25,8,'$'.number_format($totalTOcup,0, ',', '.'),1,0,'C');		
$pdf->Cell(25,8,'$'.number_format($totalNut,0, ',', '.'),1,0,'C');

$pdf->Ln(8);
$pdf->Cell(20,8,"IVA 10,5%",1,0,'C');
$pdf->Cell(20,8,'$'.number_format($ivaMedClin,0, ',', '.'),1,0,'C');	
$pdf->Cell(20,8,'$'.number_format($ivaMedEsp,0, ',', '.'),1,0,'C');	
$pdf->Cell(20,8,'$'.number_format($ivaMedPal,0, ',', '.'),1,0,'C');	
$pdf->Cell(20,8,'$'.number_format($ivaKines,0, ',', '.'),1,0,'C');		
$pdf->Cell(20,8,'$'.number_format($ivaDlmt,0, ',', '.'),1,0,'C');
$pdf->Cell(25,8,'$'.number_format($ivaFono,0, ',', '.'),1,0,'C');
$pdf->Cell(25,8,'$'.number_format($ivaTOcup,0, ',', '.'),1,0,'C');				
$pdf->Cell(25,8,'$'.number_format($ivaNut,0, ',', '.'),1,0,'C');				

$pdf->Ln(8);
$pdf->Cell(20,10,"Subtotal",1,0,'C');
$pdf->Cell(20,10,'$'.number_format($totalMedClin + $ivaMedClin,0, ',', '.'),1,0,'C');		
$pdf->Cell(20,10,'$'.number_format($totalMedEsp + $ivaMedEsp,0, ',', '.'),1,0,'C');		
$pdf->Cell(20,10,'$'.number_format($totalMedPal + $ivaMedPal,0, ',', '.'),1,0,'C');
$pdf->Cell(20,10,'$'.number_format($totalKines + $ivaKines,0, ',', '.'),1,0,'C');		
$pdf->Cell(20,10,'$'.number_format($totalDlmt + $ivaDlmt,0, ',', '.'),1,0,'C');
$pdf->Cell(25,10,'$'.number_format($totalFono + $ivaFono,0, ',', '.'),1,0,'C');
$pdf->Cell(25,10,'$'.number_format($totalTOcup + $ivaTOcup,0, ',', '.'),1,0,'C');
$pdf->Cell(25,10,'$'.number_format($totalNut + $ivaNut,0, ',', '.'),1,0,'C');		
$pdf->Ln(12);
$pdf->Cell(195,10,"IVA            $ ". number_format($totalIva,0, ',', '.'),1,0,'L');
$pdf->Ln(12);
$pdf->Cell(195,10,"TOTAL          $ ". number_format($total,0, ',', '.'),1,0,'L');

$pdf->Output("Pre_Factura.pdf",'F');
//echo "<script language='javascript'>window.open('Pre_Factura.pdf','_self','');</script>";//para ver el archivo pdf generado		
exit;		
			
	?>
     
	</body>
</html>


