
<?php

include('../../server/db_connect.php');
session_start();
include('../../server/forbidden.php');
include('../listaPeriodos.php');

$sqlPacientes="SELECT * FROM pacientes";
$pacientes=mysqli_query($enlace, $sqlPacientes);
?>

<div class="container">
<div class="titleForm">Facturación Pacientes</div>
<hr>
    <table>
        <tr>    
            <td>
                <select class="custom-select selectTabla" id="idPaciente">
                <option selected hidden value="">Seleccionar Paciente</option>                  
                    <?php
                        while($paciente=mysqli_fetch_assoc($pacientes))
                        {
                            echo "<option value='".$paciente['pac_num_afil']."'>".$paciente['pac_nombre']."</option>";
                        }
                    ?>       
                </select>
            </td>   
            <td>
                <select class="custom-select selectTabla" id="anio"> 
                <option selected hidden value="">Año</option>                      
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>    
                </select>
            </td>   
            <td>
                <select class="custom-select selectTabla" id="periodo">  
                <option selected hidden value="">Período</option>                  
                    <?php
                        foreach ($listaPeriodos as $v)  
                        {   
                            echo "<option value='".$v->id."'>".$v->mes."</option>";
                        }
                    ?>       
                </select>
            </td>        
            <td><button type="button" onclick="cargarFacturacionPaciente()" class="btn btn-primary"><i class="fa fa-search"></i></span>  Buscar</button></td>
            <td>
                <button type="button" onclick="cargarFacturacionPacientes()" id="btnPDF" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i>  Pdf</button>
            </td>
        </tr>
    </table>   
    <div id="tableFacturacionPacientes" style="margin-top:50px;"></div>

    

    
        

    
    <script src="js/fc_facturacion_pac.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">   