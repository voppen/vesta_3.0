<?php
               
    include('../../server/db_connect.php'); 
    session_start();
    include('../../server/forbidden.php');

    $idFinanciador = $_POST['idFinanciador'];
    $anio = $_POST['anio'];
    $periodo = $_POST['periodo'];    

    // $idFinanciador = '113';
    // $anio = '2018';
    // $periodo = '7';
    $pacienteAnt = '';
    $count = 0;
    $idCabAnt = '';
    
    $total = 0;
    $totalValorPrestacion = 0;
        
    $sqlFacturacionFinanciadores = "SELECT * FROM hiscli_det 
    JOIN hiscli_cab on hiscli_id=hiscli_det_cab_id
    JOIN pacientes on hiscli_id_paciente=pac_num_afil
    JOIN lista_prestaciones on hiscli_det_id_prest = prest_id 
    where pac_finan='$idFinanciador'
    AND year(hiscli_det_fecha_alta)>='$anio' AND month(hiscli_det_fecha_alta)<='$periodo' 
    AND year(hiscli_det_fecha_baja)<='$anio' AND month(hiscli_det_fecha_baja)>='$periodo'";

    $facturaciones = mysqli_query($enlace, $sqlFacturacionFinanciadores);    
    $cantRowsFacturacion = mysqli_num_rows($facturaciones);    
?>

<table id="tableFacturacion" class="table table-hover">
    <thead>
    <tr>
        <th>Paciente</th>
        <th></th><th></th><th></th>
        <th style="text-align:right !important;">Total</th>
        <th></th>
    </tr>
    </thead>        

<?php

while($facturacion=mysqli_fetch_assoc($facturaciones))
{  
    $count++;
    $valCordon = $facturacion['pac_cordon'];
    $idPrest = $facturacion['hiscli_det_id_prest'];

    $sqlValores = "SELECT * FROM valores_cab 
    join valores_det on val_id=val_det_id_cab 
    where val_anio='$anio' and val_periodo='$periodo'        
    and val_id_finan='$idFinanciador'
    and val_det_id_prest = '$idPrest'";

    $valoresPrestacion = mysqli_query($enlace, $sqlValores);         
    while($valorPrestacion = mysqli_fetch_assoc($valoresPrestacion))
    {
        switch ($facturacion['pac_cordon']) 
        {
            case '1':
                $valor = $valorPrestacion['val_det_valor_amba'];
                break;
            case '2':
                $valor = $valorPrestacion['val_det_valor_cordUno'];
                break;
            case '3':
                $valor = $valorPrestacion['val_det_valor_cordDos'];
                break;
        }
    }

    $paciente = $facturacion['pac_nombre'];  
    $hsPrestacion = $facturacion['hiscli_det_total_mod'] + $facturacion['hiscli_det_total_ses'] + $facturacion['hiscli_det_total_hs'];          
    $valorPrestacion = $hsPrestacion * $valor;
      

    if($paciente != $pacienteAnt)
    {
        if($pacienteAnt != '')
        {
            $total = $total + $totalValorPrestacion;
?>                    
            <td style="text-align:right !important;"><label for="" id="fac_<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>"><?php echo('$'.number_format($totalValorPrestacion,0, ',', '.')); ?></label></td>        
            <td style="text-align:right !important;"><button type="button" onclick="buscarDetallesFacturacionFinanciador(<?php echo(utf8_encode($idCabAnt)); ?>)" class="btn btn-success"><i class="fa fa-plus-circle"></i></span>  Detalles</button></td>
            </tr>
            <tr>
            <td></td>
            <td colspan="3" style="visibilidad:hidden"><div id="tablaDetallesFinanciador_<?php echo(utf8_encode($idCabAnt)); ?>"></div></td>
            <td></td><td></td>
            </tr>
            <tr>    
                <td><label for="" id="fac_<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>"><?php echo(utf8_encode($paciente)); ?></label></td>
                <td></td><td></td><td></td>                
<?php                
        }
        else
        {
?>
            <tr>    
                <td><label for="" id="fac_<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>"><?php echo(utf8_encode($paciente)); ?></label></td>
                <td></td><td></td><td></td>        
<?php             
        }        
    
        $totalValorPrestacion = $valorPrestacion;
        $pacienteAnt = $paciente;
        $idCabAnt = $facturacion["hiscli_id"];

    }
    else
    {
        
        // $total = $total + $totalValorPrestacion;
        if($count == $cantRowsFacturacion)
        {
            $totalValorPrestacion = $totalValorPrestacion + $valorPrestacion;
            $total = $total + $totalValorPrestacion;
?>                    
            <td style="text-align:right !important;"><label for="" id="fac_<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($facturacion["hiscli_det_id_prest"])); ?>"><?php echo('$'.number_format($totalValorPrestacion,0, ',', '.')); ?></label></td>        
            <td style="text-align:right !important;"><button type="button" onclick="buscarDetallesFacturacionFinanciador(<?php echo(utf8_encode($idCabAnt)); ?>)" class="btn btn-success"><i class="fa fa-plus-circle"></i></span>  Detalles</button></td>
            </tr>
            <tr>
            <td></td>
            <td colspan="7" style="visibilidad:hidden"><div id="tablaDetallesFinanciador_<?php echo(utf8_encode($facturacion["hiscli_id"])); ?>"></div></td>
            </tr>
<?php
        }
    }
    }    
?>

<tr><td></td><td></td><td></td><td></td><td style="text-align:right !important;"><?php echo('$'.number_format($total,0, ',', '.')); ?></td></tr>