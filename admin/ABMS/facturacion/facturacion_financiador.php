
<?php

include('../../server/db_connect.php');
session_start();
include('../../server/forbidden.php');
include('../listaPeriodos.php');

$sqlFinanciadores="SELECT * FROM financiadores ORDER BY finan_nombre";
$financiadores=mysqli_query($enlace, $sqlFinanciadores);
?>

<div class="container">
<div class="titleForm">Facturación Financiador</div>
<hr>
    <table>
        <tr>    
            <td>
                <select class="custom-select selectTabla" id="idFinanciador">
                <option selected hidden value="">Seleccionar Financiador</option>                  
                    <?php
                        while($financiador=mysqli_fetch_assoc($financiadores))
                        {
                            echo "<option value='".$financiador['finan_id']."'>".$financiador['finan_nombre']."</option>";
                        }
                    ?>       
                </select>
            </td>   
            <td>
                <select class="custom-select selectTabla" id="anio"> 
                <option selected hidden value="">Año</option>                      
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>    
                </select>
            </td>   
            <td>
                <select class="custom-select selectTabla" id="periodo">  
                <option selected hidden value="">Período</option>                  
                    <?php
                        foreach ($listaPeriodos as $v)  
                        {   
                            echo "<option value='".$v->id."'>".$v->mes."</option>";
                        }
                    ?>       
                </select>
            </td>        
            <td><button type="button" onclick="buscarFacturacionFinanciador()" class="btn btn-primary"><i class="fa fa-search"></i></span>  Buscar</button></td>
            <td>
                <button type="button" onclick="exportarPdfFinanciador()" id="btnPDF" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i>  PDF</button>
            </td>
        </tr>
    </table>  
    <div id="tablaFacturacionFinanciadores"></div> 
    
    
    <script src="js/fc_facturacion_finan.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">   