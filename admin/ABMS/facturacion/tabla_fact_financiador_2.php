<?php
               
    include('../../server/db_connect.php'); 
    session_start();
    include('../../server/forbidden.php');

    // $idFinanciador = $_POST['idFinanciador'];
    // $anio = $_POST['anio'];
    // $periodo = $_POST['periodo'];    

    $idFinanciador = '113';
    $anio = '2018';
    $periodo = '7';
    $total = 0;
    $pacienteAnt = '';
    $echoPaciente;
    $echoTotal;
        
    $sqlFacturacionFinanciadores = "SELECT * FROM hiscli_det 
    JOIN hiscli_cab on hiscli_id=hiscli_det_cab_id
    JOIN pacientes on hiscli_id_paciente=pac_num_afil
    JOIN lista_prestaciones on hiscli_det_id_prest = prest_id 
    where pac_finan='$idFinanciador'
    AND year(hiscli_det_fecha_alta)>='$anio' AND month(hiscli_det_fecha_alta)<='$periodo' 
    AND year(hiscli_det_fecha_baja)<='$anio' AND month(hiscli_det_fecha_baja)>='$periodo'";

    $facturaciones = mysqli_query($enlace, $sqlFacturacionFinanciadores);    
    
?>

<table id="tableFacturacion" class="table table-hover">
        <thead>
        <tr>
            <th>Paciente</th>
            <th></th><th></th><th></th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>

<?php

    while($facturacion=mysqli_fetch_assoc($facturaciones))
    {                        
        $valCordon = $facturacion['pac_cordon'];
        $idPrest = $facturacion['hiscli_det_id_prest'];

        $sqlValores = "SELECT * FROM valores_cab 
        join valores_det on val_id=val_det_id_cab 
        where val_anio='$anio' and val_periodo='$periodo'        
        and val_id_finan='$idFinanciador'
        and val_det_id_prest = '$idPrest'";

        $valoresPrestacion = mysqli_query($enlace, $sqlValores);         
        while($valorPrestacion = mysqli_fetch_assoc($valoresPrestacion))
        {
            switch ($facturacion['pac_cordon']) 
            {
                case '1':
                    $valor = $valorPrestacion['val_det_valor_amba'];
                    break;
                case '2':
                    $valor = $valorPrestacion['val_det_valor_cordUno'];
                    break;
                case '3':
                    $valor = $valorPrestacion['val_det_valor_cordDos'];
                    break;
            }
        }
        

        $totalPrestacion = $facturacion['hiscli_det_total_mod'] + $facturacion['hiscli_det_total_ses'] + $facturacion['hiscli_det_total_hs'];          
        $paciente = $facturacion['pac_nombre'];          

        if($paciente != $pacienteAnt)
        {
            $pacienteAnt = $paciente;
            if($total != 0)
            {

            
?>  
<tr>                            
                        <td></td><td></td><td></td><td></td>
                        <td><label for="" id="fac_<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>"><?php echo('$'.number_format($total,0, ',', '.')); ?></label></td>        
                        <td></td>
                    </tr>
<?php
            }
?>                    
            <tr>    
                <td><label for="" id="fac_<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>"><?php echo(utf8_encode($facturacion['pac_nombre'])); ?></label></td>
                <td></td><td></td><td></td>
                <!-- <td><label for="" id="fac_<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>"><?php echo('$'.number_format($total,0, ',', '.')); ?></label></td>         -->
                <td></td>
            </tr>
           
<?php    


            
        }
        $sqlFacturacionFinanciadoresPaciente = "SELECT * FROM hiscli_det 
        JOIN hiscli_cab on hiscli_id=hiscli_det_cab_id
        JOIN pacientes on hiscli_id_paciente=pac_num_afil
        JOIN lista_prestaciones on hiscli_det_id_prest = prest_id 
        WHERE pac_finan='$idFinanciador'
        AND year(hiscli_det_fecha_alta)>='$anio' AND month(hiscli_det_fecha_alta)<='$periodo' 
        AND year(hiscli_det_fecha_baja)<='$anio' AND month(hiscli_det_fecha_baja)>='$periodo'
        AND pac_nombre='$paciente'";   
        $FinanciadorPacientes = mysqli_query($enlace, $sqlFacturacionFinanciadoresPaciente);         

        while($finanPaciente = mysqli_fetch_assoc($FinanciadorPacientes))
        {
            $prestacion = $finanPaciente['prest_id'];

            $sqlValores = "SELECT * FROM valores_cab 
            join valores_det on val_id=val_det_id_cab 
            where val_anio='$anio' and val_periodo='$periodo'        
            and val_id_finan='$idFinanciador'
            and val_det_id_prest = '$prestacion'";
            
            $valoresPrestacion = mysqli_query($enlace, $sqlValores);   

            while($valorPrestacion = mysqli_fetch_assoc($valoresPrestacion))
            {
                switch ($facturacion['pac_cordon']) 
                {
                    case '1':
                        $valor = $valorPrestacion['val_det_valor_amba'];
                        break;
                    case '2':
                        $valor = $valorPrestacion['val_det_valor_cordUno'];
                        break;
                    case '3':
                        $valor = $valorPrestacion['val_det_valor_cordDos'];
                        break;
                }
            }

            $hsPrestPaciente = $finanPaciente['hiscli_det_total_mod'] + $finanPaciente['hiscli_det_total_ses'] + $finanPaciente['hiscli_det_total_hs'];          
            $totalPrestPaciente = $hsPrestPaciente * $valor;
            $total = $total + $totalPrestPaciente;
        
?>                    
                <tr>                    
                    <td></td>
                    <td><label for="" id="fac_<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>"><?php echo(utf8_encode($finanPaciente['prest_nombre'])); ?></label></td>
                    <td><label for="" id="fac_<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>"><?php echo($hsPrestPaciente); ?></label></td>
                    <td><label for="" id="fac_<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>" value="<?php echo(utf8_encode($fila["hiscli_det_id_prest"])); ?>"><?php echo('$'.number_format($totalPrestPaciente,0, ',', '.')); ?></label></td>
                    <td></td>
                </tr>
               
<?php

        } 
        
        }  
    
?>


