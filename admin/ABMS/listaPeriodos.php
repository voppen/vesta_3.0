<?php

class periodo
{
    public $id;
    public $mes;

    public function  __construct($id, $mes) 
    {
        $this->id = $id;
        $this->mes = $mes;
    }
}

    $listaPeriodos[1]= new periodo(1, "Enero");
    $listaPeriodos[2]= new periodo(2, "Febrero");
    $listaPeriodos[3]= new periodo(3, "Marzo");
    $listaPeriodos[4]= new periodo(4, "Abril");
    $listaPeriodos[5]= new periodo(5, "Mayo");
    $listaPeriodos[6]= new periodo(6, "Junio");
    $listaPeriodos[7]= new periodo(7, "Julio");
    $listaPeriodos[8]= new periodo(8, "Agosto");
    $listaPeriodos[9]= new periodo(9, "Septiembre");
    $listaPeriodos[10]= new periodo(10, "Octubre");
    $listaPeriodos[11]= new periodo(11, "Noviembre");
    $listaPeriodos[12]= new periodo(12, "Diciembre");
    
?>