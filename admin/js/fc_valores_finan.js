$(".selectTabla").change(function(){
    let financiador = $("#idFinanciador").val();
    let año = $("#anio").val();
    let boton = (this.id);
    buscarOpciones("0",financiador,año, boton);
    if (boton == 'idFinanciador') {
        buscarOpciones("0",financiador,año, 'anio');
    }
})

function buscarOpciones(coordinador, financiador, año, boton) {
    
   $.ajax
    ({
        type:'POST',
        url: '../admin/server/valores/financiadores/buscar_opciones.php',
        data: {coordinador: coordinador, financiador: financiador, año: año, boton: boton},
        success: function(response)
        {          
            switch (boton) {
                case 'idFinanciador':
                    $('#anio').empty().html('<option selected hidden value="Todos">Año</option><option value="Todos">Todos</option>'+response);
                break;

                case 'anio':
                    $('#periodo').empty().html('<option selected hidden value="Todos">Período</option><option value="Todos">Todos</option>'+response);
                break;
                
        }
        },
        fail: function(response)
        {
            console.log(response);
        }
    });    
}


// CARGA TABLA LISTA

function cargarValoresPorFinanciador()
{    
    var coordinador = "";
    var financiador = $("#idFinanciador").val();
    var anio = $("#anio").val();
    var periodo = $("#periodo").val();    

    $.ajax
        ({
            type: 'POST',
            url: 'ABMS/valores/financiadores/table_valores_finan.php',
            data: { anio: anio, periodo: periodo, coordinador: coordinador, financiador: financiador },
            success: function (response) 
            {                  
                $('#tablaValores').html(response);                            
                
            },
            fail: function (response) {    
                alert("fail");
                // $("#errorMsg").show();   
                // $("#errorMsg").html(response);                                   
            }
        });
    
}

// CARGAR NUEVO
function addRow()
{               
    $("#btnGuardar").prop('disabled', false);
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/financiadores/div_modal_valores.php',
        success: function(response)
        {                                    
            var div = document.createElement('div');
            div.innerHTML = response;
            document.getElementById('rows').append(div);            
            // setId();                        
        },
        fail: function(response)
        {
            // $("#principal").html(retorno.responseText);
        }
    });    

    
}

function setId()
{                
    var nro = parseInt($("#numeroId").val(), 10) + 1;    
    $("#numeroId").val(nro);
    
    var prestacion = document.getElementById('selectPrestacion_');    
    prestacion.setAttribute("id", "selectPrestacion_"+nro);            

    var amba = document.getElementById('val_amba');    
    amba.setAttribute("id", "val_amba_"+nro);

    var cordonUno = document.getElementById('val_cord1');    
    cordonUno.setAttribute("id", "val_cord1_"+nro);

    var cordonDos = document.getElementById('val_cord2');    
    cordonDos.setAttribute("id", "val_cord2_"+nro);
}

function guardarValores()
{        
    var anio = $("#anioModal").val();
    var periodo = $("#periodoModal").val();
    var coordinador = "";
    var financiador = $("#idFinanciadorModal").val();

    $.ajax
    ({
        type: 'POST',
        url: 'server/valores/financiadores/sv_valores_finan.php',
        data: { anio: anio, periodo: periodo, coordinador: coordinador, financiador: financiador, accion:'guardarCab' },
        success: function (response) 
        {                                  
            guardarDetalles(response);                                
        },
        fail: function (response) {    
            alert("fail");                                             
        }
    });
}   

function guardarDetalles(idCab)
{    
    var count= 1;
    var prestacion = $("#selectPrestacion_" + count).val();        
                
    while($("#selectPrestacion_" + count).val()!=undefined)
    {
        prestacion = $("#selectPrestacion_" + count).val();              
        var idCab = idCab;
        var prestacion = $("#selectPrestacion_" + count).val();
        var valor_amba = $("#val_amba_" + count).val();
        var valor_cordUno = $("#val_cord1_" + count).val();
        var valor_cordDos = $("#val_cord2_" + count).val();
    
        $.ajax
        ({
            type: 'POST',
            url: 'server/valores/financiadores/sv_valores_finan.php',
            data: { idCab:idCab, prestacion: prestacion, valor_amba: valor_amba, valor_cordUno: valor_cordUno, valor_cordDos: valor_cordDos, accion:'guardarDet' },
            success: function (response) 
            {              
                $('#modalValor').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();                
                document.getElementById("form").reset();           
                $("#tableRows").remove();
                cargarValoresFinan();                                                
            },
            fail: function (response) {    
                alert("fail");
                // $("#errorMsg").show();   
                // $("#errorMsg").html(response);                                   
            }
        });

        count++;
    }
}



//CARGAR DATOS EDICION
function cargarValoresEditar(id)
{                
    var idCab = id;        
    $.ajax
        ({
            type: 'POST',
            url: 'server/valores/financiadores/sv_valores_finan.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarDatosEdit' },
            success: function (response) 
            {                                                                                 
                $("#anioModalEdit").val(response.result.val_anio);
                $("#anioHidden").val(response.result.val_anio);
                $("#periodoModalEdit").val(response.result.val_periodo);
                $("#periodoHidden").val(response.result.val_periodo);                
                $("#idFinanciadorModalEdit").val(response.result.val_id_finan);  
                $("#idCabModalEdit").val(idCab);   
                $("#val_EditPorcentajeFinan").val(0);               
            },        
            fail: function (response) {    
                alert("fail");                               
            }
        }).done(function()
        {          
            cargarDetallesValoresEdit(idCab);
        });    
}

function cargarDetallesValoresEdit(idCab)
{    
    $("#rowsEdit").empty() 
    var id=1;
    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/valores/financiadores/sv_valores_finan.php',
        dataType: 'json',
        data: { idCab:idCab, accion:'buscarDetallesEdit' },
        success: function (response) 
        {
            $.each(response, function(idx, obj) 
            {                                                        
                addRowEdit(id, obj);                                   
                id++;   
            });                                                  
        },  
        complete: function()
        {
            // $('#modalFinanciadorEdit').modal('show');
        },
        fail: function (response) {    
            alert("fail");                               
        }
    });    
}

function addRowEdit(id, obj)
{               
    var id = id;
    $("#btnGuardarEdit").prop('disabled', false);
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/financiadores/div_modal_valores_edit.php',
        success: function(response)
        {                                                
            var div = document.createElement('div');
            div.innerHTML = response;
            document.getElementById('rowsEdit').append(div);        
            setIdEdit(obj); 

        },
        fail: function(response)
        {
            // $("#principal").html(retorno.responseText);
        }
    });    

    
}



function setIdEdit(obj)
{                    
    var obj = obj;
    var nro = parseInt($("#numeroIdEditFinan").val(), 10) + 1;    
    $("#numeroIdEditFinan").val(nro);        


    var idDet = document.getElementById('idDetModalEdit');
    idDet.setAttribute("id", "idDetModalEdit_"+nro);    

    // var porcentaje =document.getElementById('val_EditPorcentajeFinan');
    // porcentaje.setAttribute("id", "val_EditPorcentaje_"+nro);  
    // alert("por" + porcentaje);

    var prestacion = document.getElementById('selectPrestacionEdit_');       
    prestacion.setAttribute("id", "selectPrestacionEdit_"+nro);         
       
    var amba =document.getElementById('val_ambaEditFinanciador');           
    amba.setAttribute("id", "val_ambaEdit_"+nro);       

    var ambaTotal =document.getElementById('val_ambaEditTotal');
    ambaTotal.setAttribute("id", "val_ambaEditTotal_"+nro);
    var ambaNuevo =document.getElementById('val_ambaNuevo');
    ambaNuevo.setAttribute("id", "val_ambaNuevo_"+nro);

    var cordonUno = document.getElementById('val_cord1Edit');    
    cordonUno.setAttribute("id", "val_cord1Edit_"+nro);    
    var cordUnoTotal =document.getElementById('val_cord1EditTotal');
    cordUnoTotal.setAttribute("id", "val_cord1EditTotal_"+nro);
    var cord1Nuevo =document.getElementById('val_cord1Nuevo');
    cord1Nuevo.setAttribute("id", "val_cord1Nuevo_"+nro);

    var cordonDos = document.getElementById('val_cord2Edit');    
    cordonDos.setAttribute("id", "val_cord2Edit_"+nro);
    var cordDosTotal =document.getElementById('val_cord2EditTotal');
    cordDosTotal.setAttribute("id", "val_cord2EditTotal_"+nro);
    var cord2Nuevo =document.getElementById('val_cord2Nuevo');
    cord2Nuevo.setAttribute("id", "val_cord2Nuevo_"+nro);


    if(obj != undefined)
    {        
        $("#selectPrestacionEdit_"+nro).val(obj.val_det_id_prest);         
        $("#val_ambaEdit_"+nro).val(obj.val_det_valor_amba);
        $("#val_cord1Edit_"+nro).val(obj.val_det_valor_cordUno);
        $("#val_cord2Edit_"+nro).val(obj.val_det_valor_cordDos);
        $("#idDetModalEdit_"+nro).val(obj.val_det_id);   
        $("#val_ambaNuevo_"+nro).val(0);  
        $("#val_cord1Nuevo_"+nro).val(0);  
        $("#val_cord2Nuevo_"+nro).val(0);  

    }
    else
    {
        $("#selectPrestacionEdit_"+nro).prop('disabled', false);   
        $("#val_ambaNuevo_"+nro).val(1);  
        $("#val_cord1Nuevo_"+nro).val(1);  
        $("#val_cord2Nuevo_"+nro).val(1);  
    }
}

// GUARDAR EDIT

function validarCondicionGuardadoEdit()
{
    var anio = $("#anioModalEdit").val();
    var anioHidden = $("#anioHidden").val();

    var periodo = $("#periodoModalEdit").val();
    var periodoHidden = $("#periodoHidden").val();
    
    var idCab = $("#idCabModalEdit").val();

    $("#numeroIdEditFinan").val(0);
    
    if(periodo == periodoHidden && anio == anioHidden)
    {                
        guardarDetallesEdit(idCab, "edit");
    }
    else
    {
        guardarValoresEditNuevo();
    }
}

function guardarValoresEditNuevo()
{
    var anio = $("#anioModalEdit").val();
    var periodo = $("#periodoModalEdit").val();
    var coordinador = "";
    var financiador = $("#idFinanciadorModalEdit").val();

    $.ajax
        ({
            type: 'POST',
            url: 'server/valores/financiadores/sv_valores_finan.php',
            data: { anio: anio, periodo: periodo, coordinador: coordinador, financiador: financiador, accion:'guardarCab' },
            success: function (response) 
            {                                  
                guardarDetallesEdit(response, "new");                                
            },
            fail: function (response) {    
                alert("fail");                                             
            }
        });
}

function guardarDetallesEdit(idCab, action)
{        
    var count= 1;
    var prestacion = $("#selectPrestacionEdit_" + count).val();        
    var action = action; 
                
    while($("#selectPrestacionEdit_" + count).val()!=undefined)
    {
        var ambaNuevo = $("#val_ambaNuevo_" + count).val();
        var cord1Nuevo = $("#val_ambaNuevo_" + count).val();
        var cord2Nuevo = $("#val_ambaNuevo_" + count).val();

        prestacion = $("#selectPrestacionEdit_" + count).val();          
        var idCab = idCab;        
        var idDet = $("#idDetModalEdit_" + count).val();
        var valor_amba = $("#val_ambaEdit_" + count).val();
        var valor_cordUno = $("#val_cord1Edit_" + count).val();
        var valor_cordDos = $("#val_cord2Edit_" + count).val();
        var accion;

        var ambaTotal = $("#val_ambaEditTotal_" + count).val();
        if(ambaTotal==""){ ambaTotal = valor_amba; }

        var cord1Total = $("#val_cord1EditTotal_" + count).val();
        if(cord1Total==""){ cord1Total = valor_cordUno; }

        var cord2Total = $("#val_cord2EditTotal_" + count).val();
        if(cord2Total==""){ cord2Total = valor_cordDos; }    

        if(action == "new")
        {
            $.ajax
            ({
                type: 'POST',
                url: 'server/valores/financiadores/sv_valores_finan.php',
                data: { idCab:idCab, prestacion: prestacion, valor_amba: ambaTotal, valor_cordUno: cord1Total, valor_cordDos: cord2Total, accion: "guardarDet" },
                success: function (response) 
                {              
                    $('#modalFinanciadorEdit').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();                
                    document.getElementById("form").reset();           
                    $("#tableRows").remove();
                    cargarValoresFinan();                                                
                },
                fail: function (response) 
                {    
                    alert("fail");                             
                }
            });          
        }
        else
        {
            if(ambaNuevo != 0 || cord1Nuevo != 0 || cord2Nuevo != 0)
            {                        
                $.ajax
                ({
                    type: 'POST',
                    url: 'server/valores/financiadores/sv_valores_finan.php',
                    data: { idCab:idCab, prestacion: prestacion, valor_amba: ambaTotal, valor_cordUno: cord1Total, valor_cordDos: cord2Total, accion: "guardarDet" },
                    success: function (response) 
                    {              
                        $('#modalFinanciadorEdit').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();                
                        document.getElementById("form").reset();           
                        $("#tableRows").remove();
                        cargarValoresFinan();                                                
                    },
                    fail: function (response) 
                    {    
                        alert("fail");                             
                    }
                });          
            }
            else
            {                        
                $.ajax
                ({
                    type: 'POST',
                    url: 'server/valores/financiadores/sv_valores_finan.php',
                    data: { idCab:idCab, idDet:idDet, prestacion: prestacion, valor_amba: valor_amba, valor_cordUno: valor_cordUno, valor_cordDos: valor_cordDos, accion: "updateDetEdit" },
                    success: function (response) 
                    {              
                        $('#modalValor').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();                
                        document.getElementById("form").reset();           
                        $("#tableRows").remove();
                        cargarValoresFinan();                                                
                    },
                    fail: function (response) 
                    {    
                        alert("fail");                             
                    }
                });          
            }
        }

        count++;
    }
}


function cargarTotal()
{         
    
    var count= 1;
    var prestacion = $("#selectPrestacionEdit_" + count).val();        
    var porcentaje = $("#val_EditPorcentajeFinan").val();

    while($("#selectPrestacionEdit_" + count).val()!=undefined)
    {                
        var valorAMBA = $("#val_ambaEdit_" + count).val();
        var valor2do = $("#val_cord1Edit_" + count).val();
        var valor3ro = $("#val_cord2Edit_" + count).val();

        var totalAMBA = parseFloat(valorAMBA) + ((parseFloat(valorAMBA) * parseFloat(porcentaje))/100);
        var total2do = parseFloat(valor2do) + ((parseFloat(valor2do) * parseFloat(porcentaje))/100);
        var total3ro = parseFloat(valor3ro) + ((parseFloat(valor3ro) * parseFloat(porcentaje))/100);
        
        $("#val_ambaEditTotal_"+count).val(totalAMBA);
        $("#val_cord1EditTotal_"+count).val(total2do);  
        $("#val_cord2EditTotal_"+count).val(total3ro);

        count++;
    }
    
    // let idCampos = $(obj).attr('id');
    // idCampos = idCampos[idCampos.length -1];      
    // var valorAMBA = $("#val_ambaEdit_"+idCampos).val(); 
    // var valor2do = $("#val_cord1Edit_"+idCampos).val();
    // var valor3ro = $("#val_cord2Edit_"+idCampos).val();     
    // var porcentaje = $(obj).val();     
    // var totalAMBA = parseFloat(valorAMBA) + ((parseFloat(valorAMBA) * parseFloat(porcentaje))/100);
    // var total2do = parseFloat(valor2do) + ((parseFloat(valor2do) * parseFloat(porcentaje))/100);
    // var total3ro = parseFloat(valor3ro) + ((parseFloat(valor3ro) * parseFloat(porcentaje))/100);
    //($(obj).parent()).closest('td').next('td').find('input').val(total);
     

    
}

// ELIMINAR ROWS Y CAMPOS

function removeRow(input) {
    document.getElementById('rows').removeChild(input.parentNode);
}

function borrarCamposValoresFinan()
{        
    document.getElementById("form").reset(); 
    $("#rows").empty()   
    $('#modalDetalleValorFinanciadorDelete').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove(); 
    $("#numeroIdEditFinan").val(0);
    cargarValoresPorFinanciador();
}

function cargarValoresDeleteModal(id)
{
    var idCab = id;        
    $.ajax
        ({
            type: 'POST',
            url: 'server/valores/financiadores/sv_valores_finan.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarDatosEdit' },
            success: function (response) 
            {                                                                                   
                $("#anioModalDelete").val(response.result.val_anio);                
                $("#periodoModalDelete").val(response.result.val_periodo);
                $("#periodoHidden").val(response.result.val_periodo);                
                $("#idCoordinadorModalDelete").val(response.result.val_id_coord);
                $("#idFinanciadorModalDelete").val(response.result.val_id_finan);  
                $("#idCabModalDelete").val(idCab);                  
            },        
            fail: function (response) {    
                alert("fail");                               
            }
        }).done(function()
        {          
            cargarDetallesValoresDelete(idCab);
        });       
}


function cargarDetallesValoresDelete(idCab)
{    
    $("#rowsDelete").empty() 
    var id=1;
    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/valores/financiadores/sv_valores_finan.php',
        dataType: 'json',
        data: { idCab:idCab, accion:'buscarDetallesEdit' },
        success: function (response) 
        {
            $.each(response, function(idx, obj) 
            {                                                        
                addRowDelete(id, obj);                                   
                id++;   
            });                                                  
        },  
        complete: function()
        {
            // $('#modalFinanciadorEdit').modal('show');
        },
        fail: function (response) {    
            alert("fail");                               
        }
    });    
}

function addRowDelete(id, obj)
{               
    var id = id;

    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/financiadores/div_modal_valores_delete.php',
        success: function(response)
        {                                                
            var div = document.createElement('div');
            div.innerHTML = response;
            document.getElementById('rowsDelete').append(div);            
            setIdDelete(obj); 

        },
        fail: function(response)
        {           
        }
    });        
}

function setIdDelete(obj)
{                
    var obj = obj;
    var nro = parseInt($("#numeroIdDelete").val(), 10) + 1;    
    $("#numeroIdDelete").val(nro);
    
    var prestacion = document.getElementById('selectPrestacionDelete');    
    prestacion.setAttribute("id", "selectPrestacionDelete_"+nro); 

    var idDet = document.getElementById('idDetModalDelete');
    idDet.setAttribute("id", "idDetModalDelete_"+nro); 
   
    var amba = document.getElementById('val_ambaDelete');    
    amba.setAttribute("id", "val_ambaDelete_"+nro);      
    
    var btnBorrar = document.getElementById('btnBorrarPrestacion');
    btnBorrar.setAttribute("id", "btnBorrarPrestacion"+nro);  
    

    var cordonUno = document.getElementById('val_cord1Delete');    
    cordonUno.setAttribute("id", "val_cord1Delete_"+nro);    

    var cordonDos = document.getElementById('val_cord2Delete');    
    cordonDos.setAttribute("id", "val_cord2Delete_"+nro);

    $("#selectPrestacionDelete_"+nro).val(obj.val_det_id_prest);         
    $("#val_ambaDelete_"+nro).val(obj.val_det_valor_amba);
    $("#val_cord1Delete_"+nro).val(obj.val_det_valor_cordUno);
    $("#val_cord2Delete_"+nro).val(obj.val_det_valor_cordDos);
    $("#idDetModalDelete_"+nro).val(obj.val_det_id);
    $("#btnBorrarPrestacion"+nro).val(obj.val_det_id);

}

function borrarPrestacion(obj)
{
    var idCab = $("#idCabModalDelete").val();
    var idDet = $(obj).val();
    $.ajax
    ({
        type: 'POST',
        url: 'server/valores/financiadores/sv_valores_finan.php',
        data: { idDet:idDet,idCab:idCab, accion:'borrarDetalle' },
        success: function (response) 
        {                                           
            if(response=="delete")
            {
                $('#modalDetalleValorFinanciadorDelete').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove(); 
                cargarValoresFinan();
            }   
            else
            {
                cargarDetallesValoresDelete(idCab);
            }
            
        },
        fail: function (response) {    
            alert("fail");                                             
        }
    });    
}

function borrarValoresFinanciador()
{
    var idCab = $("#idCabModalDelete").val();
    $.ajax
    ({
        type: 'POST',
        url: 'server/valores/financiadores/sv_valores_finan.php',
        data: { idCab:idCab, accion:'borrarValores' },
        success: function (response) 
        {                         
            $('#modalDetalleValorFinanciadorDelete').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();                 
            cargarValoresPorFinanciador();
        },
        fail: function (response) {    
            alert("fail");                                             
        }
    });    
}