
$(document).ready(function() 
{
    $('.form_edit').click(function(){      
        var financiador_id = $(this).attr("id");             
        $("#finan_id_edit").val(financiador_id);
        let nombre_edit = $('#finan_'+ financiador_id).attr("value");
        $("input[name='finan_nombre_edit']").val(nombre_edit);
    });  
});


function borrarCampos() 
{
    $("#finan_nombre_edit").val("");
    $("#errorMsg").hide();
    $("#errorMsg").html("");
}

function modificarFinanciador(accion) 
{            
    $id = $("#finan_id_edit").val();
    $nombre = $("#finan_nombre_edit").val();
    $accion = accion;
    
    if($nombre.replace(/\s/g,'') == "" && $accion!='borrar')
    {        
        $("#errorMsg").show();
        $("#errorMsg").html("Ingresar Nombre.");  
        return;
    }

    $.ajax
        ({
            type: 'POST',
            url: 'server/financiadores/sv_financiadores.php',
            data: { id: $id, nombre: $nombre, accion: $accion },
            success: function (response) {                  
                if(response !== " ")
                {    
                    $("#errorMsg").show();
                    $("#errorMsg").html(response);                    
                }                                
                else
                {                    
                    $('#modalFinanciador').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#modalFinanciadorDelete').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    cargarFinanciadores();      
                }
                
            },
            fail: function (response) {    
                alert("fail");
                $("#errorMsg").show();   
                $("#errorMsg").html(response);                                   
            }
        });
}


