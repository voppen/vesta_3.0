$(document).ready(function() 
{
    $('.form_edit').click(function(){  
        var coordinador_id = $(this).attr("id");             
        $("#coord_id_edit").val(coordinador_id);
        let nombre_edit = $('#coord_'+ coordinador_id).attr("value");
        $("input[name='coord_nombre_edit']").val(nombre_edit);

    });  

    $('.pagina').on('click', function(){
        $('.pagina').removeClass('active');
        $(this).addClass('active');
        $('.pagina').css('background-color','transparent');
        $(this).css('background-color','#6699ff');
        let mostrarpagina = $(this).attr('pag');
        $('.datos').hide();
        $('tr[pagina="'+mostrarpagina+'"]').show();
    })

    $('.anterior').on('click',function(){
    })

});

    function borrarCampos() {
        $("#coord_nombre_edit").val("");
        $("#errorMsg").hide();
        $("#errorMsg").html("");
    }

function modificarCoordinador(accion) 
{            
    $id = $("#coord_id_edit").val();
    $nombre = $("#coord_nombre_edit").val();
    $accion = accion;
    
    if($nombre.replace(/\s/g,'') == "" && $accion!='borrar')
    {        
        $("#errorMsg").show();
        $("#errorMsg").html("Ingresar Nombre.");  
        return;
    }

    $.ajax
        ({
            type: 'POST',
            url: 'server/coordinadores/sv_coordinadores.php',
            data: { id: $id, nombre: $nombre, accion: $accion },
            success: function (response) {                  
                if(response !== " ")
                {    
                    $("#errorMsg").show();
                    $("#errorMsg").html(response);                    
                }                                
                else
                {                    
                    $('#modalCoord').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#modalCoordDelete').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    cargarCoordinadores();      
                }
                
            },
            fail: function (response) {    
                alert("fail");
                $("#errorMsg").show();   
                $("#errorMsg").html(response);                                   
            }
        });
}

