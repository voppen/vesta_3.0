
$(document).ready(function() 
{
    $('.asda').click(function(){
        alert(profesional_id+", "+nombre_edit+", "+profesion_edit);
    })
    $('.form_edit').click(function(){      
        var profesional_id = $(this).attr("id");             
        $("#prof_id_edit").val(profesional_id);
        let nombre_edit = $('#prof_'+ profesional_id).attr("value");
        $("input[name='prof_nombre_edit']").val(nombre_edit);
        let profesion_edit = $('#prof_profesion_'+ profesional_id).attr("value");
        $("input[name='prof_profesion_edit']").val(profesion_edit);
        var coord_edit = $(this).attr("coordinador");
        $("#dropdownCoordinadores").val($("#coord_edit_"+coord_edit).val());
    });  
});


function borrarCampos() 
{
    $("#prof_nombre_edit").val("");
    $("#prof_profesion_edit").val("");
    $("#dropdownCoordinadores").val($(".listaCoordinadores").first().val());
    $("#errorMsg").hide();
    $("#errorMsg").html("");
}

function modificarProfesional(accion) 
{            
    $id = $("#prof_id_edit").val();
    $nombre = $("#prof_nombre_edit").val();
    $profesion = $("#prof_profesion_edit").val();
    $coordinador = $("#idCoordinadorModal").val();
    $accion = accion;
    
    if($nombre.replace(/\s/g,'') == "" && $accion!='borrar')
    {        
        $("#errorMsg").show();
        $("#errorMsg").html("Ingresar Nombre.");  
        return;
    }


    $.ajax
        ({
            type: 'POST',
            url: 'server/profesionales/sv_profesionales.php',
            data: { id: $id, nombre: $nombre, profesion: $profesion, coordinador: $coordinador, accion: $accion },
            success: function (response) {              
                if(response !== " ")
                {    
                    $("#errorMsg").show();
                    $("#errorMsg").html(response);                    
                }                                
                else
                {                    
                    $('#modalProfesional').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#modalProfesionalDelete').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    cargarProfesionales();      
                }
                
            },
            fail: function (response) {    
                alert("fail");
                $("#errorMsg").show();   
                $("#errorMsg").html(response);                                   
            }
        });
}


