
function cargarHistoriasClinicas()
{
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/historias_clinicas/lista_historias_clinicas.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function abrirFormNuevaHistoriaClinica()
{
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/historias_clinicas/form_nuevaHistClinica.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarCoordinadores()
{    
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/coordinadores/lista_coordinadores.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarFinanciadores()
{ 
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/financiadores/lista_financiadores.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarProfesionales()
{ 
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/profesionales/lista_profesionales.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarPacientes()
{ 
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/pacientes/lista_pacientes.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarValoresCoord()
{        
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/coordinadores/lista_valores_coord.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarValoresFinan()
{        
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/Financiadores/lista_valores_finan.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarAlertas()
{        
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/alertas.php',
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarFacturacionPacientes()
{        
    var paciente = '1';
    var anio = '2018';
    var periodo = '7';

    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/facturacion/pdf_facturacion_paciente.php',
        data: { paciente:paciente, anio:anio, periodo:periodo },
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarFacturacionPacientesAjax()
{        
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/facturacion/facturacion_pacientes.php',        
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

function cargarFacturacionFinanciadores()
{        
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/facturacion/facturacion_financiador.php',        
        success: function(response)
        {
            $('#principal').html(response);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });
}

