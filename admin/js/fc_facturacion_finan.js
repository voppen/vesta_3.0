
function buscarFacturacionFinanciador()
{    
    $idFinanciador = $("#idFinanciador").val();
    $anio = $("#anio").val();
    $periodo = $("#periodo").val();

    // if($idFinanciador == "")
    // {
    //     alert("Seleccionar Financiador");
    // }

    // if($anio == "")
    // {
    //     alert("Seleccionar Año");
    // }

    // if($periodo == "")
    // {
    //     alert("Seleccionar Periodo");
    // }


    $.ajax
        ({
            type: 'POST',
            url: 'ABMS/facturacion/tabla_fact_financiador.php',
            data: { anio: $anio, periodo: $periodo, idFinanciador: $idFinanciador },
            success: function (response) 
            {                  
                $('#tablaFacturacionFinanciadores').html(response);                            
                
            },
            fail: function (response) 
            {    
                alert("fail");                                
            }
        });        

}


function buscarDetallesFacturacionFinanciador(idCab)
{
    $idFinanciador = $("#idFinanciador").val();
    $anio = $("#anio").val();
    $periodo = $("#periodo").val();

    $.ajax
        ({
            type: 'POST',
            url: 'ABMS/facturacion/tabla_detalles_financiador.php',
            data: { idCab: idCab, anio: $anio, periodo: $periodo, idFinanciador: $idFinanciador },
            success: function (response) 
            {                  
                $('#tablaDetallesFinanciador_'+idCab).html(response);                            
                
            },
            fail: function (response) 
            {    
                alert("fail");                                
            }
        });  
}