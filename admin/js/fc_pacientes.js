
$(document).ready(function() 
{
    $('.form_edit').click(function(){
        $('#btnGuardarPaciente').attr("onclick", "modificarPaciente('editar')");            
        var pac_num_afil = $(this).attr("id");            
        $("#pac_num_afil_edit").val(pac_num_afil);
        let nombre_edit = $('#pac_'+ pac_num_afil).attr("value");
        $("input[name='pac_nombre_edit']").val(nombre_edit);
        $("#pac_finan_edit").val($('#finan_'+ pac_num_afil).attr("value"));
        $("#pac_cordon_edit").val($('#cordon_'+ pac_num_afil).attr("value"));
        $("#pac_iva_edit").val($('#iva_'+ pac_num_afil).attr("value"));
        $("#pac_diag_edit").val($('#diag_'+ pac_num_afil).attr("value"));
        $("#pac_fecha_alta_edit").val($('#fecha_alta_'+ pac_num_afil).attr("value"));
        $("#pac_dom_edit").val($('#dom_'+ pac_num_afil).attr("value"));
        $("#pac_tel_1_edit").val($('#tel_1_'+ pac_num_afil).attr("value"));
        $("#pac_tel_2_edit").val($('#tel_2_'+ pac_num_afil).attr("value"));
        $("#pac_resp_edit").val($('#resp_'+ pac_num_afil).attr("value"));
        $("#pac_tel_resp_edit").val($('#tel_resp_'+ pac_num_afil).attr("value"));
    });  

    $('#btnNewPaciente').click(function(){
        $('#btnGuardarPaciente').attr("onclick", "modificarPaciente('nuevo')");
    })

});


function borrarCampos() 
{
    document.getElementById("form").reset();
    $("#errorMsg").hide();
    $("#errorMsg").html("");
}

function modificarPaciente(accion) 
{            
    $id = $("#pac_num_afil_edit").val();
    $nombre = $("#pac_nombre_edit").val();
    $finan = $("#pac_finan_edit").val();
    $cordon = $("#pac_cordon_edit").val();
    $iva = $("#pac_iva_edit").val();
    $diag = $("#pac_diag_edit").val();
    $fecha_alta = $("#pac_fecha_alta_edit").val();
    $dom = $("#pac_dom_edit").val();
    $tel_1 = $("#pac_tel_1_edit").val();
    $tel_2 = $("#pac_tel_2_edit").val();
    $resp = $("#pac_resp_edit").val();
    $tel_resp = $("#pac_tel_resp_edit").val();
    $accion = accion;
    
  /*  if($nombre.replace(/\s/g,'') == "" && $accion!='borrar')
    {        
        $("#errorMsg").show();
        $("#errorMsg").html("Ingresar Nombre.");  
        return;
    }

    if($("select[name='pac_cordon_edit'] option:selected") === "Seleccionar") {
        $("#errorCordon").show();
        $("#errorCordon").html("Debe seleccionar un cordón.");
        return;
    }
    */

    //VALIDACIONES
    function validarForm() {
        let validForm = true;
        let errorHtml = "";
        if (!$(".required").val() || $(".required").val().replace(/\s/g,'') == "") {
            validForm = false;
            errorHtml+="Debes completar los campos requeridos.";
        }
        if (validForm === true) {
            return true;
        } else {
            return errorHtml;
        }
    }




    if (validarForm() === true) {
        $.ajax
            ({
                type: 'POST',
                url: 'server/pacientes/sv_pacientes.php',
                data: { id: $id, nombre: $nombre, finan: $finan, cordon: $cordon, iva: $iva, diag: $diag, fecha_alta: $fecha_alta, dom: $dom, tel_1: $tel_1, tel_2: $tel_2, resp: $resp, tel_resp: $tel_resp, accion: $accion },
                success: function (response) {                  
                    if(response !== " ")
                    {    
                        $("#errorMsg").show();
                        $("#errorMsg").html(response);      
                    }                                
                    else
                    {                    
                        $('#modalPaciente').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $('#modalPacienteDelete').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        cargarPacientes();      
                    }
                    
                },
                fail: function (response) {    
                    alert("fail");
                    $("#errorMsg").show();   
                    $("#errorMsg").html(response);                              
                }
            });
    } else {
        alert(validarForm());
        /*
        $("#errorMsg").show();
        $("#errorMsg").html(validarForm());*/
    }
}


