$(".selectTabla").change(function(){
    let coordinador = $("#idCoordinador").val();
    let financiador = $("#idFinanciador").val();
    let año = $("#anio").val();
    let boton = (this.id);
    buscarOpciones(coordinador,financiador,año, boton);
    if (boton == 'idCoordinador') {
        buscarOpciones(coordinador,financiador,año, 'idFinanciador');
        buscarOpciones(coordinador,financiador,año, 'anio');
    }
    if (boton == 'idFinanciador') {
        buscarOpciones(coordinador,financiador,año, 'anio');
    }
})

function buscarOpciones(coordinador, financiador, año, boton) {
    
   $.ajax
    ({
        type:'POST',
        url: '../admin/server/valores/coordinadores/buscar_opciones.php',
        data: {coordinador: coordinador, financiador: financiador, año: año, boton: boton},
        success: function(response)
        {          
            
            switch (boton) {
                case 'idCoordinador':
                    $('#idFinanciador').empty().html('<option selected hidden value="Todos">Seleccionar Financiador</option><option value="Todos">Todos</option>'+response);
                break;

                case 'idFinanciador':
                    $('#anio').empty().html('<option selected hidden value="Todos">Año</option><option value="Todos">Todos</option>'+response);
                break;

                case 'anio':
                    $('#periodo').empty().html('<option selected hidden value="Todos">Período</option><option value="Todos">Todos</option>'+response);
                break;
                
        }
        },
        fail: function(response)
        {
            console.log(response);
        }
    });    
}


// CARGA TABLA LISTA

function cargarValoresPorCoordinador()
{    
    var coordinador = $("#idCoordinador").val();
    var financiador = $("#idFinanciador").val();
    var anio = $("#anio").val();
    var periodo = $("#periodo").val();

    $.ajax
        ({
            type: 'POST',
            url: 'ABMS/valores/coordinadores/table_valores_coord.php',
            data: { anio: anio, periodo: periodo, coordinador: coordinador, financiador: financiador },
            success: function (response) 
            {                  
                $('#tablaValores').html(response);                            
                
            },
            fail: function (response) {    
                alert("fail");
                // $("#errorMsg").show();   
                // $("#errorMsg").html(response);                                   
            }
        });
    
}

// CARGAR NUEVO
function addRow()
{               
    $("#btnGuardar").prop('disabled', false);
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/coordinadores/div_modal_valores.php',
        success: function(response)
        {                                    
            var div = document.createElement('div');
            div.innerHTML = response;
            document.getElementById('rows').append(div);            
            setId();                        
        },
        fail: function(response)
        {
            // $("#principal").html(retorno.responseText);
        }
    });    

    
}

function setId()
{                
    var nro = parseInt($("#numeroId").val(), 10) + 1;    
    $("#numeroId").val(nro);
    
    var prestacion = document.getElementById('selectPrestacion_');    
    prestacion.setAttribute("id", "selectPrestacion_"+nro);            

    var amba = document.getElementById('val_amba');    
    amba.setAttribute("id", "val_amba_"+nro);

    var cordonUno = document.getElementById('val_cord1');    
    cordonUno.setAttribute("id", "val_cord1_"+nro);

    var cordonDos = document.getElementById('val_cord2');    
    cordonDos.setAttribute("id", "val_cord2_"+nro);
}

function guardarValores()
{        
    var anio = $("#anioModal").val();
    var periodo = $("#periodoModal").val();
    var coordinador = $("#idCoordinadorModal").val();
    var financiador = $("#idFinanciadorModal").val();

    $.ajax
    ({
        type: 'POST',
        url: 'server/valores/coordinadores/sv_valores_coord.php',
        data: { anio: anio, periodo: periodo, coordinador: coordinador, financiador: financiador, accion:'guardarCab' },
        success: function (response) 
        {                                  
            guardarDetalles(response);                                
        },
        fail: function (response) {    
            alert("fail");                                             
        }
    });
}   

function guardarDetalles(idCab)
{    
    var count= 1;
    var prestacion = $("#selectPrestacion_" + count).val();        
                
    while($("#selectPrestacion_" + count).val()!=undefined)
    {
        prestacion = $("#selectPrestacion_" + count).val();              
        var idCab = idCab;
        var prestacion = $("#selectPrestacion_" + count).val();
        var valor_amba = $("#val_amba_" + count).val();
        var valor_cordUno = $("#val_cord1_" + count).val();
        var valor_cordDos = $("#val_cord2_" + count).val();
    
        $.ajax
        ({
            type: 'POST',
            url: 'server/valores/coordinadores/sv_valores_coord.php',
            data: { idCab:idCab, prestacion: prestacion, valor_amba: valor_amba, valor_cordUno: valor_cordUno, valor_cordDos: valor_cordDos, accion:'guardarDet' },
            success: function (response) 
            {              
                $('#modalValor').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();                
                document.getElementById("form").reset();           
                $("#tableRows").remove();
                cargarValoresCoord();                                                
            },
            fail: function (response) {    
                alert("fail");
                // $("#errorMsg").show();   
                // $("#errorMsg").html(response);                                   
            }
        });

        count++;
    }
}



//CARGAR DATOS EDICION
function cargarValoresEditar(id)
{            
    var idCab = id;        
    $.ajax
        ({
            type: 'POST',
            url: 'server/valores/coordinadores/sv_valores_coord.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarDatosEdit' },
            success: function (response) 
            {                                                                                   
                $("#anioModalEdit").val(response.result.val_anio);
                $("#anioHidden").val(response.result.val_anio);
                $("#periodoModalEdit").val(response.result.val_periodo);
                $("#periodoHidden").val(response.result.val_periodo);                
                $("#idCoordinadorModalEdit").val(response.result.val_id_coord);
                $("#idFinanciadorModalEdit").val(response.result.val_id_finan);  
                $("#idCabModalEdit").val(idCab);                  
            },        
            fail: function (response) {    
                alert("fail");                               
            }
        }).done(function()
        {          
            cargarDetallesValoresEdit(idCab);
        });    
}

function cargarDetallesValoresEdit(idCab)
{    
    $("#rowsEdit").empty() 
    var id=1;
    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/valores/coordinadores/sv_valores_coord.php',
        dataType: 'json',
        data: { idCab:idCab, accion:'buscarDetallesEdit' },
        success: function (response) 
        {
            $.each(response, function(idx, obj) 
            {                                                        
                addRowEdit(id, obj);                                   
                id++;   
            });                                                  
        },  
        complete: function()
        {
            // $('#modalFinanciadorEdit').modal('show');
        },
        fail: function (response) {    
            alert("fail");                               
        }
    });    
}

function addRowEdit(id, obj)
{               
    var id = id;
    $("#btnGuardarEdit").prop('disabled', false);
    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/coordinadores/div_modal_valores_edit.php',
        success: function(response)
        {                                                
            var div = document.createElement('div');
            div.innerHTML = response;
            document.getElementById('rowsEdit').append(div);            
            setIdEdit(obj); 

        },
        fail: function(response)
        {
            // $("#principal").html(retorno.responseText);
        }
    });    

    
}



function setIdEdit(obj)
{                
    var obj = obj;
    var nro = parseInt($("#numeroIdEdit").val(), 10) + 1;    
    $("#numeroIdEdit").val(nro);
    
    var prestacion = document.getElementById('selectPrestacionEdit_');    
    prestacion.setAttribute("id", "selectPrestacionEdit_"+nro); 

    var idDet = document.getElementById('idDetModalEdit');
    idDet.setAttribute("id", "idDetModalEdit_"+nro); 
   
    var amba = document.getElementById('val_ambaEdit');    
    amba.setAttribute("id", "val_ambaEdit_"+nro);       
    var ambaPorcentaje =document.getElementById('val_ambaEditPorcentaje');
    ambaPorcentaje.setAttribute("id", "val_ambaEditPorcentaje_"+nro);
    var ambaTotal =document.getElementById('val_ambaEditTotal');
    ambaTotal.setAttribute("id", "val_ambaEditTotal_"+nro);
    var ambaNuevo =document.getElementById('val_ambaNuevo');
    ambaNuevo.setAttribute("id", "val_ambaNuevo_"+nro);

    var cordonUno = document.getElementById('val_cord1Edit');    
    cordonUno.setAttribute("id", "val_cord1Edit_"+nro);    
    var cordUnoPorcentaje =document.getElementById('val_cord1EditPorcentaje');
    cordUnoPorcentaje.setAttribute("id", "val_cord1EditPorcentaje_"+nro);
    var cordUnoTotal =document.getElementById('val_cord1EditTotal');
    cordUnoTotal.setAttribute("id", "val_cord1EditTotal_"+nro);
    var cord1Nuevo =document.getElementById('val_cord1Nuevo');
    cord1Nuevo.setAttribute("id", "val_cord1Nuevo_"+nro);

    var cordonDos = document.getElementById('val_cord2Edit');    
    cordonDos.setAttribute("id", "val_cord2Edit_"+nro);
    var cordDosPorcentaje =document.getElementById('val_cord2EditPorcentaje');
    cordDosPorcentaje.setAttribute("id", "val_cord2EditPorcentaje_"+nro);
    var cordDosTotal =document.getElementById('val_cord2EditTotal');
    cordDosTotal.setAttribute("id", "val_cord2EditTotal_"+nro);
    var cord2Nuevo =document.getElementById('val_cord2Nuevo');
    cord2Nuevo.setAttribute("id", "val_cord2Nuevo_"+nro);

    if(obj != undefined)
    {        
        $("#selectPrestacionEdit_"+nro).val(obj.val_det_id_prest);         
        $("#val_ambaEdit_"+nro).val(obj.val_det_valor_amba);
        $("#val_cord1Edit_"+nro).val(obj.val_det_valor_cordUno);
        $("#val_cord2Edit_"+nro).val(obj.val_det_valor_cordDos);
        $("#idDetModalEdit_"+nro).val(obj.val_det_id);   
        $("#val_ambaNuevo_"+nro).val(0);  
        $("#val_cord1Nuevo_"+nro).val(0);  
        $("#val_cord2Nuevo_"+nro).val(0);  

    }
    else
    {
        $("#selectPrestacionEdit_"+nro).prop('disabled', false);   
        $("#val_ambaNuevo_"+nro).val(1);  
        $("#val_cord1Nuevo_"+nro).val(1);  
        $("#val_cord2Nuevo_"+nro).val(1);  
    }

    periodo = $("#periodoModalEdit").val();
    periodoHid = $("#periodoHidden").val(); 

    if(periodo != periodoHid)
    {        
        $("#val_ambaEditPorcentaje_" + nro).prop('disabled', false);            
        $("#val_cord1EditPorcentaje_" + nro).prop('disabled', false);            
        $("#val_cord2EditPorcentaje_" + nro).prop('disabled', false);            
        $("#val_ambaEditTotal_" + nro).prop('disabled', false);            
        $("#val_cord1EditTotal_" + nro).prop('disabled', false);            
        $("#val_cord2EditTotal_" + nro).prop('disabled', false);  
    }
}

function habilitarCamposEdit()
{    
    periodo = $("#periodoModalEdit").val();
    periodoHid = $("#periodoHidden").val();    

    if(periodo != periodoHid)
    {        
        var count= 1;                   
        while($("#selectPrestacionEdit_" + count).val()!=undefined)
        {
            // alert("while");
            $("#val_ambaEditPorcentaje_" + count).prop('disabled', false);            
            $("#val_cord1EditPorcentaje_" + count).prop('disabled', false);            
            $("#val_cord2EditPorcentaje_" + count).prop('disabled', false);            
            $("#val_ambaEditTotal_" + count).prop('disabled', false);            
            $("#val_cord1EditTotal_" + count).prop('disabled', false);            
            $("#val_cord2EditTotal_" + count).prop('disabled', false);            
            count++;
        }
    }
}

// GUARDAR EDIT

function validarCondicionGuardadoEdit()
{
    var anio = $("#anioModalEdit").val();
    var anioHidden = $("#anioHidden").val();

    var periodo = $("#periodoModalEdit").val();
    var periodoHidden = $("#periodoHidden").val();
    
    var idCab = $("#idCabModalEdit").val();
    
    if(periodo == periodoHidden && anio == anioHidden)
    {                
        guardarDetallesEdit(idCab, "edit");
    }
    else
    {
        guardarValoresEditNuevo();
    }
}

function guardarValoresEditNuevo()
{
    var anio = $("#anioModalEdit").val();
    var periodo = $("#periodoModalEdit").val();
    var coordinador = $("#idCoordinadorModalEdit").val();
    var financiador = $("#idFinanciadorModalEdit").val();

    $.ajax
        ({
            type: 'POST',
            url: 'server/valores/coordinadores/sv_valores_coord.php',
            data: { anio: anio, periodo: periodo, coordinador: coordinador, financiador: financiador, accion:'guardarCab' },
            success: function (response) 
            {                                  
                guardarDetallesEdit(response, "new");                                
            },
            fail: function (response) {    
                alert("fail");                                             
            }
        });
}

function guardarDetallesEdit(idCab, action)
{        
    var count= 1;
    var prestacion = $("#selectPrestacionEdit_" + count).val();        
    var action = action; 
                
    while($("#selectPrestacionEdit_" + count).val()!=undefined)
    {
        var ambaNuevo = $("#val_ambaNuevo_" + count).val();
        var cord1Nuevo = $("#val_ambaNuevo_" + count).val();
        var cord2Nuevo = $("#val_ambaNuevo_" + count).val();        

        prestacion = $("#selectPrestacionEdit_" + count).val();          
        var idCab = idCab;        
        var idDet = $("#idDetModalEdit_" + count).val();
        var valor_amba = $("#val_ambaEdit_" + count).val();
        var valor_cordUno = $("#val_cord1Edit_" + count).val();
        var valor_cordDos = $("#val_cord2Edit_" + count).val();
        var accion;

        var ambaTotal = $("#val_ambaEditTotal_" + count).val();
        if(ambaTotal==""){ ambaTotal = valor_amba; }

        var cord1Total = $("#val_cord1EditTotal_" + count).val();
        if(cord1Total==""){ cord1Total = valor_cordUno; }

        var cord2Total = $("#val_cord2EditTotal_" + count).val();
        if(cord2Total==""){ cord2Total = valor_cordDos; }    

        if(action == "new")
        {
            $.ajax
            ({
                type: 'POST',
                url: 'server/valores/coordinadores/sv_valores_coord.php',
                data: { idCab:idCab, prestacion: prestacion, valor_amba: ambaTotal, valor_cordUno: cord1Total, valor_cordDos: cord2Total, accion: "guardarDet" },
                success: function (response) 
                {              
                    $('#modalFinanciadorEdit').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();                
                    document.getElementById("form").reset();           
                    $("#tableRows").remove();
                    cargarValoresCoord();                                                
                },
                fail: function (response) 
                {    
                    alert("fail");                             
                }
            });          
        }
        else
        {
            if(ambaNuevo != 0 || cord1Nuevo != 0 || cord2Nuevo != 0)
            {                        
                $.ajax
                ({
                    type: 'POST',
                    url: 'server/valores/coordinadores/sv_valores_coord.php',
                    data: { idCab:idCab, prestacion: prestacion, valor_amba: ambaTotal, valor_cordUno: cord1Total, valor_cordDos: cord2Total, accion: "guardarDet" },
                    success: function (response) 
                    {              
                        $('#modalFinanciadorEdit').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();                
                        document.getElementById("form").reset();           
                        $("#tableRows").remove();
                        cargarValoresCoord();                                                
                    },
                    fail: function (response) 
                    {    
                        alert("fail");                             
                    }
                });          
            }
            else
            {                        
                $.ajax
                ({
                    type: 'POST',
                    url: 'server/valores/coordinadores/sv_valores_coord.php',
                    data: { idCab:idCab, idDet:idDet, prestacion: prestacion, valor_amba: valor_amba, valor_cordUno: valor_cordUno, valor_cordDos: valor_cordDos, accion: "updateDetEdit" },
                    success: function (response) 
                    {              
                        $('#modalValor').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();                
                        document.getElementById("form").reset();           
                        $("#tableRows").remove();
                        cargarValoresCoord();                                                
                    },
                    fail: function (response) 
                    {    
                        alert("fail");                             
                    }
                });          
            }
        }

        count++;
    }
}


function cargarTotal(obj)
{               
    var valor = ($(obj).parent()).closest('td').prev('td').find('input').val();             
    var porcentaje = $(obj).val();     
    var total = parseFloat(valor) + ((parseFloat(valor) * parseFloat(porcentaje))/100);
    ($(obj).parent()).closest('td').next('td').find('input').val(total);        
    
}

// ELIMINAR ROWS Y CAMPOS

function removeRow(input) {
    document.getElementById('rows').removeChild(input.parentNode);
}

function borrarCamposValoresCoord()
{    
    document.getElementById("form").reset(); 
    $("#rows").empty()   
    $('#modalDetalleValorCoordinadorDelete').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove(); 
    cargarValoresPorCoordinador();
}

function cargarValoresDeleteModal(id)
{
    var idCab = id;        
    $.ajax
        ({
            type: 'POST',
            url: 'server/valores/coordinadores/sv_valores_coord.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarDatosEdit' },
            success: function (response) 
            {                                                                                   
                $("#anioModalDelete").val(response.result.val_anio);                
                $("#periodoModalDelete").val(response.result.val_periodo);
                $("#periodoHidden").val(response.result.val_periodo);                
                $("#idCoordinadorModalDelete").val(response.result.val_id_coord);
                $("#idFinanciadorModalDelete").val(response.result.val_id_finan);  
                $("#idCabModalDelete").val(idCab);                  
            },        
            fail: function (response) {    
                alert("fail");                               
            }
        }).done(function()
        {          
            cargarDetallesValoresDelete(idCab);
        });       
}


function cargarDetallesValoresDelete(idCab)
{    
    $("#rowsDelete").empty() 
    var id=1;
    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/valores/coordinadores/sv_valores_coord.php',
        dataType: 'json',
        data: { idCab:idCab, accion:'buscarDetallesEdit' },
        success: function (response) 
        {
            $.each(response, function(idx, obj) 
            {                                                        
                addRowDelete(id, obj);                                   
                id++;   
            });                                                  
        },  
        complete: function()
        {
            // $('#modalFinanciadorEdit').modal('show');
        },
        fail: function (response) {    
            alert("fail");                               
        }
    });    
}

function addRowDelete(id, obj)
{               
    var id = id;

    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/valores/coordinadores/div_modal_valores_delete.php',
        success: function(response)
        {                                                
            var div = document.createElement('div');
            div.innerHTML = response;
            document.getElementById('rowsDelete').append(div);            
            setIdDelete(obj); 

        },
        fail: function(response)
        {           
        }
    });        
}

function setIdDelete(obj)
{                
    var obj = obj;
    var nro = parseInt($("#numeroIdDelete").val(), 10) + 1;    
    $("#numeroIdDelete").val(nro);
    
    var prestacion = document.getElementById('selectPrestacionDelete');    
    prestacion.setAttribute("id", "selectPrestacionDelete_"+nro); 

    var idDet = document.getElementById('idDetModalDelete');
    idDet.setAttribute("id", "idDetModalDelete_"+nro); 
   
    var amba = document.getElementById('val_ambaDelete');    
    amba.setAttribute("id", "val_ambaDelete_"+nro);      
    
    var btnBorrar = document.getElementById('btnBorrarPrestacion');
    btnBorrar.setAttribute("id", "btnBorrarPrestacion"+nro);  
    

    var cordonUno = document.getElementById('val_cord1Delete');    
    cordonUno.setAttribute("id", "val_cord1Delete_"+nro);    

    var cordonDos = document.getElementById('val_cord2Delete');    
    cordonDos.setAttribute("id", "val_cord2Delete_"+nro);

    $("#selectPrestacionDelete_"+nro).val(obj.val_det_id_prest);         
    $("#val_ambaDelete_"+nro).val(obj.val_det_valor_amba);
    $("#val_cord1Delete_"+nro).val(obj.val_det_valor_cordUno);
    $("#val_cord2Delete_"+nro).val(obj.val_det_valor_cordDos);
    $("#idDetModalDelete_"+nro).val(obj.val_det_id);
    $("#btnBorrarPrestacion"+nro).val(obj.val_det_id);

}

function borrarPrestacion(obj)
{
    var idCab = $("#idCabModalDelete").val();
    var idDet = $(obj).val();
    $.ajax
    ({
        type: 'POST',
        url: 'server/valores/coordinadores/sv_valores_coord.php',
        data: { idDet:idDet,idCab:idCab, accion:'borrarDetalle' },
        success: function (response) 
        {                                           
            if(response=="delete")
            {
                $('#modalDetalleValorCoordinadorDelete').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove(); 
                cargarValoresCoord();
            }   
            else
            {
                cargarDetallesValoresDelete(idCab);
            }
            
        },
        fail: function (response) {    
            alert("fail");                                             
        }
    });    
}

function borrarValoresCoordinador()
{
    var idCab = $("#idCabModalDelete").val();
    $.ajax
    ({
        type: 'POST',
        url: 'server/valores/coordinadores/sv_valores_coord.php',
        data: { idCab:idCab, accion:'borrarValores' },
        success: function (response) 
        {                         
            $('#modalDetalleValorCoordinadorDelete').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();                 
            cargarValoresPorCoordinador();
        },
        fail: function (response) {    
            alert("fail");                                             
        }
    });    
}