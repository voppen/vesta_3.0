
function cargarFacturacionPaciente()
{
    var paciente = $("#idPaciente").val(); 
    if(paciente == "")
    {
        alert("Seleccionar Paciente");
    }
    var anio= $("#anio").val();
    if(anio == "")
    {
        alert("Seleccionar Año");
    }
    var periodo = $("#periodo").val();
    if(periodo == "")
    {
        alert("Seleccionar Periodo");
    }

    $.ajax
    ({
        type: 'POST',
        async:false,        
        url: 'ABMS/facturacion/tabla_fact_pacientes.php',
        data: { paciente:paciente, anio:anio, periodo:periodo },
        done: function(response)
        {
            alert("done");
        },
        success: function (response) 
        {                                                      
            $('#tableFacturacionPacientes').html(response);            
            //calcularRegistroFacturacion(response);                 
        },

        fail: function (response) {    
            alert("fail");                                             
        }
    });         
}

function calcularRegistroFacturacion(response)
{
    var prestacion;
    var tipoPrestacion;
    var totalModulos;
    var totalHoras;
    var totalSesiones;
    
    $.each(response.data.hiscliDet, function(key, value) 
    {
        prestacion = value.hiscli_det_id_prest;
        tipoPrestacion = value.hiscli_det.prest_mod;
        totalModulos = value.hiscli_det.hiscli_det_total_mod;
        totalHoras = value.hiscli_det.hiscli_det_total_hs;
        totalSesiones = value.hiscli_det.hiscli_det_total_ses;        
        
    });                                           

}


function calcularFacturacion()
{    
    alert("fact");
    var count= 1;
    var prestacion = $("#prestacionCreateHiscli_" + count).val();  
    var paciente = $("#pacientesEditHiscli").val();
    // var action = action; 
    // var idCab = idCab;      
    
    var anio = (new Date()).getFullYear();
    var periodo = (new Date().getMonth()+1);

    var valorPrestacion;

    alert(prestacion);

    while($("#prestacionCreateHiscli_" + count).val()!=undefined)
    {        
        alert("hile");
        
        // var idDetalle = $("#idDetalleCreateHiscli_" + count).val();
        prestacion = $("#prestacionCreateHiscli_" + count).val();
        var tipoPrestacion;        
        var fechaAlta = $("#fechaAltaPrestacionCreateHiscli_" + count).val();
        var fechaBaja = $("#fechaBajaPrestacionCreateHiscli_" + count).val();     
        var total;        
        
        if(fechaAlta.substr(0,4)<=anio && fechaAlta.substr(5,2)<=periodo)
        {
            if(fechaBaja.substr(0,4)>=anio && fechaBaja.substr(5,2)>=periodo)
            {                
                tipoPrestacion = buscarTipoPrestacion(prestacion);     
                alert(tipoPrestacion);                           
                switch (tipoPrestacion.trim()) 
                {                    
                    case 'M':                    
                    var total = $("#totalModulosCreateHiscli_" + count).val();                      
                    break;
                    case 'H':                    
                    var total = $("#totalHorasCreateHiscli_" + count).val();  
                    break;
                    case 'S':                       
                    var total = $("#totalSesionesCreateHiscli_" + count).val();
                    alert(total);                                      
                    break;
                }
                
                calcularGuardarValorTotalPrestacion(anio, periodo, prestacion, paciente, total);                
            }
        }                           

        count++;
    }
}

function calcularGuardarValorTotalPrestacion(anio, periodo, prestacion, paciente, total)
{    
    var cordon;
    var totalPrestacion;
    var total = total;    
    $.ajax
    ({
        type: 'POST',
        url: 'server/facturacion/sv_facturacion_paciente.php',
        dataType:'json',
        async: false,            
        data: { anio:anio, periodo:periodo, prestacion:prestacion, paciente:paciente,  accion: "buscarValorPaciente" },
        success: function (response) 
        {                       
            $.each(response.data.valores, function(key, value) 
            {                
                cordon = value.pac_cordon;     
                alert(cordon);           
                valorUno = value.val_det_valor_amba;  
                valorDos = value.val_det_valor_cordUno;
                valorTres = value.val_det_valor_cordDos;                     
                switch (cordon) {
                    case '1':                                        
                        totalPrestacion = total * valorUno;     
                        alert(valorUno);
                        alert(total);
                        alert(totalPrestacion);                   
                        break;
                    case '2':
                        totalPrestacion = total * valorDos;
                        alert(valorDos);
                        alert(totalPrestacion); 
                        break;  
                    case '3':
                        totalPrestacion = total * valorTres;
                        alert(valorTres);
                        alert(totalPrestacion); 
                        break;                
                }                  
                
                guardarFacturacion(anio, periodo, prestacion, paciente, totalPrestacion);
                
            });                                                    
        
        },
        fail: function (response) 
        {    
            alert("error");                             
        }
    });                 
}

function buscarTipoPrestacion(prestacion)
{
    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        data: { prestacion:prestacion, accion:"buscarModPrestacion" },
        success: function (response) 
        {       
            modPrestacion = response;                           
        },
        fail: function (response) {    
            alert("error");                                             
        }
    });        
    return modPrestacion;
}

function guardarFacturacion(anio, periodo, prestacion, paciente, totalPrestacion)
{
    $.ajax
    ({
        type: 'POST',
        url: 'server/facturacion/sv_facturacion_paciente.php',        
        async: false,            
        data: { anio:anio, periodo:periodo, prestacion:prestacion, paciente:paciente, total:totalPrestacion,  accion: "guardarFacturacion" },
        success: function (response) 
        {
            // cargarFacturacionPaciente();                                               
        },
        fail: function (response) 
        {    
            alert("error");                             
        }
    });             
}