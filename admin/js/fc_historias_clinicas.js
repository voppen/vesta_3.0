$('.form_edit').click(function(){
    let pac_num_afil = $(this).attr("id");
    $("#pac_num_afil_edit").val(pac_num_afil);
    $("#pac_nombre_edit").val($("#hiscli_"+pac_num_afil).attr("value"));
})


//BORRAR
function deleteHiscli(id){
    $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            data: { id: id, accion: 'borrarHiscli' },
            success: function (response) {                                
                    $('#modalHistoriaClinicaDelete').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    cargarHistoriasClinicas();      
            },
            fail: function (response) {    
                alert("error");                         
            }
        });
}

//END BORRAR

//LEER HISTORIAS CLINICAS
function buscarInternaciones(newInt)
{
    var estado = $("#estadoCreateHiscli").val();    
    if(estado == "Internado")
    {        
        $.ajax
        ({
            type:'POST',
            url: '../admin/ABMS/historias_clinicas/divInternaciones.php',
            success: function(response)
            {
                $('.newInternacion').remove();
                $('#divInternaciones').append(response);
                if(newInt != "newInt")
                {
                    let boton = '<button type="button" onclick="buscarInternaciones()" class="btn btn-success newInternacion" >Nueva internación</button>';
                    $('#divInternaciones').append(boton);
                }
                setIdDivInternaciones($('.tablaInternacion').length);
                
            },
            fail: function(response)
            {
                $("#divInternaciones").append(retorno.responseText);
            }
        });
    }
    else
    {
        $('#divInternaciones').html("");
    }    
}

function addRowPrestacionHistoriasClinicas()
{
    $.ajax
        ({
            type:'POST',
            url: '../admin/ABMS/historias_clinicas/divPrestacion.php',
            success: function(response)
            {
                var div = document.createElement('div');
                div.innerHTML = response;
                document.getElementById('divPrestaciones').append(div);
                document.getElementsByClassName("modulos")[0].style.display = 'none';
                document.getElementsByClassName("modulos")[1].style.display = 'none';
                document.getElementsByClassName("horas")[0].style.display = 'none';
                document.getElementsByClassName("horas")[1].style.display = 'none';
                document.getElementsByClassName("sesiones")[0].style.display = 'none';
                document.getElementsByClassName("sesiones")[1].style.display = 'none';
            },
            fail: function(response)
            {
                $("#divPrestaciones").html(retorno.responseText);
            }
        });
}

function seleccionarPrestacion(obj)
{        
    var prestacion = $(obj).val();
    var modPrestacion;
    var idPrest = $(obj).attr('id');    

    var nroId = setId(); 

    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        data: { prestacion:prestacion, accion:"buscarModPrestacion" },
        success: function (response) 
        {       
            modPrestacion = response;               
            mostrarRowsPrestacion(modPrestacion, nroId, idPrest);                                       
        },
        fail: function (response) {    
            alert("error");                                             
        }
    });        
}

function seleccionarPrestacionEdit(prestacion)
{    
    var modPrestacion;

    var nroId = setId(); 

    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        data: { prestacion:prestacion, accion:"buscarModPrestacion" },
        success: function (response) 
        {       
            modPrestacion = response;               
            mostrarRowsPrestacion(modPrestacion, nroId, "prestacionCreateHiscli");                                       
        },
        fail: function (response) {    
            alert("error");                                             
        }
    });        
}

function mostrarRowsPrestacion(modPrestacion, prestacion, idPrest)
{
    var modPrestacion = modPrestacion.trim();      
    var prest = prestacion;      
    if(idPrest != undefined)
    {
        var id = idPrest.substring(idPrest.indexOf("_")+1, idPrest.length);        
    }
    else
    {
        idPrest = "";
    }
    

    if(idPrest == "prestacionCreateHiscli")
    {        
        $("#tableRows_" + prest).show();    
    
        if(modPrestacion == 'M')
        {                       
            document.getElementsByClassName("modulos_" + prest)[0].style.display = 'table-row';
            document.getElementsByClassName("modulos_" + prest)[1].style.display = 'table-row';

            document.getElementsByClassName("horas_" + prest)[0].style.display = 'none';
            document.getElementsByClassName("horas_" + prest)[1].style.display = 'none'; 
            document.getElementsByClassName("horas_" + prest)[2].style.display = 'none';
            document.getElementsByClassName("horas_" + prest)[3].style.display = 'none'; 

            document.getElementsByClassName("sesiones_" + prest)[0].style.display = 'none';        
            document.getElementsByClassName("sesiones_" + prest)[1].style.display = 'none';    
        }
        else
        if(modPrestacion == 'H')
        {        
            document.getElementsByClassName("horas_" + prest)[0].style.display = 'table-row';
            document.getElementsByClassName("horas_" + prest)[1].style.display = 'table-row';  
            document.getElementsByClassName("horas_" + prest)[2].style.display = 'table-row';
            document.getElementsByClassName("horas_" + prest)[3].style.display = 'table-row';   
            
            document.getElementsByClassName("modulos_" + prest)[0].style.display = 'none';
            document.getElementsByClassName("modulos_" + prest)[1].style.display = 'none';

            document.getElementsByClassName("sesiones_" + prest)[0].style.display = 'none';        
            document.getElementsByClassName("sesiones_" + prest)[1].style.display = 'none'; 
        }
        else
        if(modPrestacion == 'S')
        {
            document.getElementsByClassName("sesiones_" + prest)[0].style.display = 'table-row';        
            document.getElementsByClassName("sesiones_" + prest)[1].style.display = 'table-row';   
            
            document.getElementsByClassName("horas_" + prest)[0].style.display = 'none';
            document.getElementsByClassName("horas_" + prest)[1].style.display = 'none'; 
            document.getElementsByClassName("horas_" + prest)[2].style.display = 'none';
            document.getElementsByClassName("horas_" + prest)[3].style.display = 'none';

            document.getElementsByClassName("modulos_" + prest)[0].style.display = 'none';
            document.getElementsByClassName("modulos_" + prest)[1].style.display = 'none';
        }   
    }
    else
    {        
        document.getElementsByClassName("modulos_" + id)[0].style.display = 'none';
        document.getElementsByClassName("modulos_" + id)[1].style.display = 'none';  

        document.getElementsByClassName("horas_" + id)[0].style.display = 'none';
        document.getElementsByClassName("horas_" + id)[1].style.display = 'none'; 
        document.getElementsByClassName("horas_" + id)[2].style.display = 'none';
        document.getElementsByClassName("horas_" + id)[3].style.display = 'none';  

        document.getElementsByClassName("sesiones_" + id)[0].style.display = 'none';        
        document.getElementsByClassName("sesiones_" + id)[1].style.display = 'none'; 

        if(modPrestacion == 'M')
        {                       
            document.getElementsByClassName("modulos_" + id)[0].style.display = 'table-row';
            document.getElementsByClassName("modulos_" + id)[1].style.display = 'table-row';                        
        }
        else
        if(modPrestacion == 'H')
        {        
            document.getElementsByClassName("horas_" + id)[0].style.display = 'table-row';
            document.getElementsByClassName("horas_" + id)[1].style.display = 'table-row';
            document.getElementsByClassName("horas_" + id)[2].style.display = 'table-row';
            document.getElementsByClassName("horas_" + id)[3].style.display = 'table-row';             
        }
        else
        if(modPrestacion == 'S')
        {
            document.getElementsByClassName("sesiones_" + id)[0].style.display = 'table-row';        
            document.getElementsByClassName("sesiones_" + id)[1].style.display = 'table-row';        
        }   
    }
       
    
}


function setId()
{                
    var prest = parseInt($("#numeroId").val(), 10) + 1;    
        
    $("#numeroId").val(prest);
    
    var containerPrestacion = document.getElementById('containerPrestaciones');
    if(containerPrestacion != null)
    {    
        containerPrestacion.setAttribute("id", "containerPrestaciones_" + prest);
        var tablaPrestacion = document.getElementById('tableRows');
        tablaPrestacion.setAttribute("id", "tableRows_" + prest);
        
        var prestacion = document.getElementById('prestacionCreateHiscli');    
        prestacion.setAttribute("id", "prestacionCreateHiscli_" + prest);  
                
        
        var fechaAlta = document.getElementById('fechaAltaPrestacionCreateHiscli');       
        fechaAlta.setAttribute("id", "fechaAltaPrestacionCreateHiscli_"+prest);          

        var fechaBaja = document.getElementById('fechaBajaPrestacionCreateHiscli');        
        fechaBaja.setAttribute("id", "fechaBajaPrestacionCreateHiscli_"+prest); 
        
        var profesional = document.getElementById('profesionalCreateHiscli');    
        profesional.setAttribute("id", "profesionalCreateHiscli_"+prest); 
        
        var coordinador = document.getElementById('coordinadorCreateHiscli');    
        coordinador.setAttribute("id", "coordinadorCreateHiscli_"+prest); 
        
        var frecuencia = document.getElementById('frecuenciaCreateHiscli');    
        frecuencia.setAttribute("id", "frecuenciaCreateHiscli_"+prest); 

        var modulos = document.getElementById('modulosCreateHiscli')
        modulos.setAttribute("id", "modulosCreateHiscli_"+prest); 

        var totalMdsPorMes = document.getElementById('totalModulosCreateHiscli');    
        totalMdsPorMes.setAttribute("id", "totalModulosCreateHiscli_"+prest); 

        var totalHorasVisita = document.getElementById('visitasHorasCreateHiscli');
        totalHorasVisita.setAttribute("id", "visitasHorasCreateHiscli_"+prest); 

        var horasVisita = document.getElementById('horasCreateHiscli');    
        horasVisita.setAttribute("id", "horasCreateHiscli_"+prest); 

        var totalHsPorMes = document.getElementById('totalHorasCreateHiscli');    
        totalHsPorMes.setAttribute("id", "totalHorasCreateHiscli_"+prest); 

        var visitas = document.getElementById('sesionesCreateHiscli');    
        visitas.setAttribute("id", "sesionesCreateHiscli_"+prest); 

        var totalVisitas = document.getElementById('totalSesionesCreateHiscli');    
        totalVisitas.setAttribute("id", "totalSesionesCreateHiscli_"+prest); 
        
        var observaciones = document.getElementById('obserCreateHiscli');    
        observaciones.setAttribute("id", "obserCreateHiscli_"+prest); 

        var btnDelete = document.getElementById('btnDeletePrest');    
        btnDelete.setAttribute("prest",prest);
        btnDelete.setAttribute("id", "btnDeletePrest_"+prest); 

        var idDetalle = document.getElementById('idDetalleCreateHiscli');    
        idDetalle.setAttribute("id", "idDetalleCreateHiscli_"+prest); 


        document.getElementsByClassName("modulos")[0].className = 'modulos_' + prest;
        document.getElementsByClassName("modulos")[0].className = 'modulos_' + prest;

        document.getElementsByClassName("horas")[0].className = 'horas_' + prest;
        document.getElementsByClassName("horas")[0].className = 'horas_' + prest;
        document.getElementsByClassName("horas")[0].className = 'horas_' + prest;
        document.getElementsByClassName("horas")[0].className = 'horas_' + prest;

        document.getElementsByClassName("sesiones")[0].className = 'sesiones_' + prest;
        document.getElementsByClassName("sesiones")[0].className = 'sesiones_' + prest;
        
        return prest;
    }
    

    
}

//CREATE HISTORIA CLINICA

function guardarHistoriaClinicaNueva()
{
    var paciente = $("#pacientesCreateHiscli").val();
    var motivoIngreso = $("#motivoIngresoCreateHiscli").val();
    var fechaAlta = $("#fechaAltaCreateHiscli").val();    
    var fechaBaja = $("#fechaBajaCreateHiscli").val();    
    var estado = $("#estadoCreateHiscli").val();
    var descripcion = $("#descripcionCreateHiscli").val();    

    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        data: { paciente:paciente, motivoIngreso:motivoIngreso, fechaAlta:fechaAlta, fechaBaja:fechaBaja, estado:estado, descripcion:descripcion, accion:"guardarCab" },
        success: function (response) 
        {               
            let paciente = $('#pacientesCreateHiscli').val();
            guardarInternaciones(response,paciente);                                  
            guardarDetallesHistoriaClinica(response);                                
        },
        fail: function (response) {    
            alert("error");                                             
        }
    });
}

function guardarDetallesHistoriaClinica(idCab)
{    
    var count= 1;
    var prestacion = $("#prestacionCreateHiscli_" + count).val();   
    var action = action; 
    var idCab = idCab;     
    while($("#prestacionCreateHiscli_" + count).val()!=undefined)
    {
        prestacion = $("#prestacionCreateHiscli_" + count).val();
        var fechaAlta = $("#fechaAltaPrestacionCreateHiscli_" + count).val();
        var fechaBaja = $("#fechaBajaPrestacionCreateHiscli_" + count).val();            
        var profesional = $("#profesionalCreateHiscli_" + count).val();                
        var coordinador = $("#coordinadorCreateHiscli_" + count).val();        
        var frecuencia = $("#frecuenciaCreateHiscli_" + count).val();    
        
        var cantModulos = $("#modulosCreateHiscli_" + count).val();    
        var totalModulos = $("#totalModulosCreateHiscli_" + count).val();    

        var cantVisitasPorHoras = $("#visitasHorasCreateHiscli_" + count).val();    
        var horasVisita = $("#horasCreateHiscli_" + count).val();    
        var totalHorasVisita = $("#totalHorasCreateHiscli_" + count).val();    

        var cantSesiones = $("#sesionesCreateHiscli_" + count).val();    
        var totalSesiones = $("#totalSesionesCreateHiscli_" + count).val();    
     
        var observaciones = $("#obserCreateHiscli_" + count).val();                        

        $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            async: false,
            data: { idCab: idCab, prestacion:prestacion, fechaAlta:fechaAlta, fechaBaja:fechaBaja, 
                profesional:profesional, coordinador:coordinador, frecuencia:frecuencia, 
                cantModulos: cantModulos, totalModulos: totalModulos, cantVisitasPorHoras: cantVisitasPorHoras,
                horasVisita:horasVisita, totalHorasVisita: totalHorasVisita, cantSesiones: cantSesiones,
                totalSesiones: totalSesiones, observaciones:observaciones ,  accion: "guardarDet" },
            success: function (response) 
            {          
                cargarHistoriasClinicas();                                                
            },
            fail: function (response) 
            {    
                alert("error");                             
            }
        });             

        count++;
    }
}

function guardarInternaciones(idCab, paciente){
    let increment = 1;
    let maximo = $('.tablaInternacion').length;
    while (increment <= maximo) {
        let motivo = $('#hiscli_ingreso_inter_edit_' +increment).val();
        let alta = $('#hiscli_fecha_alta_inter_edit_' +increment).val();
        let baja = $('#hiscli_fecha_baja_inter_edit_' +increment).val();
        $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            data: { motivo:motivo, alta:alta, baja:baja, idCab:idCab, paciente:paciente, accion:'guardarInternacion' },
            success: function(response){
                console.log("Se guardaron las internaciones.");
            },
            fail: function(){
                console.log("ERROR.");
                console.log(response);
            }
        }).done(function()
        {          

        });   
        increment++;
    }
}


// CARGAR HISTORIAS CLINICAS DETALLES

function cargarEdicionHistoriaClinica(obj)
{
    var idCab = $(obj).attr('id');
    var responseCab;    

    $.ajax
    ({
        type: 'POST',
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        async: false,
        dataType: 'json',
        data: { idCab:idCab, accion:'buscarHistoriaClinicaCab' },
        success: function (responseData) 
        {                                                  
            responseCab = responseData;              
        },        
        fail: function (response) {    
            alert("error");                               
        }
    }).done(function()
    {                  
        //cargarDetallesValoresEdit(idCab);
    });       

    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/historias_clinicas/form_editarHistClinica.php',
        success: function(response)
        {                        
            $('#principal').html(response);  
            $("#cabecera").val(idCab);          
            $("#pacientesEditHiscli").val(responseCab.result.hiscli_id_paciente);
            $("#motivoIngresoEditHiscli").val(responseCab.result.hiscli_ingreso);
            $("#fechaAltaEditHiscli").val(responseCab.result.hiscli_fecha_alta);
            $("#fechaBajaEditHiscli").val(responseCab.result.hiscli_fecha_baja);
            $("#estadoEditHiscli").val(responseCab.result.hiscli_estado);
            $("#descripcionEditHiscli").val(responseCab.result.hiscli_descripcion);            
            buscarInternacionesEdicionHistoriaClinica(idCab);
            buscarPrestacionesEdicionHistoriaClinica(idCab); 
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });       

}

function  buscarInternacionesEdicionHistoriaClinica(idCab)
{
    $idCab = idCab;      
    var count = 1;
    var internacion = [];        

    $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarInternacionesHistoriaClinica' },
            success: function (response) 
            {                                    
                $.each(response.data.internaciones, function(key, value) 
                {
                    alert("entra"); 
                    $("#titleInternacionesHidden").show();
                    $("#titleInternacion").hide();
                    $.ajax
                    ({
                        type:'POST',
                        url: '../admin/ABMS/historias_clinicas/divInternaciones.php',
                        success: function(response)
                        {                          
                            var div = document.createElement('div');
                            div.innerHTML = response;
                            document.getElementById('divInternaciones').append(div);
                            setIdDivInternaciones(count);
                            setValuesDivInternaciones(count, value);
                            count++;                                                     
                        },
                        fail: function(response)
                        {
                            $("#divInternaciones").html(retorno.responseText);
                        }
                    });
                });  
                if (count > 1) {
                    $('#titleInternaciones').attr('hidden',false);
                }         
            },        
            fail: function (response) {    
                alert("error");                               
            }
        }).done(function()
        {          
        });   
}

function setIdDivInternaciones(count)
{
    var count = count;

    var ingresoInternacion = document.getElementById('hiscli_ingreso_inter_edit');
    ingresoInternacion.setAttribute("id", "hiscli_ingreso_inter_edit_" + count);

    var fechaAltaInternacion = document.getElementById('hiscli_fecha_alta_inter_edit');
    fechaAltaInternacion.setAttribute("id", "hiscli_fecha_alta_inter_edit_" + count);

    var fechaBajaInternacion = document.getElementById('hiscli_fecha_baja_inter_edit');
    fechaBajaInternacion.setAttribute("id", "hiscli_fecha_baja_inter_edit_" + count);

    var numInternacion = document.getElementById('hiscli_num_inter_edit');
    numInternacion.setAttribute("id", "hiscli_num_inter_edit_" + count);    
}

function setValuesDivInternaciones(idInter, internacion)
{

    $("#hiscli_ingreso_inter_edit_"+idInter).val(internacion.inter_motivo);
    $("#hiscli_fecha_alta_inter_edit_"+idInter).val(internacion.inter_fecha_desde);
    $("#hiscli_fecha_baja_inter_edit_"+idInter).val(internacion.inter_fecha_hasta);
    $("#hiscli_num_inter_edit_"+idInter).val(internacion.inter_id);
}

function buscarPrestacionesEdicionHistoriaClinica(idCab)
{
    $idCab = idCab;      
    var count = 1;       
    $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarPrestacionesHistoriaClinica' },
            success: function (response) 
            {                                       
                $.each(response.data.prestaciones, function(key, value) 
                {                    
                    // $("#titleInternacionesHidden").show();
                    $("#titleInternacion").hide();
                    $.ajax
                    ({
                        type:'POST',
                        url: '../admin/ABMS/historias_clinicas/divPrestacion.php',
                        success: function(response)
                        {                        
                            var div = document.createElement('div');
                            div.innerHTML = response;
                            document.getElementById('divPrestaciones').append(div);
                            //setIdDivPrestaciones(count);
                            setValuesDivPrestaciones(count, value);
                            count++;                                                        
                        },
                        fail: function(response)
                        {
                            $('divPrestaciones').html(retorno.responseText);
                        }
                    });
                });      
            },        
            fail: function (response) {    
                alert("error");                               
            }
        }).done(function()
        {          
        });   
}

function setIdDivPrestaciones(count)
{
    var count = count;

    var fechaAltaPrestacion = document.getElementById('fechaAltaPrestacionCreateHiscli');
    fechaAltaPrestacion.setAttribute("id", "fechaAltaPrestacionCreateHiscli_" + count);

    var fechaBajaPrestacion = document.getElementById('fechaBajaPrestacionCreateHiscli');
    fechaBajaPrestacion.setAttribute("id", "fechaBajaPrestacionCreateHiscli_" + count);

    var profesionalPrestacion = document.getElementById('profesionalCreateHiscli');
    profesionalPrestacion.setAttribute("id", "profesionalCreateHiscli_" + count);

    var CoordinadorPrestacion = document.getElementById('coordinadorCreateHiscli');
    CoordinadorPrestacion.setAttribute("id", "coordinadorCreateHiscli_" + count);

    var frecuenciaPrestacion = document.getElementById('frecuenciaCreateHiscli');
    frecuenciaPrestacion.setAttribute("id", "frecuenciaCreateHiscli_" + count);

    var cantVisitasPrestacion = document.getElementById('visitasCreateHiscli');
    cantVisitasPrestacion.setAttribute("id", "visitasCreateHiscli_" + count);
    
    var visitasMesPrestacion = document.getElementById('totalVisitasCreateHiscli');
    visitasMesPrestacion.setAttribute("id", "totalVisitasCreateHiscli_" + count);

    var observacionesPrestacion = document.getElementById('obserCreateHiscli');
    observacionesPrestacion.setAttribute("id", "obserCreateHiscli_" + count);

    
}

function setValuesDivPrestaciones(idInter, prestacion)
{
    seleccionarPrestacionEdit(prestacion.hiscli_det_id_prest);    
    $("#prestacionCreateHiscli_"+idInter).attr("disabled",true).val(prestacion.hiscli_det_id_prest);
    $("#idDetalleCreateHiscli_"+idInter).val(prestacion.hiscli_det_id);
    $("#fechaAltaPrestacionCreateHiscli_"+idInter).val(prestacion.hiscli_det_fecha_alta);
    $("#fechaBajaPrestacionCreateHiscli_"+idInter).val(prestacion.hiscli_det_fecha_baja);
    $("#profesionalCreateHiscli_"+idInter).val(prestacion.hiscli_det_profesional);
    $("#coordinadorCreateHiscli_"+idInter).val(prestacion.hiscli_det_id_coord);
    $("#frecuenciaCreateHiscli_"+idInter).val(prestacion.hiscli_det_frecuencia);    
    $("#obserCreateHiscli_"+idInter).val(prestacion.hiscli_det_observaciones);
        
    $("#modulosCreateHiscli_"+idInter).val(prestacion.hiscli_det_cant_mod);
    $("#totalModulosCreateHiscli_"+idInter).val(prestacion.hiscli_det_total_mod);
    
    $("#visitasHorasCreateHiscli_"+idInter).val(prestacion.hiscli_det_cant_vist_hs);
    $("#horasCreateHiscli_"+idInter).val(prestacion.hiscli_det_cant_hs);
    $("#totalHorasCreateHiscli_"+idInter).val(prestacion.hiscli_det_total_hs);
    
    $("#sesionesCreateHiscli_"+idInter).val(prestacion.hiscli_det_cant_ses);
    $("#totalSesionesCreateHiscli_"+idInter).val(prestacion.hiscli_det_total_ses);
}

function cargarDetalles2(idCab){
        $.ajax
        ({
            type:'POST',
            url: '../admin/ABMS/historias_clinicas/historia_clinica_detalle.php',
            data: {idCab:idCab},
            success: function(response)
            {
                $('#principal').html(response);
                buscarInternacionesHistoriaClinica(idCab);
            },
            fail: function(response)
            {
                $("#principal").html(retorno.responseText);
            }
        });
}

function cargarDetallesHistoriaClinica(obj)
{
    var idCab = $(obj).attr('id');
    var responseCab;

    $.ajax
    ({
        type: 'POST',
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        async: false,
        dataType: 'json',
        data: { idCab:idCab, accion:'buscarHistoriaClinicaCab' },
        success: function (responseData) 
        {                               
            responseCab = responseData;  
            buscarPrestacionesEdicionHistoriaClinica(idCab);
            let increment = 1;
            while ($('#containerPrestaciones_' +increment).val() != undefined)
            {
                $('#containerPrestaciones_' +increment+' *').attr("disabled",true)
                increment++;
            }              
        },        
        fail: function (response) {    
            alert("error");                               
        }
    }).done(function()
    {          

        //cargarDetallesValoresEdit(idCab);
    });       

    $.ajax
    ({
        type:'POST',
        url: '../admin/ABMS/historias_clinicas/form_nuevaHistClinica.php',
        success: function(response)
        {
            $('#principal').html(response);
            $("#pacientesCreateHiscli").val(responseCab.result.hiscli_id_paciente);
            $("#motivoIngresoCreateHiscli").val(responseCab.result.hiscli_ingreso);
            $("#fechaAltaCreateHiscli").val(responseCab.result.hiscli_fecha_alta);
            $("#fechaBajaCreateHiscli").val(responseCab.result.hiscli_fecha_baja);
            $("#estadoCreateHiscli").val(responseCab.result.hiscli_estado);
            $("#descripcionCreateHiscli").val(responseCab.result.hiscli_descripcion);
            $("#divBtnAgregarPrestacion").hide();
            buscarInternacionesHistoriaClinica(idCab);
            buscarPrestacionesHistoriaClinica(idCab);
        },
        fail: function(response)
        {
            $("#principal").html(retorno.responseText);
        }
    });    
}

function buscarPrestacionesHistoriaClinica(idCab)
{
    $idCab = idCab;
    var count = 1;
    var tablePrestaciones = $("#tablePrestaciones");    

    $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarPrestacionesHistoriaClinica' },
            success: function (response) 
            {                                          
                $.each(response.data.prestaciones, function(key, value) 
                {
                    tablePrestaciones.append('<tr><td>'+value.inter_motivo+'</td><td>'+value.inter_fecha_desde+'</td><td>'+value.inter_fecha_hasta+'</td></tr>');
                    $("#divTableInternaciones").show();
                    $("#tableInternaciones").show();
                });      
            },        
            fail: function (response) {    
                alert("error");                               
            }
        }).done(function()
        {          
        });   
}

function buscarInternacionesHistoriaClinica(idCab)
{
    $idCab = idCab;
    var count = 1;
    var tableInternaciones = $("#tableInternaciones");    

    $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            async: false,
            dataType: 'json',
            data: { idCab:idCab, accion:'buscarInternacionesHistoriaClinica' },
            success: function (response) 
            {                       
                
                $.each(response.data.internaciones, function(key, value) 
                {
                    tableInternaciones.append('<tr><td>'+value.inter_motivo+'</td><td>'+value.inter_fecha_desde+'</td><td>'+value.inter_fecha_hasta+'</td></tr>');
                    $("#divTableInternaciones").show();
                    $("#tableInternaciones").show();
                });      
            },        
            fail: function (response) {    
                alert("error");                               
            }
        }).done(function()
        {          
        });       

}

function setIdTableInternaciones(count)
{
    $count = count;
    document.getElementById('tableInternacion').setAttribute("id", "tableInternacion_" + count);
    document.getElementById('hiscli_ingreso_edit').setAttribute("id", "hiscli_ingreso_edit_" + count);
    document.getElementById('hiscli_fecha_alta_edit').setAttribute("id", "hiscli_fecha_alta_edit_" + count);
    document.getElementById('hiscli_fecha_baja_edit').setAttribute("id", "hiscli_fecha_baja_edit_" + count);    
}

function habilitarDivPrestacion()
{    
    $.ajax
        ({
            type:'POST',
            url: '../admin/ABMS/historias_clinicas/divPrestacionHoras.php',
            success: function(response)
            {                
                var div = document.createElement('div');
                div.innerHTML = response;
                document.getElementById('prestacionRow').append(div);               
                // setId(); 
            },
            fail: function(response)
            {
                $("#prestacionRow").html(retorno.responseText);
            }
        });
    
}


//REINICIAR CAMPOS

function borrarCampos() 
{
    document.getElementById("form").reset();
    $("#errorMsg").hide();
    $("#errorMsg").html("");
}

$(".alertaDias").each(function(){
    let dias = $(this).attr("value");
    if (dias >= 0 && dias <= 5) {
        $(this).attr("hidden", false);
        $(this).parent().parent().addClass("table-danger");
    }
})


function guardarHisCliCabEdit()
{
    var paciente = $("#pacientesEditHiscli").val();
    var motivoIngreso = $("#motivoIngresoEditHiscli").val();
    var fechaAlta = $("#fechaAltaEditHiscli").val();    
    var fechaBaja = $("#fechaBajaEditHiscli").val();    
    var estado = $("#estadoEditHiscli").val();
    var descripcion = $("#descripcionEditHiscli").val();    
    var idCab = $("#cabecera").val();

    $.ajax
    ({
        type: 'POST',
        async:false,
        url: 'server/historias_clinicas/sv_historias_clinicas.php',
        data: { idCab:idCab, paciente:paciente, motivoIngreso:motivoIngreso, fechaAlta:fechaAlta, fechaBaja:fechaBaja, estado:estado, descripcion:descripcion, accion:"guardarCabEdit" },
        success: function (response) 
        {               
            let paciente = $('#pacientesEditHiscli').val();
            // guardarInternacionesEdit(response,paciente);                      
            if(response == 0)
            {
                response = idCab;
            }            
            guardarDetallesHistoriaClinicaEdit(response);                                
        },
        fail: function (response) {    
            alert("error");                                             
        }
    });
}

function guardarInternacionesEdit(idCab, paciente){
    let increment = 1;
    let maximo = $('.tablaInternacion').length;
    while (increment <= maximo) {
        let motivo = $('#hiscli_ingreso_inter_edit_' +increment).val();
        let alta = $('#hiscli_fecha_alta_inter_edit_' +increment).val();
        let baja = $('#hiscli_fecha_baja_inter_edit_' +increment).val();
        let numInter = $("#hiscli_num_inter_edit_" +increment).val();
        $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            data: { numInter:numInter, motivo:motivo, alta:alta, baja:baja, idCab:idCab, paciente:paciente, accion:'guardarInternacionEdit' },
            success: function(response){
                console.log("Se guardaron las internaciones.");
            },
            fail: function(){
                console.log("ERROR.");
                console.log(response);
            }
        }).done(function()
        {          

        });   
        increment++;
    }
}

function guardarDetallesHistoriaClinicaEdit(idCab)
{    
    var count= 1;
    var prestacion = $("#prestacionCreateHiscli_" + count).val();   
    var action = action; 
    var idCab = idCab;         

    while($("#prestacionCreateHiscli_" + count).val()!=undefined)
    {        
        var idDetalle = $("#idDetalleCreateHiscli_" + count).val();
        prestacion = $("#prestacionCreateHiscli_" + count).val();
        var fechaAlta = $("#fechaAltaPrestacionCreateHiscli_" + count).val();
        var fechaBaja = $("#fechaBajaPrestacionCreateHiscli_" + count).val();            
        var profesional = $("#profesionalCreateHiscli_" + count).val();                
        var coordinador = $("#coordinadorCreateHiscli_" + count).val();        
        var frecuencia = $("#frecuenciaCreateHiscli_" + count).val();       
        
        var cantModulos = $("#modulosCreateHiscli_" + count).val();    
        var totalModulos = $("#totalModulosCreateHiscli_" + count).val();    

        var cantVisitasPorHoras = $("#visitasHorasCreateHiscli_" + count).val();    
        var horasVisita = $("#horasCreateHiscli_" + count).val();    
        var totalHorasVisita = $("#totalHorasCreateHiscli_" + count).val();    

        var cantSesiones = $("#sesionesCreateHiscli_" + count).val();    
        var totalSesiones = $("#totalSesionesCreateHiscli_" + count).val();    
     
        var observaciones = $("#obserCreateHiscli_" + count).val();                        


        $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            async: false,
            // data: { idCab: idCab, idDetalle:idDetalle, prestacion:prestacion, fechaAlta:fechaAlta, fechaBaja:fechaBaja, profesional:profesional, coordinador:coordinador, frecuencia:frecuencia, horasPorVisita:horasPorVisita, visitas:visitas, visitasPorMes: visitasPorMes, observaciones:observaciones ,  accion: "guardarDetEdit" },
            data: { idCab: idCab, idDetalle:idDetalle, prestacion:prestacion, fechaAlta:fechaAlta, fechaBaja:fechaBaja, 
                profesional:profesional, coordinador:coordinador, frecuencia:frecuencia, 
                cantModulos: cantModulos, totalModulos: totalModulos, cantVisitasPorHoras: cantVisitasPorHoras,
                horasVisita:horasVisita, totalHorasVisita: totalHorasVisita, cantSesiones: cantSesiones,
                totalSesiones: totalSesiones, observaciones:observaciones ,  accion: "guardarDetEdit" },
            success: function (response) 
            {          
                cargarHistoriasClinicas();                                                
            },
            fail: function (response) 
            {    
                alert("error");                             
            }
        });             

        count++;
    }
}

function borrarPrestacion(btn){
    if (confirm("¿Deseas eliminar esta prestación?")) {
        let prest = $(btn).attr("prest");
        let id = $("#idDetalleCreateHiscli_"+prest).val();
        $.ajax
        ({
            type: 'POST',
            url: 'server/historias_clinicas/sv_historias_clinicas.php',
            data: {id:id, accion:'borrarDetalle' },
            success: function(response){
                $("#containerPrestaciones_"+prest).html("");
                $("#containerPrestaciones_"+prest).remove();
                alert("La prestación se ha eliminado correctamente.");
            },
            fail: function(){
                console.log("ERROR.");
                console.log(response);
            }
        }).done(function()
        {          

        }); 
    }
}


