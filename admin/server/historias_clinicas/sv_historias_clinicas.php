<?php
include('../db_connect.php');

$accion = $_POST['accion'];

if($accion === "guardarCab")
{   
    $paciente = $_POST['paciente'];
    $motivoIngreso = $_POST['motivoIngreso'];
    $fechaAlta = $_POST['fechaAlta'];
    $fechaBaja = $_POST['fechaBaja'];
    $estado = $_POST['estado'];
    $descripcion = $_POST['descripcion'];

    $sqlInsert = "INSERT INTO hiscli_cab VALUES ('','$paciente', '$motivoIngreso', '$fechaAlta', '$fechaBaja', '$estado', '$descripcion')";         
    $result = mysqli_query($enlace, $sqlInsert);
    $lastId = mysqli_insert_id($enlace);      
    echo $lastId;  
}

if($accion == "guardarInternacion")
{
    $idCab = $_POST['idCab'];
    $paciente = $_POST['paciente'];
    $motivo = $_POST['motivo'];
    $alta = $_POST['alta'];
    $baja = $_POST['baja'];

    $sqlInsert = "INSERT INTO internaciones VALUES ('','$idCab','$paciente','$alta','$baja','$motivo')";
    $result = mysqli_query($enlace, $sqlInsert);
}

if($accion === "guardarDet")
{    
    $idCab = $_POST['idCab'];
    $idPrestacion = $_POST['prestacion'];
    $fechaAlta = $_POST['fechaAlta'];
    $fechaBaja = $_POST['fechaBaja'];
    $profesional = $_POST['profesional'];        
    $coordinador = $_POST['coordinador'];
    $frecuencia = $_POST['frecuencia'];
    $observaciones = $_POST['observaciones'];

    $cantModulos = $_POST['cantModulos'];
    $totalModulos = $_POST['totalModulos'];
    
    $cantVisitasPorHoras = $_POST['cantVisitasPorHoras'];
    $horasVisita = $_POST['horasVisita'];
    $totalHorasVisita = $_POST['totalHorasVisita'];
    
    $cantSesiones = $_POST['cantSesiones'];
    $totalSesiones = $_POST['totalSesiones'];

    $sqlInsert = "INSERT INTO hiscli_det VALUES ('','$idCab', '$idPrestacion','$fechaAlta','$fechaBaja','$coordinador', '$profesional','$frecuencia', '$cantModulos','$totalModulos','$cantSesiones','$totalSesiones','$cantVisitasPorHoras','$horasVisita','$totalHorasVisita','$observaciones')";        
    $result = mysqli_query($enlace, $sqlInsert);    
}

if($accion === "buscarHistoriaClinicaCab")
{    
    $idCab = $_POST["idCab"];
    $sqlSelectEdit = "SELECT * FROM hiscli_cab WHERE hiscli_id='$idCab'";
    $result = mysqli_query($enlace, $sqlSelectEdit); 
    $dataResult = $result->fetch_assoc();
    $data['result'] = $dataResult;
    echo json_encode($data);
}

if($accion == "buscarModPrestacion")
{
    $prestacion = $_POST["prestacion"];

    $sqlSelectPrestaciones = "SELECT prest_mod FROM lista_prestaciones WHERE prest_id='$prestacion'";
    $prest_mod = mysqli_query($enlace, $sqlSelectPrestaciones)->fetch_object()->prest_mod; 
    echo $prest_mod;
}

if($accion == "buscarInternacionesHistoriaClinica")
{
    $idCab = $_POST['idCab'];
    $jsonData["data"]["internaciones"] = array();

    $sqlSelect = "SELECT * FROM internaciones where inter_id_cab='$idCab'";
    $result = mysqli_query($enlace, $sqlSelect); 

    while($row = $result->fetch_object())
    {
        $jsonData["data"]["internaciones"][] = $row;
    }

    echo json_encode($jsonData, JSON_FORCE_OBJECT);
}

if($accion == "buscarPrestacionesHistoriaClinica")
{
    $idCab = $_POST['idCab'];
    $jsonData["data"]["prestaciones"] = array();    

    $sqlSelect = "SELECT * FROM hiscli_det where hiscli_det_cab_id='$idCab'";
    $result = mysqli_query($enlace, $sqlSelect); 

    while($row = $result->fetch_object())
    {
        $jsonData["data"]["prestaciones"][] = $row;
    }

    echo json_encode($jsonData, JSON_FORCE_OBJECT);
}

if ($accion === "borrarHiscli") {
    $idCab = $_POST['id'];
    $sqlDelete = "DELETE FROM hiscli_cab where hiscli_id='$idCab'";
    $result = mysqli_query($enlace, $sqlDelete); 

    $sqlDelete = "DELETE FROM hiscli_det where hiscli_det_cab_id='$idCab'";
    $result = mysqli_query($enlace, $sqlDelete); 
}

if ($accion === "borrarDetalle") {
    $idDet = $_POST['id'];

    $sqlDelete = "DELETE FROM hiscli_det where hiscli_det_id='$idDet'";
    $result = mysqli_query($enlace, $sqlDelete); 
}

if($accion === "guardarCabEdit")
{   
    $paciente = $_POST['paciente'];
    $motivoIngreso = $_POST['motivoIngreso'];
    $fechaAlta = $_POST['fechaAlta'];
    $fechaBaja = $_POST['fechaBaja'];
    $estado = $_POST['estado'];
    $descripcion = $_POST['descripcion'];
    $idCab = $_POST['idCab'];

    $sqlInsert = "UPDATE hiscli_cab SET hiscli_ingreso='$motivoIngreso', hiscli_fecha_alta='$fechaAlta', hiscli_fecha_baja='$fechaBaja', hiscli_estado='$estado', hiscli_descripcion='$descripcion' WHERE hiscli_id='$idCab'";         
    $result = mysqli_query($enlace, $sqlInsert);
    $lastId = mysqli_insert_id($enlace);      
    echo $lastId;  
}

if($accion == "guardarInternacionEdit")
{
    $idCab = $_POST['idCab'];
    $paciente = $_POST['paciente'];
    $motivo = $_POST['motivo'];
    $alta = $_POST['alta'];
    $baja = $_POST['baja'];
    $numInter = $_POST['numInter'];

    $sqlInsert = "UPDATE internaciones SET inter_fecha_desde='$alta', inter_fecha_hasta='$baja',inter_motivo='$motivo' WHERE inter_id='$numInter' AND inter_id_cab='$idCab'";
    $result = mysqli_query($enlace, $sqlInsert);
}

if($accion === "guardarDetEdit")
{        
    $idCab = $_POST['idCab'];
    $idDetalle = $_POST['idDetalle'];
    $idPrestacion = $_POST['prestacion'];
    $fechaAlta = $_POST['fechaAlta'];
    $fechaBaja = $_POST['fechaBaja'];
    $profesional = $_POST['profesional'];        
    $coordinador = $_POST['coordinador'];
    $frecuencia = $_POST['frecuencia'];
    
    $observaciones = $_POST['observaciones'];

    $cantModulos = $_POST['cantModulos'];
    $totalModulos = $_POST['totalModulos'];
    
    $cantVisitasPorHoras = $_POST['cantVisitasPorHoras'];
    $horasVisita = $_POST['horasVisita'];
    $totalHorasVisita = $_POST['totalHorasVisita'];
    
    $cantSesiones = $_POST['cantSesiones'];
    $totalSesiones = $_POST['totalSesiones'];


    if($idDetalle == '')
    {
        $sqlInsert = "INSERT INTO hiscli_det VALUES ('','$idCab', '$idPrestacion','$fechaAlta','$fechaBaja','$coordinador', '$profesional','$frecuencia', '$cantModulos','$totalModulos','$cantSesiones','$totalSesiones','$cantVisitasPorHoras','$horasVisita','$totalHorasVisita','$observaciones')";                
        $result = mysqli_query($enlace, $sqlInsert);    
    }
    else
    {
        $change = 0;
        $sqlSelectPrestaciones = "SELECT * FROM hiscli_det WHERE hiscli_det_id='$idDetalle'";        
        $result = mysqli_query($enlace, $sqlSelectPrestaciones);  

        if ($result->num_rows > 0) 
        {            
            while($row = $result->fetch_assoc()) 
            {
                
                if($row["hiscli_det_id_coord"] != $coordinador) { $change = 1; }
                if($row["hiscli_det_profesional"] != $profesional) { $change = 1; }
                if($row["hiscli_det_frecuencia"] != $frecuencia) { $change = 1; }
                if($row["hiscli_det_cant_mod"] != $cantModulos) { $change = 1; }
                if($row["hiscli_det_total_mod"] != $totalModulos) { $change = 1; }
                if($row["hiscli_det_cant_ses"] != $cantVisitasPorHoras) { $change = 1; }
                if($row["hiscli_det_total_ses"] != $horasVisita) { $change = 1; }
                if($row["hiscli_det_cant_vist_hs"] != $totalHorasVisita) { $change = 1; }
                if($row["hiscli_det_cant_hs"] != $cantSesiones) { $change = 1; }
                if($row["hiscli_det_total_hs"] != $totalSesiones) { $change = 1; }                                            
            }
        }        
        if($change == 1)
        {                      
            $sqlInsert = "INSERT INTO hiscli_det_old select '', hiscli_det_cab_id, hiscli_det_id_prest, hiscli_det_fecha_alta, now(), hiscli_det_id_coord, hiscli_det_profesional, hiscli_det_frecuencia, hiscli_det_cant_mod, hiscli_det_total_mod, hiscli_det_cant_ses, hiscli_det_total_ses, hiscli_det_cant_vist_hs, hiscli_det_cant_hs, hiscli_det_total_hs from hiscli_det WHERE hiscli_det_cab_id='$idCab' AND hiscli_det_id='$idDetalle'";                     
            $sqlUpdate = "UPDATE hiscli_det SET hiscli_det_id_prest='$idPrestacion',hiscli_det_fecha_alta=now(), hiscli_det_fecha_baja='$fechaBaja',hiscli_det_id_coord='$coordinador',hiscli_det_profesional='$profesional',hiscli_det_frecuencia='$frecuencia', hiscli_det_cant_mod='$cantModulos', hiscli_det_total_mod='$totalModulos', hiscli_det_cant_ses='$cantSesiones', hiscli_det_total_ses = '$totalSesiones', hiscli_det_cant_vist_hs = '$cantVisitasPorHoras', hiscli_det_cant_hs = '$horasVisita', hiscli_det_total_hs = '$totalHorasVisita', hiscli_det_observaciones = '$observaciones' WHERE hiscli_det_cab_id='$idCab' AND hiscli_det_id='$idDetalle'";                     
            $resultInsert = mysqli_query($enlace, $sqlInsert);        
            $resultUpdate = mysqli_query($enlace, $sqlUpdate); 
        }

    }


    // $sqlInsert = "UPDATE hiscli_det SET hiscli_det_id_prest='$idPrestacion',hiscli_det_fecha_alta='$fechaAlta',hiscli_det_fecha_baja='$fechaBaja',hiscli_det_desc=''
    // ,hiscli_det_id_coord='$coordinador',hiscli_det_profesional='$profesional',hiscli_det_frecuencia='$frecuencia',hiscli_det_cantidad='$visitas',hiscli_det_horas_visita='$horasPorVisita',
    // hiscli_det_total_horas='$totalHsPorMes',hiscli_det_total_modulos='$totalMdsPorMes',hiscli_det_observaciones='$observaciones' WHERE hiscli_det_cab_id='$idCab' AND hiscli_det_id='$idDetalle'";         
    // $result = mysqli_query($enlace, $sqlInsert);    
}


mysqli_close($enlace);
?>