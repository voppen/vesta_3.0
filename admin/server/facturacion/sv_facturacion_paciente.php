<?php
include('../db_connect.php');

$accion = $_POST['accion'];

if($accion === "buscarFacturacionPaciente")
{    
    $idPaciente = $_POST['paciente'];
    $anio = $_POST['anio'];
    $periodo = $_POST['periodo'];

    $sqlSelectHiscliCab = "SELECT hiscli_id FROM hiscli_cab WHERE hiscli_id_paciente='$idPaciente'";
    $result = mysqli_query($enlace, $sqlSelectHiscliCab); 

    $idCab = mysqli_query($enlace, $sqlSelectHiscliCab)->fetch_object()->hiscli_id; 

    $sqlSelectHiscliDet = "SELECT * FROM hiscli_det JOIN lista_prestaciones on hiscli_det_id_prest = prest_id WHERE hiscli_det_cab_id='$idCab' AND year(hiscli_det_fecha_alta)>='$anio' AND month(hiscli_det_fecha_alta)<='$periodo' AND year(hiscli_det_fecha_baja)<='$anio' AND month(hiscli_det_fecha_baja)>='$periodo' ORDER BY hiscli_det_id_prest";    
    $result = mysqli_query($enlace, $sqlSelectHiscliDet); 

    while($row = $result->fetch_object())
    {
        $jsonData["data"]["hiscliDet"][] = $row;
    }

    echo json_encode($jsonData, JSON_FORCE_OBJECT);
                                
}

if($accion === "buscarFacturacionPacienteOld")
{    
    $idPaciente = $_POST['paciente'];
    $anio = $_POST['anio'];
    $periodo = $_POST['periodo'];

    $sqlSelectHiscliCab = "SELECT hiscli_id FROM hiscli_cab WHERE hiscli_id_paciente='$idPaciente'";
    $result = mysqli_query($enlace, $sqlSelectHiscliCab); 

    $idCab = mysqli_query($enlace, $sqlSelectHiscliCab)->fetch_object()->hiscli_id; 

    $sqlSelectHiscliDetOld = "SELECT * FROM hiscli_det_old JOIN lista_prestaciones on hiscli_det_id_prest = prest_id WHERE hiscli_det_old_cab_id='$idCab' AND year(hiscli_det_old_fecha_alta)>='$anio' AND month(hiscli_det_old_fecha_alta)<='$periodo' AND year(hiscli_det_old_fecha_baja)<='$anio' AND month(hiscli_det_old_fecha_baja)>='$periodo' ORDER BY hiscli_det_old_id_prest";
    $result = mysqli_query($enlace, $sqlSelectHiscliDetOld); 
    $dataResult = $result->fetch_assoc();
    $data['result'] = $dataResult;
    echo json_encode($data);       
                                
}

if($accion=='buscarValorPaciente')
{
    $paciente = $_POST['paciente'];
    $anio = $_POST['anio'];
    $periodo = $_POST['periodo'];
    $prestacion = $_POST['prestacion'];

    $sqlSelectValor = "SELECT * from pacientes join valores_cab on val_id_finan=pac_finan join valores_det on val_det_id_cab=val_id where pac_num_afil = '$paciente' AND val_anio='$anio' AND val_periodo='$periodo' AND val_det_id_prest='$prestacion'";    
    $result = mysqli_query($enlace, $sqlSelectValor); 
    

    while($row = $result->fetch_object())
    {
        $jsonData["data"]["valores"][] = $row;
    }

    echo json_encode($jsonData, JSON_FORCE_OBJECT);
}

if($accion=='guardarFacturacion')
{
    $paciente = $_POST['paciente'];
    $anio = $_POST['anio'];
    $periodo = $_POST['periodo'];
    $prestacion = $_POST['prestacion'];
    $total = $_POST['total'];

    $sqlInsert = "INSERT into facturacion_pac values ('', '$paciente', '$prestacion', '$anio', '$periodo', '$total')";
    $result = mysqli_query($enlace, $sqlInsert);        
    $lastId = mysqli_insert_id($enlace);      
    echo $lastId; 
}

?>