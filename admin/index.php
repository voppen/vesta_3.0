<?php
    session_start();
    include("server/forbidden.php");
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ADMIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/admin.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/form_styles.css" />      
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">        
    <script src="https://use.fontawesome.com/e3b2e79084.js"></script>
    <script src="js/fc_ajax.js"></script>   
    <script src="js/fc_financiadores.js"></script>
    <script src="js/fc_pacientes.js"></script>
    <script src="js/fc_coordinadores.js"></script>
    <script src="js/fc_valores_coord.js"></script>
    <script src="js/fc_valores_finan.js"></script>   
    <script src="js/fc_historias_clinicas.js"></script> 

</head>
<body>    

    <ul class="nav nav-tabs" style="padding-left:6.5em;">
        <li class="nav-item"><a class="nav-link" onclick="cargarPacientes()">Pacientes</a></li>
        <li class="nav-item"><a class="nav-link" onclick="cargarProfesionales()">Profesionales</a></li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Valores</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item"  onclick="cargarValoresCoord()">Coordinadores</a>
                    <a class="dropdown-item" onclick="cargarValoresFinan()">Financiadores</a>                
                </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Facturacion</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" onclick="cargarFacturacionFinanciadores()">Folio Mensual Financiadores</a>
                    <a class="dropdown-item" onclick="cargarFacturacionPacientesAjax()">Folio Mensual Pacientes</a>                
                </div>
        </li>
        <li class="nav-item"><a class="nav-link" onclick="cargarHistoriasClinicas()">Historias Clinicas</a></li>  
        <li class="nav-item"><a class="nav-link" onclick="cargarAlertas()">Alertas</a></li>  
        <li class="nav-item"><a class="nav-link" onclick="cargarFinanciadores()">Financiadores</a></li>  
        <li class="nav-item"><a class="nav-link" onclick="cargarCoordinadores()">Coordinadores</a></li>  
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Usuario: <?php echo($_SESSION["user"]); ?></a>
                <div class="dropdown-menu">
                            <a class="dropdown-item" href="#" onclick="">Cerrar sesión</a>             
                </div>
        </li>
    </ul>

    <div class="container" id="principal">
    <div>          
</body>    
</html>

 