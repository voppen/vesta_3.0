<?php 
  if (!isset ($_SESSION["msgError"])) {
    $_SESSION["msgError"] = "";
    } 
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VestaHC - Inicio de sesión</title>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">   
    <link rel="stylesheet" type="text/css" media="screen" href="css/login.css" />
  </head>

  <body class="text-center" style='margin-left:35%; margin-right:35%; margin-top: 10%;'>
    <form method="post" action="server/sv_login.php" class="form-signin border border-primary rounded" style='padding:10px'>
      <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
	  <h1 class="h3 mb-3 font-weight-normal">VestaHC Panel de control</h1> 
	  <div class="titleForm">Iniciar sesión</div> 
      <label for="user" class="sr-only">Usuario</label>
      <input type="text" name="user" id="user" class="form-control text-center" placeholder="Usuario" required autofocus>
      <label for="password" class="sr-only">Contraseña</label>
      <input type="password" id="password" name="password" class="form-control text-center" placeholder="Contraseña" required>
      <span style="font-color:red"><?php echo($_SESSION["msgError"]); ?></span>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Recordarme
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
    </form>
  </body>
</html>
