-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-06-2018 a las 18:35:25
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vesta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hiscli_det`
--

CREATE TABLE `hiscli_det` (
  `hiscli_det_id` int(11) NOT NULL,
  `hiscli_det_cab_id` int(11) NOT NULL,
  `hiscli_det_id_prest` int(11) NOT NULL,
  `hiscli_det_id_coord` int(11) NOT NULL,
  `hiscli_det_profesional` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hiscli_det_frecuencia` int(11) NOT NULL,
  `hiscli_det_cant_mod` int(11) NOT NULL,
  `hiscli_det_total_mod` int(11) NOT NULL,
  `hiscli_det_cant_ses` int(11) NOT NULL,
  `hiscli_det_total_ses` int(11) NOT NULL,
  `hiscli_det_cant_hs` int(11) NOT NULL,
  `hiscli_det_total_hs` int(11) NOT NULL,
  `hiscli_det_observaciones` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `hiscli_det`
--

INSERT INTO `hiscli_det` (`hiscli_det_id`, `hiscli_det_cab_id`, `hiscli_det_id_prest`, `hiscli_det_id_coord`, `hiscli_det_profesional`, `hiscli_det_frecuencia`, `hiscli_det_cant_mod`, `hiscli_det_total_mod`, `hiscli_det_cant_ses`, `hiscli_det_total_ses`, `hiscli_det_cant_hs`, `hiscli_det_total_hs`, `hiscli_det_observaciones`) VALUES
(3, 27, 0, 0, 'sfe', 0, 0, 0, 0, 0, 0, 0, ''),
(4, 27, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hiscli_det`
--
ALTER TABLE `hiscli_det`
  ADD PRIMARY KEY (`hiscli_det_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `hiscli_det`
--
ALTER TABLE `hiscli_det`
  MODIFY `hiscli_det_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
