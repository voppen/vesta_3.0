-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-07-2018 a las 02:13:21
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vesta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinadores`
--

CREATE TABLE `coordinadores` (
  `coord_id` int(11) NOT NULL,
  `coord_nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `coordinadores`
--

INSERT INTO `coordinadores` (`coord_id`, `coord_nombre`) VALUES
(1, 'Coordprueba'),
(2, 'Amigo'),
(3, 'Tres'),
(4, 'ESID');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cordones`
--

CREATE TABLE `cordones` (
  `cordon_id` int(11) NOT NULL,
  `cordon_nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cordones`
--

INSERT INTO `cordones` (`cordon_id`, `cordon_nombre`) VALUES
(1, 'AMBA'),
(2, 'Primer Cordón'),
(3, 'Segundo Cordón');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `detalle_id_cab` int(11) NOT NULL,
  `detalle_id` int(11) NOT NULL,
  `detalle_id_prest` int(11) NOT NULL,
  `detalle_fecha_inicio` int(11) NOT NULL,
  `detalle_fecha_fin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_pac`
--

CREATE TABLE `facturacion_pac` (
  `fac_id` int(11) NOT NULL,
  `fac_id_pac` int(11) NOT NULL,
  `fac_id_prest` int(11) NOT NULL,
  `fac_anio` int(11) NOT NULL,
  `fac_periodo` int(11) NOT NULL,
  `fac_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facturacion_pac`
--

INSERT INTO `facturacion_pac` (`fac_id`, `fac_id_pac`, `fac_id_prest`, `fac_anio`, `fac_periodo`, `fac_total`) VALUES
(25, 1, 9, 2018, 7, 1821),
(26, 1, 4, 2018, 7, 12720);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiadores`
--

CREATE TABLE `financiadores` (
  `finan_id` int(11) NOT NULL,
  `finan_nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `financiadores`
--

INSERT INTO `financiadores` (`finan_id`, `finan_nombre`) VALUES
(113, 'Medife');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hiscli_cab`
--

CREATE TABLE `hiscli_cab` (
  `hiscli_id` int(11) NOT NULL,
  `hiscli_id_paciente` int(11) NOT NULL,
  `hiscli_ingreso` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hiscli_fecha_alta` date NOT NULL,
  `hiscli_fecha_baja` date NOT NULL,
  `hiscli_estado` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `hiscli_descripcion` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hiscli_cab`
--

INSERT INTO `hiscli_cab` (`hiscli_id`, `hiscli_id_paciente`, `hiscli_ingreso`, `hiscli_fecha_alta`, `hiscli_fecha_baja`, `hiscli_estado`, `hiscli_descripcion`) VALUES
(18, 1, 'Enfermedad', '2018-01-01', '2018-10-25', 'Vigente', 'rescripcionsdf'),
(19, 2, '', '0000-00-00', '0000-00-00', 'Vigente', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hiscli_det`
--

CREATE TABLE `hiscli_det` (
  `hiscli_det_id` int(11) NOT NULL,
  `hiscli_det_cab_id` int(11) NOT NULL,
  `hiscli_det_id_prest` int(11) NOT NULL,
  `hiscli_det_fecha_alta` date NOT NULL,
  `hiscli_det_fecha_baja` date NOT NULL,
  `hiscli_det_id_coord` int(11) NOT NULL,
  `hiscli_det_profesional` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hiscli_det_frecuencia` int(11) NOT NULL,
  `hiscli_det_cant_mod` int(11) NOT NULL,
  `hiscli_det_total_mod` int(11) NOT NULL,
  `hiscli_det_cant_ses` int(11) NOT NULL,
  `hiscli_det_total_ses` int(11) NOT NULL,
  `hiscli_det_cant_vist_hs` int(11) NOT NULL,
  `hiscli_det_cant_hs` int(11) NOT NULL,
  `hiscli_det_total_hs` int(11) NOT NULL,
  `hiscli_det_observaciones` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `hiscli_det`
--

INSERT INTO `hiscli_det` (`hiscli_det_id`, `hiscli_det_cab_id`, `hiscli_det_id_prest`, `hiscli_det_fecha_alta`, `hiscli_det_fecha_baja`, `hiscli_det_id_coord`, `hiscli_det_profesional`, `hiscli_det_frecuencia`, `hiscli_det_cant_mod`, `hiscli_det_total_mod`, `hiscli_det_cant_ses`, `hiscli_det_total_ses`, `hiscli_det_cant_vist_hs`, `hiscli_det_cant_hs`, `hiscli_det_total_hs`, `hiscli_det_observaciones`) VALUES
(12, 18, 4, '2018-06-01', '2018-08-31', 1, 'PROFESIONAL', 1, 2, 60, 0, 0, 0, 0, 0, ''),
(13, 18, 5, '2018-06-01', '2018-06-30', 1, 'Prof', 2, 2, 16, 0, 0, 0, 0, 0, ''),
(15, 18, 9, '2018-06-01', '2018-07-18', 1, 'Prof Med', 3, 0, 0, 4, 4, 0, 0, 0, ''),
(16, 19, 2, '2018-06-01', '2018-08-24', 0, '', 1, 0, 0, 0, 0, 0, 0, 10, ''),
(17, 19, 4, '2018-07-01', '2018-07-31', 0, '', 1, 0, 4, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hiscli_det_old`
--

CREATE TABLE `hiscli_det_old` (
  `hiscli_det_old_id` int(11) NOT NULL,
  `hiscli_det_old_cab_id` int(11) NOT NULL,
  `hiscli_det_old_id_prest` int(11) NOT NULL,
  `hiscli_det_old_fecha_alta` date NOT NULL,
  `hiscli_det_old_fecha_baja` date NOT NULL,
  `hiscli_det_old_id_coord` int(11) NOT NULL,
  `hiscli_det_old_profesional` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hiscli_det_old_frecuencia` int(11) NOT NULL,
  `hiscli_det_old_cant_mod` int(11) NOT NULL,
  `hiscli_det_old_total_mod` int(11) NOT NULL,
  `hiscli_det_old_cant_ses` int(11) NOT NULL,
  `hiscli_det_old_total_ses` int(11) NOT NULL,
  `hiscli_det_old_cant_vist_hs` int(11) NOT NULL,
  `hiscli_det_old_cant_hs` int(11) NOT NULL,
  `hiscli_det_old_total_hs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `hiscli_det_old`
--

INSERT INTO `hiscli_det_old` (`hiscli_det_old_id`, `hiscli_det_old_cab_id`, `hiscli_det_old_id_prest`, `hiscli_det_old_fecha_alta`, `hiscli_det_old_fecha_baja`, `hiscli_det_old_id_coord`, `hiscli_det_old_profesional`, `hiscli_det_old_frecuencia`, `hiscli_det_old_cant_mod`, `hiscli_det_old_total_mod`, `hiscli_det_old_cant_ses`, `hiscli_det_old_total_ses`, `hiscli_det_old_cant_vist_hs`, `hiscli_det_old_cant_hs`, `hiscli_det_old_total_hs`) VALUES
(43, 20, 0, '0000-00-00', '2018-07-18', 0, '', 1, 0, 0, 10, 20, 0, 0, 0),
(44, 21, 14, '0000-00-00', '2018-07-18', 0, '', 1, 0, 0, 0, 0, 100, 200, 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internaciones`
--

CREATE TABLE `internaciones` (
  `inter_id` int(11) NOT NULL,
  `inter_id_cab` int(11) NOT NULL,
  `inter_pac_id` int(11) NOT NULL,
  `inter_fecha_desde` date NOT NULL,
  `inter_fecha_hasta` date NOT NULL,
  `inter_motivo` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `internaciones`
--

INSERT INTO `internaciones` (`inter_id`, `inter_id_cab`, `inter_pac_id`, `inter_fecha_desde`, `inter_fecha_hasta`, `inter_motivo`) VALUES
(149, 7, 1, '0000-00-00', '0000-00-00', 'asda'),
(151, 15, 1, '2018-07-02', '2018-07-05', 'Motivo Internacion 1'),
(152, 16, 1, '2018-07-02', '2018-07-12', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_periodos`
--

CREATE TABLE `lista_periodos` (
  `id_periodo` int(11) NOT NULL,
  `nombre_periodo` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `lista_periodos`
--

INSERT INTO `lista_periodos` (`id_periodo`, `nombre_periodo`) VALUES
(1, 'Enero'),
(2, 'Febrero'),
(3, 'Marzo'),
(4, 'Abril'),
(5, 'Mayo'),
(6, 'Junio'),
(7, 'Julio'),
(8, 'Agosto'),
(9, 'Septiembre'),
(10, 'Octubre'),
(11, 'Noviembre'),
(12, 'Diciembre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_prestaciones`
--

CREATE TABLE `lista_prestaciones` (
  `prest_id` int(11) NOT NULL,
  `prest_nombre` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `prest_mod` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `lista_prestaciones`
--

INSERT INTO `lista_prestaciones` (`prest_id`, `prest_nombre`, `prest_mod`) VALUES
(0, 'Akm', 'S'),
(1, 'Akr', 'S'),
(2, 'Cuidador', 'H'),
(3, 'DL/MT', 'S'),
(4, 'Modulo 1 Enfermeria', 'M'),
(5, 'Modulo 2 Enfermeria', 'M'),
(6, 'Visita Enfermeria', 'H'),
(7, 'Fonoaudiologia', 'S'),
(8, 'Kinesiologia', 'S'),
(9, 'Medico Clinico', 'S'),
(10, 'Medico Especialista', 'S'),
(11, 'Medico Paliativista', 'S'),
(12, 'Nutricion', 'S'),
(13, 'Terapia Ocupacional', 'S'),
(14, 'Guardia Enfermeria 4', 'H'),
(15, 'Guardia Enfermeria 6', 'H'),
(16, 'Guardia Enfermeria 8', 'H'),
(17, 'Guardia Enfermeria 12', 'H');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `pac_num_afil` int(11) NOT NULL,
  `pac_cordon` int(2) NOT NULL,
  `pac_nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_finan` int(11) NOT NULL,
  `pac_iva` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_diag` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_fecha_alta` varchar(50) COLLATE utf16_unicode_ci NOT NULL,
  `pac_dom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_tel1` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_tel2` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_resp` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_tel_resp` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`pac_num_afil`, `pac_cordon`, `pac_nombre`, `pac_finan`, `pac_iva`, `pac_diag`, `pac_fecha_alta`, `pac_dom`, `pac_tel1`, `pac_tel2`, `pac_resp`, `pac_tel_resp`) VALUES
(1, 1, 'Paciente Uno', 113, 'EXC', 'Enfermedad', '2018-07-01', 'Domicilio 001', '34095', '230498', 'Responsable 1', '0011-0011'),
(2, 2, 'Paciente Dos', 113, 'EXC', 'Algo', '2018-07-01', '', '', '', '', ''),
(3, 3, 'Paciente Tres', 113, 'RES', 'Diagnostico', '2017-06-01', 'Domicilio 003', '22222', '3333', 'Responsable 3', ' 333333');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesionales`
--

CREATE TABLE `profesionales` (
  `prof_id` int(11) NOT NULL,
  `prof_id_coord` int(11) NOT NULL,
  `prof_nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prof_profesion` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesionales`
--

INSERT INTO `profesionales` (`prof_id`, `prof_id_coord`, `prof_nombre`, `prof_profesion`) VALUES
(2, 1, 'UNO', 'AKR'),
(3, 0, 'DOS', 'PROF'),
(4, 0, 'TRES', ''),
(5, 0, 'CUATRO', ''),
(6, 4, 'CINCO', 'PROF');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `contraseña` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `contraseña`) VALUES
('usuario1', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores_cab`
--

CREATE TABLE `valores_cab` (
  `val_id` int(11) NOT NULL,
  `val_id_finan` int(11) NOT NULL,
  `val_id_coord` int(11) NOT NULL,
  `val_periodo` int(11) NOT NULL,
  `val_anio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `valores_cab`
--

INSERT INTO `valores_cab` (`val_id`, `val_id_finan`, `val_id_coord`, `val_periodo`, `val_anio`) VALUES
(97, 113, 0, 7, 2018),
(98, 113, 1, 7, 2018),
(99, 113, 2, 7, 2018),
(100, 113, 2, 10, 2018),
(101, 113, 0, 11, 2018),
(102, 113, 0, 12, 2018);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores_det`
--

CREATE TABLE `valores_det` (
  `val_det_id` int(11) NOT NULL,
  `val_det_id_cab` int(11) NOT NULL,
  `val_det_id_prest` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `val_det_valor_amba` decimal(10,2) NOT NULL,
  `val_det_valor_cordUno` decimal(10,2) NOT NULL,
  `val_det_valor_cordDos` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `valores_det`
--

INSERT INTO `valores_det` (`val_det_id`, `val_det_id_cab`, `val_det_id_prest`, `val_det_valor_amba`, `val_det_valor_cordUno`, `val_det_valor_cordDos`) VALUES
(35524, 97, '4', '212.00', '265.00', '328.60'),
(35525, 97, '5', '402.80', '445.20', '530.00'),
(35526, 97, '9', '455.20', '540.60', '657.20'),
(35527, 97, '2', '100.00', '100.00', '100.00'),
(35528, 98, '6', '100.00', '200.00', '300.00'),
(35529, 98, '1', '400.00', '500.00', '600.00'),
(35530, 99, '13', '100.00', '300.00', '334.00'),
(35531, 99, '15', '200.00', '300.00', '340.00'),
(35532, 100, '15', '220.00', '330.00', '374.00'),
(35533, 100, '13', '120.00', '360.00', '400.80'),
(35534, 101, '15', '100.00', '200.00', '300.00'),
(35535, 102, '4', '243.80', '304.75', '377.89'),
(35536, 102, '2', '115.00', '115.00', '115.00'),
(35537, 102, '9', '523.48', '621.69', '755.78'),
(35538, 102, '5', '463.22', '511.98', '609.50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `coordinadores`
--
ALTER TABLE `coordinadores`
  ADD PRIMARY KEY (`coord_id`);

--
-- Indices de la tabla `cordones`
--
ALTER TABLE `cordones`
  ADD PRIMARY KEY (`cordon_id`);

--
-- Indices de la tabla `facturacion_pac`
--
ALTER TABLE `facturacion_pac`
  ADD PRIMARY KEY (`fac_id`);

--
-- Indices de la tabla `financiadores`
--
ALTER TABLE `financiadores`
  ADD PRIMARY KEY (`finan_id`);

--
-- Indices de la tabla `hiscli_cab`
--
ALTER TABLE `hiscli_cab`
  ADD PRIMARY KEY (`hiscli_id`);

--
-- Indices de la tabla `hiscli_det`
--
ALTER TABLE `hiscli_det`
  ADD PRIMARY KEY (`hiscli_det_id`);

--
-- Indices de la tabla `hiscli_det_old`
--
ALTER TABLE `hiscli_det_old`
  ADD PRIMARY KEY (`hiscli_det_old_id`);

--
-- Indices de la tabla `internaciones`
--
ALTER TABLE `internaciones`
  ADD PRIMARY KEY (`inter_id`);

--
-- Indices de la tabla `lista_prestaciones`
--
ALTER TABLE `lista_prestaciones`
  ADD PRIMARY KEY (`prest_id`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`pac_num_afil`);

--
-- Indices de la tabla `profesionales`
--
ALTER TABLE `profesionales`
  ADD PRIMARY KEY (`prof_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- Indices de la tabla `valores_cab`
--
ALTER TABLE `valores_cab`
  ADD PRIMARY KEY (`val_id`);

--
-- Indices de la tabla `valores_det`
--
ALTER TABLE `valores_det`
  ADD PRIMARY KEY (`val_det_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `coordinadores`
--
ALTER TABLE `coordinadores`
  MODIFY `coord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cordones`
--
ALTER TABLE `cordones`
  MODIFY `cordon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `facturacion_pac`
--
ALTER TABLE `facturacion_pac`
  MODIFY `fac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `financiadores`
--
ALTER TABLE `financiadores`
  MODIFY `finan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT de la tabla `hiscli_cab`
--
ALTER TABLE `hiscli_cab`
  MODIFY `hiscli_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `hiscli_det`
--
ALTER TABLE `hiscli_det`
  MODIFY `hiscli_det_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `hiscli_det_old`
--
ALTER TABLE `hiscli_det_old`
  MODIFY `hiscli_det_old_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `internaciones`
--
ALTER TABLE `internaciones`
  MODIFY `inter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT de la tabla `profesionales`
--
ALTER TABLE `profesionales`
  MODIFY `prof_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `valores_cab`
--
ALTER TABLE `valores_cab`
  MODIFY `val_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `valores_det`
--
ALTER TABLE `valores_det`
  MODIFY `val_det_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35539;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
