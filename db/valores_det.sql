-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-06-2018 a las 21:06:54
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vesta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores_det`
--

CREATE TABLE `valores_det` (
  `val_det_id` int(11) NOT NULL,
  `val_det_id_cab` int(11) NOT NULL,
  `val_det_id_prest` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `val_det_valor_amba` decimal(10,0) NOT NULL,
  `val_det_valor_cordUno` decimal(10,0) NOT NULL,
  `val_det_valor_cordDos` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `valores_det`
--

INSERT INTO `valores_det` (`val_det_id`, `val_det_id_cab`, `val_det_id_prest`, `val_det_valor_amba`, `val_det_valor_cordUno`, `val_det_valor_cordDos`) VALUES
(35420, 26, '0', '250', '300', '350'),
(35421, 27, '6', '100', '100', '100'),
(35422, 27, '12', '200', '200', '200');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `valores_det`
--
ALTER TABLE `valores_det`
  ADD PRIMARY KEY (`val_det_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `valores_det`
--
ALTER TABLE `valores_det`
  MODIFY `val_det_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35423;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
