-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-06-2018 a las 18:27:03
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vesta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_prestaciones`
--

CREATE TABLE `lista_prestaciones` (
  `prest_id` int(11) NOT NULL,
  `prest_nombre` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `prest_mod` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `lista_prestaciones`
--

INSERT INTO `lista_prestaciones` (`prest_id`, `prest_nombre`, `prest_mod`) VALUES
(0, 'Akm', 'S'),
(1, 'Akr', 'S'),
(2, 'Cuidador', 'H'),
(3, 'DL/MT', 'S'),
(4, 'Modulo 1 Enfermeria', 'M'),
(5, 'Modulo 2 Enfermeria', 'M'),
(6, 'Visita Enfermeria', 'H'),
(7, 'Fonoaudiologia', 'S'),
(8, 'Kinesiologia', 'S'),
(9, 'Medico Clinico', 'S'),
(10, 'Medico Especialista', 'S'),
(11, 'Medico Paliativista', 'S'),
(12, 'Nutricion', 'S'),
(13, 'Terapia Ocupacional', 'S'),
(14, 'Guardia Enfermeria 4', 'H'),
(15, 'Guardia Enfermeria 6', 'H'),
(16, 'Guardia Enfermeria 8', 'H'),
(17, 'Guardia Enfermeria 12', 'H');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `lista_prestaciones`
--
ALTER TABLE `lista_prestaciones`
  ADD PRIMARY KEY (`prest_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
