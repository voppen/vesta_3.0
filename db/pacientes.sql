-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-06-2018 a las 02:29:16
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `vesta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE IF NOT EXISTS `pacientes` (
  `pac_num_afil` int(11) NOT NULL,
  `pac_cordon` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_iva` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_diag` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_fecha_alta` varchar(50) COLLATE utf16_unicode_ci NOT NULL,
  `pac_dom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_tel1` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_tel2` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_resp` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pac_tel_resp` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`pac_num_afil`, `pac_cordon`, `pac_nombre`, `pac_iva`, `pac_diag`, `pac_fecha_alta`, `pac_dom`, `pac_tel1`, `pac_tel2`, `pac_resp`, `pac_tel_resp`) VALUES
(3, 'primero2', 'paciente t2', 'EXC', 'ssdfpo', '2017-12-27', 'sdfo', '2308', '09834', '0sdfj', '023984'),
(5, 'primero', 'paciente dos', 'RES', 'sfe', '2018-01-02', 'dom', '23498', '4324', 'feosfj', '23498'),
(6, 'tercero', 'Paciente Tres', 'EXC', 'aÃ±os', '2018-01-09', 'sfdoij', '2374', '923847', 'sfoesi', '2837'),
(7, 'segundo', 'ññ äá', 'EXC', 'wfe', '2018-05-18', 'wfe', '123', '312', '123', '123'),
(8, 'segundo', 'ññ áÄ|°%$', 'EXC', 'Ã±Ã± Ã¡Ã„|Â°%$', '2018-05-18', 'Ã±Ã± Ã¡Ã„|Â°%$', 'Ã±Ã± Ã¡Ã„|Â°%$', 'Ã±Ã± Ã¡Ã„|Â°%$', 'Ã±Ã± Ã¡Ã„|Â°%$', 'Ã±Ã± Ã¡Ã„|Â°%$'),
(9, 'tercero', 'ññ áÄ|°%$', 'EXC', 'ññ áÄ|°%$', '2018-05-18', 'ññ áÄ|°%$', 'ññ áÄ|°%$', 'ññ áÄ|°%$', 'ññ áÄ|°%$', 'ññ áÄ|°%$'),
(10, 'primero', 'ññ áÄ|°%$', 'EXC', 'ññ áÄ|°%$', '2018-05-18', 'ññ áÄ|°%$', 'ññ áÄ|°%$', 'ññ áÄ|°%$', 'ññ áÄ|°%$', 'ññ áÄ|°%$'),
(11, 'segundo', 'ññ áÄ|°%$ $#$%', 'EXC', 'ññ áÄ|°%$ $#$%', '2018-05-18', 'Ã±Ã± Ã¡Ã„|Â°%$ $#$%', 'ññ áÄ|°%$ $#$%', 'ññ áÄ|°%$ $#$%', 'ññ áÄ|°%$ $#$%', 'ññ áÄ|°%$ $#$%');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
 ADD PRIMARY KEY (`pac_num_afil`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
